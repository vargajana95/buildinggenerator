﻿using System.Collections.Generic;
using UnityEngine;

namespace BuildingGenerator.Base
{
    public abstract class BaseGenerator: ScriptableObject
    {
        public abstract BuildingBase Generate();
    }
}