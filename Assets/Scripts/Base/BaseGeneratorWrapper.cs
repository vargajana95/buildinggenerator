﻿using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;

namespace BuildingGenerator.Base
{
   
    public class BaseGeneratorWrapper : BaseGenerator
    {

        public BuildingBase BuildingBase { get; set; }

        public override BuildingBase Generate()
        {
            return BuildingBase;
        }
    }
}