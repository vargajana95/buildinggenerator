﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BuildingGenerator.Base
{
    [CreateAssetMenu(fileName = "SegmentBaseGenerator",
        menuName = "BuildingGeneration/BaseGenerator/Skyscraper/SegmentBaseGenerator", order = 1)]
    public class SegmentBaseGenerator : BaseGenerator
    {
        public List<Vector2Int> segmentSizes = new List<Vector2Int>
            {new Vector2Int(3, 1), new Vector2Int(3, 1), new Vector2Int(3, 1), new Vector2Int(3, 1)};

        public int maxSizeY = 30;

        public override BuildingBase Generate()
        {
            var polygon = new List<Vector2>();

            var depth = 0;
            var prevSegmentX = -1;
            for (int section = 0; section < segmentSizes.Count && (depth < maxSizeY || section == 0); section++)
            {
                if (segmentSizes[section].y > depth || section == segmentSizes.Count - 1)
                {
                    if (depth != 0 && prevSegmentX != segmentSizes[section].x)
                    {
                        polygon.Add(new Vector2(segmentSizes[section].x, depth));
                    }

                    depth = section == segmentSizes.Count - 1 ? maxSizeY : Math.Min(segmentSizes[section].y, maxSizeY);


                    var nextSegmentX = segmentSizes[section].x;
                    for (int i = section + 1; i < segmentSizes.Count; i++)
                    {
                        var y = i == segmentSizes.Count - 1 ? maxSizeY : Math.Min(segmentSizes[i].y, maxSizeY);
                        if (y > segmentSizes[section].y)
                        {
                            nextSegmentX = segmentSizes[i].x;
                            break;
                        }
                    }

                    if (depth >= maxSizeY || nextSegmentX != segmentSizes[section].x)
                    {
                        polygon.Add(new Vector2(segmentSizes[section].x, depth));
                    }

                    prevSegmentX = segmentSizes[section].x;
                }


                // depth = y;
            }

            var finishedPolygon = new List<Vector2>();
            var polygonReversed = polygon.Reverse<Vector2>().ToList();
            finishedPolygon.AddRange(polygonReversed);
            finishedPolygon.AddRange(polygon.Select(p => new Vector2(p.x, -p.y + 1)));
            finishedPolygon.AddRange(polygonReversed.Select(p => -p + Vector2.one));
            finishedPolygon.AddRange(polygon.Select(p => new Vector2(-p.x + 1, p.y)));

            //TODO remove *0.5f
            return new BuildingBase {Polygon = finishedPolygon.Select(p => p * 0.5f).ToList()};
        }
    }
}