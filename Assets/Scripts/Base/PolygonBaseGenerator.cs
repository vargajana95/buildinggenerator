﻿using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;

namespace BuildingGenerator.Base
{
    [CreateAssetMenu(fileName = "PolygonBaseGenerator",
        menuName = "BuildingGeneration/BaseGenerator/PolygonBaseGenerator", order = 2)]
    public class PolygonBaseGenerator : BaseGenerator
    {
        [SerializeField] private List<Vector2> polygon = new List<Vector2>()
        {
            new Vector2(35, 10),
            new Vector2(10, -19),
            new Vector2(-22, -10),
            new Vector2(-20, 10),
            new Vector2(0, 40),
            new Vector2(15, 40)
        };

        public List<Vector2> Polygon
        {
            get => polygon;
            set => polygon = value;
        }

        public List<int> AdjacentSides { get; set; } = new List<int>();

        public override BuildingBase Generate()
        {
            var buildingBase = new BuildingBase {Polygon = polygon};
            buildingBase.SetAdjacentSides(AdjacentSides);
            return buildingBase;
        }
    }
}