﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProceduralToolkit;
using UnityEngine;

namespace BuildingGenerator.Base
{
    public class BuildingBase : IEnumerable<BuildingBase.Side>
    {
        private List<Vector2> polygon = new List<Vector2>();
        private List<bool> hasMainEntrances = new List<bool>();
        private List<bool> isAdjacentSides = new List<bool>();

        public IReadOnlyList<Vector2> Polygon
        {
            get => polygon;
            set
            {
                polygon = new List<Vector2>(value);
                if (hasMainEntrances.Count == 0)
                {
                    hasMainEntrances = Enumerable.Repeat(false, value.Count).ToList();
                }

                if (isAdjacentSides.Count == 0)
                {
                    isAdjacentSides = Enumerable.Repeat(false, value.Count).ToList();
                }
            }
        }

        public IReadOnlyList<bool> HasMainEntrance => hasMainEntrances;

        public IReadOnlyList<bool> IsAdjacentSide => isAdjacentSides;

        public void AddPoint(Vector2 point, bool isAdjacent = false, bool hasEntrance = false)
        {
            polygon.Add(point);
            hasMainEntrances.Add(hasEntrance);
            isAdjacentSides.Add(isAdjacent);
        }

        public void SetAdjacentSides(List<int> adjacentSides)
        {
            foreach (var adjacentSideIndex in adjacentSides)
            {
                if (adjacentSideIndex < polygon.Count && adjacentSideIndex >= 0)
                {
                    isAdjacentSides[adjacentSideIndex] = true;
                }
            }
        }

        public class Side
        {
            public Vector2 StartPoint { get; internal set; }
            public Vector2 EndPoint { get; internal set; }
            public bool HasMainEntrance { get; internal set; }
            public bool IsAdjacent { get; internal set; }

            public int Index { get; internal set; }
        }

        public IEnumerator<Side> GetEnumerator()
        {
            for (var i = 0; i < Polygon.Count; i++)
            {
                yield return new Side
                {
                    StartPoint = polygon[i],
                    EndPoint = polygon.GetLooped(i + 1),
                    IsAdjacent = isAdjacentSides[i],
                    HasMainEntrance = hasMainEntrances[i],
                    Index = i
                };
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}