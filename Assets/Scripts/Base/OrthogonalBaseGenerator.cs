﻿using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;

namespace BuildingGenerator.Base
{
    [CreateAssetMenu(fileName = "OrthogonalBaseGenerator", menuName = "BuildingGeneration/BaseGenerator/OrthogonalBaseGenerator",
        order = 1)]
    public class OrthogonalBaseGenerator : BaseGenerator
    {
        [SerializeField] private int baseGenerationIterations = 8;
        [SerializeField] private float wallLength = 10f;

        public override BuildingBase Generate()
        {
            var (startingPos, grid) = GenerateBaseGrid();

            // DrawDebugPlanes(grid, startingPos);

            var directions = new[] {Vector2Int.right, Vector2Int.up, Vector2Int.left, Vector2Int.down};

            var polygon = new List<Vector2>();

            var currentPoint = Vector2.zero;

            var currentPos = startingPos;

            int currentDirection = 0;
            int prevBadDirection = -1;
            while (currentPoint != Vector2.zero || polygon.Count == 0)
            {
                for (int i = 0; i < directions.Length; i++)
                {
                    if (!grid.Contains(currentPos + directions.GetLooped(currentDirection)))
                    {
                        currentPos += directions.GetLooped(currentDirection);
                        currentDirection = (currentDirection - 1 + 4) % 4;

                        break;
                    }


                    //Skip middle points on straight lines
                    if (currentDirection != prevBadDirection)
                    {
                        polygon.Add(currentPoint);
                    }

                    prevBadDirection = currentDirection;
                    currentDirection = (currentDirection + 1) % 4;
                    var nextDir = directions.GetLooped(currentDirection);
                    currentPoint += new Vector2(nextDir.x, nextDir.y) * wallLength;
                }
            }

            return new BuildingBase {Polygon = polygon};
        }

        private (Vector2Int startingPos, HashSet<Vector2Int> grid) GenerateBaseGrid()
        {
            var currentPos = Vector2Int.zero;
            var startingPos = currentPos;
            var grid = new HashSet<Vector2Int>();
            grid.Add(currentPos);
            for (int i = 1;
                i < baseGenerationIterations;
                i++)
            {
                if (RandomE.Chance(0.5f))
                {
                    currentPos.x = currentPos.x + (RandomE.Chance(0.5f) ? 1 : -1);
                }
                else
                {
                    currentPos.y = currentPos.y + (RandomE.Chance(0.5f) ? 1 : -1);
                }

                if (currentPos.y < startingPos.y)
                {
                    startingPos = currentPos;
                }

                if (currentPos.y == startingPos.y && currentPos.x < startingPos.x)
                {
                    startingPos = currentPos;
                }

                grid.Add(currentPos);
            }

            return (new Vector2Int(startingPos.x - 1, startingPos.y), grid);
        }
    }
}