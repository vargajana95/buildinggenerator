﻿using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;

namespace BuildingGenerator.Base
{
    [CreateAssetMenu(fileName = "RectangleBaseGenerator",
        menuName = "BuildingGeneration/BaseGenerator/RectangleBaseGenerator", order = 2)]
    public class RectangleBaseGenerator : BaseGenerator
    {
        [SerializeField] private float width = 5;
        [SerializeField] private float height = 5;

        public float Width
        {
            get => width;
            set => width = value;
        }

        public float Height
        {
            get => height;
            set => height = value;
        }

        public override BuildingBase Generate()
        {
            return new BuildingBase
            {
                Polygon = new List<Vector2>
                {
                    Vector2.left * width / 2 + Vector2.up * height / 2,
                    -Vector2.left * width / 2 + Vector2.up * height / 2,
                    -Vector2.left * width / 2 - Vector2.up * height / 2,
                    Vector2.left * width / 2 - Vector2.up * height / 2
                }
            };
        }
    }
}