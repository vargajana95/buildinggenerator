﻿using System.Collections.Generic;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Roof;
using UnityEngine;

namespace BuildingGenerator.Creator
{
    [CreateAssetMenu(fileName = "RoofMaterialRandomizer", menuName = "BuildingGeneration/Randomizer/RoofGenerator/RoofMaterialRandomizer")]
    public class RoofMaterialRandomizer: RoofGeneratorRandomizer
    {
        [SerializeField] private RoofGenerator roofGenerator;
        
        [SerializeField] [ExposeMaterials("roofGenerator")] private MaterialContainerRandomizer materialContainerRandomizer;
        
        public override RoofGenerator CreateRoofGenerator()
        {
            var createdRoofGenerator = Instantiate(roofGenerator);

            SetMaterials(createdRoofGenerator, materialContainerRandomizer);

            return createdRoofGenerator;
        }

        public override bool CanGenerate(List<Vector2> polygon) => roofGenerator.CanGenerate(polygon);
    }
}