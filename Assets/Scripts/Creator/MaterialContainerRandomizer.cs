﻿using System;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Selectors;
using UnityEngine;

namespace BuildingGenerator.Creator
{
    [Serializable]
    public class MaterialContainerRandomizer
    {
        [HideInInspector] [SerializeField] public FieldValue[] fieldValues;

        [Serializable]
        public class FieldValue
        {
            public string propertyName;
           [Expandable] public MaterialSelector value;
        }
    }
}