﻿using System.Collections.Generic;
using BuildingGenerator.Common;
using BuildingGenerator.Facade;
using UnityEngine;
using UnityExtensions;

namespace BuildingGenerator.Creator
{
    public abstract class SkyscraperSectionCreator : ScriptableObject
    {
        [ReorderableList(hideFooterButtons = true)]
        public List<string> groupNames;
        
        [SerializeField] protected int minSectionCount = 3;
        [SerializeField] protected int maxSectionCount = 6;
        [SerializeField] private int maxWidth = 30;
        [SerializeField] private int maxDepth = 30;

        public int MaxWidth
        {
            get => maxWidth;
            set => maxWidth = value;
        }

        public int MaxDepth
        {
            get => maxDepth;
            set => maxDepth = value;
        }

        public abstract List<Section> CreateSections(int minHeight, int maxHeight, FacadeGenerator facadeGenerator, Dictionary<string, Material> commonMaterials);
    }
}