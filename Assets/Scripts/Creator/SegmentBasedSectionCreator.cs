﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Base;
using BuildingGenerator.Common;
using BuildingGenerator.Facade;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace BuildingGenerator.Creator
{
    [CreateAssetMenu(fileName = "SegmentBasedSectionCreator",
        menuName = "BuildingGeneration/Skyscraper/SegmentBasedSectionCreator", order = 1)]
    public class SegmentBasedSectionCreator : SkyscraperSectionCreator
    {
        public Quaternion BaseRotation { get; set; }
        public Vector2 Origin { get; set; }

        public override List<Section> CreateSections(int minHeight, int maxHeight, FacadeGenerator facadeGenerator, Dictionary<string, Material> commonMaterials)
        {
            var bases = GenerateSectionBases(minHeight, maxHeight);

            var sections = new List<Section>();
            foreach (var (segments, maxY, height) in bases)
            {
                var sectionBaseGenerator = CreateInstance<SegmentBaseGenerator>();
                sectionBaseGenerator.segmentSizes = segments;
                sectionBaseGenerator.maxSizeY = maxY;
                var section = new Section()
                {
                    height = height,
                    baseGenerator = sectionBaseGenerator,
                    facadeGenerator = Instantiate(facadeGenerator),
                    rotation = BaseRotation,
                    origin = Origin
                };

                sections.Add(section);
            }

            return sections;
        }

        private List<(List<Vector2Int> segments, int maxY, int height)> GenerateSectionBases(int minHeight,
            int maxHeight)
        {
            var bases = new List<(List<Vector2Int> segments, int maxY, int height)>();
            var height = Random.Range(minHeight, maxHeight);
            var remainingHeight = height;
            var sections = Random.Range(minSectionCount, maxSectionCount);
            var maxY = MaxWidth;


            int n;
            if (sections <= 1)
            {
                n = remainingHeight;
            }
            else
            {
                n = Random.Range(20, remainingHeight);
            }

            remainingHeight -= n;

            bases.Add((new List<Vector2Int>(), maxY, n));
            for (var j = 0; j < 3; j++)
            {
                var s = new Vector2Int(Random.Range(5, MaxDepth), Random.Range(0, 4) * 3);
                bases[0].segments.Add(s);
            }

            for (int i = 0; i < sections && remainingHeight > 0; i++)
            {
                if (i == sections - 1)
                {
                    n = remainingHeight;
                }
                else
                {
                    n = Random.Range(20, remainingHeight);
                }

                remainingHeight -= n;

                maxY = Random.Range(10, maxY);
                var segmentSizes = new List<Vector2Int>();
                var prevSegmentSizes = bases.Last().segments;
                for (var j = 0; j < 3; j++)
                {
                    var s = new Vector2Int(
                        Random.Range(Math.Max(1, prevSegmentSizes[j].x - 4), prevSegmentSizes[j].x + 1),
                        prevSegmentSizes[j].y);
                    segmentSizes.Add(s);
                }

                bases.Add((segmentSizes, maxY, n));
            }

            return bases;
        }
    }
}