﻿using System.Collections.Generic;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Roof;
using UnityEngine;

namespace BuildingGenerator.Creator
{
    [CreateAssetMenu(fileName = "StraightSkeletonRoofRandomizer", menuName = "BuildingGeneration/Randomizer/RoofGenerator/StraightSkeletonRoofRandomizer")]
    public class StraightSkeletonRoofRandomizer: RoofGeneratorRandomizer
    {
        [SerializeField] private HippedRoofGenerator roofGenerator;
        [MinMaxSlider(10, 80)] [SerializeField] private Vector2 roofPitch = new Vector2(30, 50);
        [MinMaxSlider(0, 2)] [SerializeField] private Vector2Int chimneyCount = new Vector2Int(0, 2);
        [MinMaxSlider(2, 5)] [SerializeField] private Vector2Int chimneyHeight = new Vector2Int(2, 5);

        
        [SerializeField] [ExposeMaterials("roofGenerator")] private MaterialContainerRandomizer materialContainerRandomizer;
        
        public override RoofGenerator CreateRoofGenerator()
        {
            var createdRoofGenerator = Instantiate(roofGenerator);

            roofGenerator.RoofPitch = Random.Range(roofPitch.x, roofPitch.y);
            roofGenerator.ChimneyCount = Random.Range(chimneyCount.x, chimneyCount.y);
            roofGenerator.ChimneyHeight = Random.Range(chimneyHeight.x, chimneyHeight.y);
            
            SetMaterials(createdRoofGenerator, materialContainerRandomizer);

            return createdRoofGenerator;
        }
        
        public override bool CanGenerate(List<Vector2> polygon) => roofGenerator.CanGenerate(polygon);
    }
}