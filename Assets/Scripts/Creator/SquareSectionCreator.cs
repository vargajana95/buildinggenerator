﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Base;
using BuildingGenerator.Common;
using BuildingGenerator.Facade;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BuildingGenerator.Creator
{
    [CreateAssetMenu(fileName = "SquareSectionCreator",
        menuName = "BuildingGeneration/Skyscraper/SquareSectionCreator", order = 1)]
    public class SquareSectionCreator : SkyscraperSectionCreator
    {
        public override List<Section> CreateSections(int minHeight, int maxHeight, FacadeGenerator facadeGenerator, Dictionary<string, Material> commonMaterials)
        {
            var remainingHeight = Random.Range(minHeight, maxHeight);
            var sectionCount = Random.Range(minSectionCount, maxSectionCount);
            var currentWidth = Random.Range(50f, 100f);
            Quaternion currentRotation = Quaternion.identity;

            var sections = new List<Section>();
            while (remainingHeight > 0 && sections.Count < sectionCount)
            {
                var sectionHeight = Random.Range(20, remainingHeight);
                var baseGenerator = CreateInstance<RectangleBaseGenerator>();

                currentWidth = currentWidth * Random.Range(0.2f, 0.8f);
                baseGenerator.Width = currentWidth;
                baseGenerator.Height = currentWidth;
                    
                var section = new Section()
                {
                    rotation = currentRotation,
                    height = sectionHeight,
                    baseGenerator = baseGenerator,
                    facadeGenerator = Instantiate(facadeGenerator),
                };

                currentRotation *= Quaternion.Euler(0, 45, 0);
                remainingHeight -= sectionHeight;
                
                sections.Add(section);
            }

            return sections;
        }
    }
}