﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Generators;
using BuildingGenerator.Roof;
using BuildingGenerator.Selectors;
using ProceduralToolkit;
using UnityEngine;
using UnityExtensions;

namespace BuildingGenerator.Creator
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(SkyscraperGenerator))]
    public class SkyscraperCreator : GeneratorBase
    {
   private SkyscraperGenerator skyscraperGenerator;

        [SerializeField] private int minHeight = 100;
        [SerializeField] private int maxHeight = 300;

        [SerializeField] private List<Material> sectionRoofMaterials;

        [SerializeField] [AssetReorderableList(hideFooterButtons = true)] [Expandable]
        private List<MaterialSelector> commonMaterials;

        [SerializeField] [AssetReorderableList(hideFooterButtons = true)] [Expandable] private List<SkyscraperSectionCreator> sectionCreators;
        [SerializeField] [AssetReorderableList(hideFooterButtons = true)] [Expandable] private List<FacadeGeneratorRandomizer> facadeGeneratorRandomizers;
        [SerializeField] [ReorderableList(hideFooterButtons = true)] private List<WeightedItem> roofGeneratorRandomizers;

        public override bool IsGeneratorReady => sectionCreators.Count > 0 && facadeGeneratorRandomizers.Count > 0 &&
                                                 roofGeneratorRandomizers.Count > 0;

        public List<SkyscraperSectionCreator> SectionCreators
        {
            get => sectionCreators;
            set => sectionCreators = value;
        }

        public List<FacadeGeneratorRandomizer> FacadeGeneratorRandomizers
        {
            get => facadeGeneratorRandomizers;
            set => facadeGeneratorRandomizers = value;
        }

        public List<WeightedItem> RoofGeneratorRandomizers
        {
            get => roofGeneratorRandomizers;
            set => roofGeneratorRandomizers = value;
        }

        private void OnEnable()
        {
            skyscraperGenerator = GetComponent<SkyscraperGenerator>();
            skyscraperGenerator.EnableGenerate = false;
        }

        private void OnDisable()
        {
            skyscraperGenerator.EnableGenerate = true;
        }

        public override void Generate()
        {
            RandomizeSkyscraperCreator();
            skyscraperGenerator.Generate();
        }

        public SkyscraperGenerator.Skyscraper GenerateSkyscraper()
        {
            RandomizeSkyscraperCreator();
            return skyscraperGenerator.GenerateSkyscraper();
        }

        private void RandomizeSkyscraperCreator()
        {
            skyscraperGenerator.SectionRoofMaterial = sectionRoofMaterials.GetRandom();

            var commonMaterialDictionary = GetCommonMaterials();

            var sectionCreator = GetSectionCreator();
            string groupName = "";
            if (sectionCreator.groupNames.Count > 0)
            {
                groupName = sectionCreator.groupNames.GetRandom();
            }
            var facadeGeneratorRandomizer = SelectFacadeGeneratorRandomizerForGroup(groupName);
            facadeGeneratorRandomizer.CommonMaterials = commonMaterialDictionary;
            
            skyscraperGenerator.sections =
                sectionCreator
                    .CreateSections(minHeight, maxHeight, facadeGeneratorRandomizer.CreateFacadePlanner(), commonMaterialDictionary);
            var roofGeneratorProxy = ScriptableObject.CreateInstance<RoofGeneratorProxy>();
            roofGeneratorProxy.RoofGeneratorSelectFunc = polygon =>
            {
                var roofGeneratorRandomizer = SelectRoofGeneratorRandomizerForGroup(groupName, polygon);
                roofGeneratorRandomizer.CommonMaterials = commonMaterialDictionary;
                return roofGeneratorRandomizer.CreateRoofGenerator();
            };
            skyscraperGenerator.RoofGenerator = roofGeneratorProxy;
        }
        
        private FacadeGeneratorRandomizer SelectFacadeGeneratorRandomizerForGroup(string groupName)
        {
            var possibleFacadeRandomizers = facadeGeneratorRandomizers
                .Where(f => string.IsNullOrEmpty(groupName) || f.groupNames.Count == 0 || f.groupNames.Any(g => string.IsNullOrEmpty(g) || g == groupName)).ToList();

            if (possibleFacadeRandomizers.Count == 0)
            {
                throw new ArgumentException($"Cannot get facadeGenerator for group: {groupName}");
            }
            return possibleFacadeRandomizers.GetRandom();
        }
        
        private RoofGeneratorRandomizer SelectRoofGeneratorRandomizerForGroup(string groupName, List<Vector2> polygon)
        {
            var roofRandomizers = roofGeneratorRandomizers.Select(r =>r.item).ToList();
            var possibleRoofGenerators = GetPossibleRoofGeneratorRandomizers(roofRandomizers, polygon).ToList();
            if (possibleRoofGenerators.Count == 0)
            {
                throw new ArgumentException("Cannot get roofGenerator for the current base polygon");
            }
            
            possibleRoofGenerators = possibleRoofGenerators
                .Where(r => string.IsNullOrEmpty(groupName) ||  r.groupNames.Count == 0 || r.groupNames.Any(g => string.IsNullOrEmpty(g) || g == groupName)).ToList();

            if (possibleRoofGenerators.Count == 0)
            {
                throw new ArgumentException($"Cannot get roofGenerator for group: {groupName}");
            }
            var weights = possibleRoofGenerators.Select(r => roofGeneratorRandomizers[roofRandomizers.IndexOf(r)].weight).ToArray();

            return possibleRoofGenerators.GetRandom(weights);
        }
        
        private IEnumerable<RoofGeneratorRandomizer> GetPossibleRoofGeneratorRandomizers(List<RoofGeneratorRandomizer> items, List<Vector2> polygon)
        {
            foreach (RoofGeneratorRandomizer roofGenerator in items)
            {
                if (roofGenerator.CanGenerate(polygon))
                {
                    yield return roofGenerator;
                }
            }
        }
        
        private SkyscraperSectionCreator GetSectionCreator()
        {
            return sectionCreators.GetRandom();
        }

        public Dictionary<string, Material> GetCommonMaterials()
        {
            var commonMaterialsDictionary = commonMaterials.ToDictionary(
                materialSelector => materialSelector.groupName,
                materialSelector => materialSelector.GetOne()
            );


            return commonMaterialsDictionary;
        }
    }
}