﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Facade;
using BuildingGenerator.Generators;
using BuildingGenerator.Selectors;
using ProceduralToolkit;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BuildingGenerator.Creator
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(HouseGenerator))]
    public class HouseGeneratorRandomizer : GeneratorBase
    {
        [MinMaxSlider(10, 60)] [SerializeField]
        private Vector2 facadeHeightRange = new Vector2(20, 30);

        [SerializeField] [AssetReorderableList(hideFooterButtons = true)] [Expandable]
        private List<MaterialSelector> commonMaterials;

        [Expandable] [SerializeField] [AssetReorderableList(hideFooterButtons = true)]  private List<FacadeGeneratorRandomizer> facadeGeneratorRandomizers;

        [Expandable] [SerializeField] [AssetReorderableList(hideFooterButtons = true)]
        private List<FacadeGeneratorRandomizer> additionalFacadeGeneratorRandomizers;

        [Expandable] [SerializeField] [AssetReorderableList(hideFooterButtons = true)]
        private List<FacadeSelectorCreator> facadeSelectorCreators;

        [Expandable] [SerializeField] [AssetReorderableList(hideFooterButtons = true)] private List<BaseGeneratorRandomizer> baseGeneratorRandomizers;
        [Expandable] [SerializeField] [AssetReorderableList(hideFooterButtons = true)]  private List<RoofGeneratorRandomizer> roofGeneratorRandomizers;

        private HouseGenerator houseGenerator;

        public override bool IsGeneratorReady => facadeGeneratorRandomizers.Count > 0 &&
                                                 baseGeneratorRandomizers.Count > 0 &&
                                                 roofGeneratorRandomizers.Count > 0;

        public List<FacadeGeneratorRandomizer> FacadeGeneratorRandomizers
        {
            get => facadeGeneratorRandomizers;
            set => facadeGeneratorRandomizers = value;
        }

        public List<BaseGeneratorRandomizer> BaseGeneratorRandomizers
        {
            get => baseGeneratorRandomizers;
            set => baseGeneratorRandomizers = value;
        }

        public List<RoofGeneratorRandomizer> RoofGeneratorRandomizers
        {
            get => roofGeneratorRandomizers;
            set => roofGeneratorRandomizers = value;
        }

        public Vector2 FacadeHeightRange
        {
            get => facadeHeightRange;
            set => facadeHeightRange = value;
        }

        private void OnEnable()
        {
            houseGenerator = GetComponent<HouseGenerator>();
            houseGenerator.EnableGenerate = false;
        }

        private void OnDisable()
        {
            houseGenerator.EnableGenerate = true;
        }


        public override void Generate()
        {
            RandomizeHouseGenerator();

            houseGenerator.Generate();
        }

        public House GenerateHouse()
        {
            RandomizeHouseGenerator();

            return houseGenerator.GenerateHouse();
        }

        private void RandomizeHouseGenerator()
        {
            houseGenerator.facadeHeight = Random.Range(facadeHeightRange.x, facadeHeightRange.y);
            var facadeGeneratorRandomizer = facadeGeneratorRandomizers.GetRandom();
            var commonMaterialsDictionary = GetCommonMaterials();
            facadeGeneratorRandomizer.CommonMaterials = commonMaterialsDictionary;

            var facadeSelectorCreator = facadeSelectorCreators.GetRandom();

            var additionalFacadePlannersCount = facadeSelectorCreator.AdditionalFacadePlannerCount;
            var additionalFacadePlanners = new List<FacadeGenerator>(additionalFacadePlannersCount);
            for (var i = 0; i < additionalFacadePlannersCount; i++)
            {
                var additionalFacadeGeneratorRandomizer = additionalFacadeGeneratorRandomizers.GetRandom();
                additionalFacadeGeneratorRandomizer.CommonMaterials = commonMaterialsDictionary;
                additionalFacadePlanners.Add(additionalFacadeGeneratorRandomizer.CreateFacadePlanner());
            }

            var mainFacadeGenerator = facadeGeneratorRandomizer.CreateFacadePlanner();
            houseGenerator.FacadeSelector = facadeSelectorCreator.CreateFacadeSelector(mainFacadeGenerator, additionalFacadePlanners);
            houseGenerator.BaseGenerator = baseGeneratorRandomizers.GetRandom().CreateBaseGenerator();
            var roofGeneratorRandomizer = roofGeneratorRandomizers.GetRandom();
            roofGeneratorRandomizer.CommonMaterials = commonMaterialsDictionary;
            houseGenerator.RoofGenerator = roofGeneratorRandomizer.CreateRoofGenerator();
        }

        public Dictionary<string, Material> GetCommonMaterials()
        {
            var commonMaterialsDictionary = commonMaterials.ToDictionary(
                materialSelector => materialSelector.groupName,
                materialSelector => materialSelector.GetOne()
            );


            return commonMaterialsDictionary;
        }
    }
}