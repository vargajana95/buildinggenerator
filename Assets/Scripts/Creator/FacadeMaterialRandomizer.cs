﻿using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Facade;
using UnityEngine;

namespace BuildingGenerator.Creator
{
        [CreateAssetMenu(fileName = "FacadeMaterialRandomizer", menuName = "BuildingGeneration/Randomizer/FacadePlanner/FacadeMaterialRandomizer")]
    public class FacadeMaterialRandomizer: FacadeGeneratorRandomizer
    {
            [SerializeField] [Expandable] private FacadeGenerator facadeGenerator;
        
            [SerializeField] [ExposeMaterials("facadeGenerator")] private MaterialContainerRandomizer materialContainerRandomizer;

            public override FacadeGenerator CreateFacadePlanner()
            {
                var createdFacadeGenerator = Instantiate(facadeGenerator);

                SetMaterials(createdFacadeGenerator, materialContainerRandomizer);

                return createdFacadeGenerator;            }
    }
}