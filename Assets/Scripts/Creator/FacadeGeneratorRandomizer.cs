﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BuildingGenerator.Facade;
using UnityEngine;
using UnityExtensions;

namespace BuildingGenerator.Creator
{
    public abstract class FacadeGeneratorRandomizer : ScriptableObject
    {
        [ReorderableList(hideFooterButtons = true)]
        public List<string> groupNames;

        public Dictionary<string, Material> CommonMaterials { get; set; } = new Dictionary<string, Material>();

        public abstract FacadeGenerator CreateFacadePlanner();

        protected void SetMaterials(FacadeGenerator facadeGenerator,
            MaterialContainerRandomizer materialContainerRandomizer /*, List<MaterialSelector> materialSelectors*/)
        {
            var materialSelectors = materialContainerRandomizer.fieldValues; /*.Select(f => f.value).ToList();*/

            var selectors = materialSelectors.Where(m => m.value != null).ToList();
            selectors.Sort((m1, m2) => m1.value.Order.CompareTo(m2.value.Order));

            var previousMaterials = new Dictionary<string, Material>(CommonMaterials);

            foreach (var materialSelector in selectors)
            {
                var material = materialSelector.value.GetOneOrFromPrevious(previousMaterials);
                previousMaterials[materialSelector.value.groupName] = material;
                var fieldInfo = facadeGenerator.Materials.GetType().GetField(materialSelector.propertyName,
                    BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                fieldInfo?.SetValue(facadeGenerator.Materials, material);
            }
        }
    }
}