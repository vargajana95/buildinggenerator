﻿using BuildingGenerator.Base;
using BuildingGenerator.Common.Attributes;
using UnityEngine;

namespace BuildingGenerator.Creator
{
    [CreateAssetMenu(fileName = "RectangleBaseGeneratorRandomizer", menuName = "BuildingGeneration/Randomizer/BaseGenerator/RectangleBaseGeneratorRandomizer")]
    public class RectangleBaseGeneratorRandomizer: BaseGeneratorRandomizer
    {
        [MinMaxSlider(0,100)] [SerializeField] private Vector2 widthRange = new Vector2(20,40);
        [MinMaxSlider(0,100)] [SerializeField] private Vector2 heightRange = new Vector2(20,40);


        public override BaseGenerator CreateBaseGenerator()
        {
            var rectangleBaseGenerator = CreateInstance<RectangleBaseGenerator>();
            rectangleBaseGenerator.Width = Random.Range(widthRange.x, widthRange.y);
            rectangleBaseGenerator.Height = Random.Range(heightRange.x, heightRange.y);

            return rectangleBaseGenerator;
        }
    }
}