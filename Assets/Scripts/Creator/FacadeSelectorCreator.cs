﻿using System.Collections.Generic;
using BuildingGenerator.Facade;
using UnityEngine;

namespace BuildingGenerator.Creator
{
    public abstract class FacadeSelectorCreator: ScriptableObject
    {
        public virtual int AdditionalFacadePlannerCount { get; protected set; }
        
        public abstract FacadeSelector CreateFacadeSelector(FacadeGenerator mainFacadePlanner, List<FacadeGenerator> additionalFacadePlanners);
    }
}