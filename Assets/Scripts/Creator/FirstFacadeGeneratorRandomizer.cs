﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Facade;
using ProceduralToolkit;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BuildingGenerator.Creator
{
    [CreateAssetMenu(fileName = "FirstFacadeGeneratorRandomizer",
        menuName = "BuildingGeneration/Randomizer/FacadePlanner/FirstFacadeGeneratorRandomizer")]
    public class FirstFacadeGeneratorRandomizer : FacadeGeneratorRandomizer
    {
        [SerializeField] protected FirstFacadePlanner facadePlanner;

        [MinMaxSlider(0, 10)] [SerializeField] private Vector2 groundFloorHeightRange = new Vector2(5, 6);
        [MinMaxSlider(0, 10)] [SerializeField] private Vector2 floorHeightRange = new Vector2(4, 5);
        [MinMaxSlider(0, 10)] [SerializeField] private Vector2 tileWidthRange = new Vector2(3, 4);
        [MinMaxSlider(0, 10)] [SerializeField] private Vector2 doorWidthRange = new Vector2(2, 3);
        [MinMaxSlider(0, 10)] [SerializeField] private Vector2 windowWidthRange = new Vector2(1, 2);
        [MinMaxSlider(0, 10)] [SerializeField] private Vector2 wallInsetRange = new Vector2(0, 1);

        [SerializeField] private List<Mesh> windowOrnamentTriangleMeshes;

        [SerializeField] [ExposeMaterials("facadePlanner")] private MaterialContainerRandomizer materialContainerRandomizer;


        // [SerializeField] [Expandable] private MaterialSelector wallMaterials;
        // [SerializeField] [Expandable] private MaterialSelector windowMaterials;
        // [SerializeField] [Expandable] private MaterialSelector windowLedgeMaterials;
        // [SerializeField] [Expandable] private MaterialSelector windowOrnamentMaterials;
        // [SerializeField] [Expandable] private MaterialSelector ledgeMaterials;
        // [SerializeField] [Expandable] private MaterialSelector glassMaterials;
        // [SerializeField] [Expandable] private MaterialSelector doorMaterials;
        // [SerializeField] [Expandable] private MaterialSelector doorFrameMaterials;
        // [SerializeField] [Expandable] private MaterialSelector balconyMaterials;

        public override FacadeGenerator CreateFacadePlanner()
        {
            //var facadeGenerator = CreateInstance<FirstFacadePlanner>();
            var facadeGenerator = Instantiate(facadePlanner);

            facadeGenerator.groundFloorHeight = Random.Range(groundFloorHeightRange.x, groundFloorHeightRange.y);
            facadeGenerator.floorHeight = Random.Range(floorHeightRange.x, floorHeightRange.y);
            facadeGenerator.tileWidth = Random.Range(tileWidthRange.x, tileWidthRange.y);
            facadeGenerator.doorWidth = Random.Range(doorWidthRange.x, doorWidthRange.y);
            facadeGenerator.windowWidth = Random.Range(windowWidthRange.x, windowWidthRange.y);
            facadeGenerator.wallInset = Random.Range(wallInsetRange.x, wallInsetRange.y);
            facadeGenerator.WindowOrnamentTriangleMesh = windowOrnamentTriangleMeshes.GetRandom();

            // if (wallMaterials)
            //     wallMaterials.FieldName = nameof(facadeGenerator.materials.wallMaterial);
            // if (windowMaterials)
            //     windowMaterials.FieldName = nameof(facadeGenerator.materials.windowFrameMaterial);
            // if (windowLedgeMaterials)
            //     windowLedgeMaterials.FieldName = nameof(facadeGenerator.materials.windowLedgeMaterial);
            // if (windowOrnamentMaterials)
            //     windowOrnamentMaterials.FieldName = nameof(facadeGenerator.materials.windowOrnamentMaterial);
            // if (ledgeMaterials)
            //     ledgeMaterials.FieldName = nameof(facadeGenerator.materials.ledgeMaterial);
            // if (doorMaterials)
            //     doorMaterials.FieldName = nameof(facadeGenerator.materials.doorMaterial);
            // if (balconyMaterials)
            //     balconyMaterials.FieldName = nameof(facadeGenerator.materials.balconyMaterial);
            // if (glassMaterials)
            //     glassMaterials.FieldName = nameof(facadeGenerator.materials.glassMaterial);
            // if (doorFrameMaterials)
            //     doorFrameMaterials.FieldName = nameof(facadeGenerator.materials.doorFrameMaterial);
            //
            // var materialSelectors = new List<MaterialSelector>
            // {
            //     wallMaterials,
            //     windowMaterials,
            //     windowLedgeMaterials,
            //     windowOrnamentMaterials,
            //     ledgeMaterials,
            //     glassMaterials,
            //     doorMaterials,
            //     balconyMaterials,
            //     doorFrameMaterials
            // };


            // SetMaterials(facadeGenerator, materialSelectors);
            SetMaterials(facadeGenerator, materialContainerRandomizer);
            return facadeGenerator;
        }
    }
}