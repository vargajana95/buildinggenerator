﻿using System.Collections.Generic;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Facade;
using ProceduralToolkit;
using UnityEngine;

namespace BuildingGenerator.Creator
{
    [CreateAssetMenu(fileName = "WrapperFacadeGeneratorRandomizer", menuName = "BuildingGeneration/Randomizer/FacadePlanner/WrapperFacadeGeneratorRandomizer")]
    public class WrapperFacadeGeneratorRandomizer: FacadeGeneratorRandomizer
    {
        [Expandable] [SerializeField] private FacadeGenerator facadePlanner;

        public FacadeGenerator FacadePlanner
        {
            get => facadePlanner;
            set => facadePlanner = value;
        }

        public override FacadeGenerator CreateFacadePlanner()
        {
            return facadePlanner;
        }
    }
}