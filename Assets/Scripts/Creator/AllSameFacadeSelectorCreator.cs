﻿using System.Collections.Generic;
using BuildingGenerator.Facade;
using UnityEngine;

namespace BuildingGenerator.Creator
{
    [CreateAssetMenu(fileName = "AllSameFacadeSelectorCreator",
        menuName = "BuildingGeneration/Randomizer/FacadeSelector/AllSameFacadeSelectorCreator")]
    public class AllSameFacadeSelectorCreator: FacadeSelectorCreator
    {
        public override FacadeSelector CreateFacadeSelector(FacadeGenerator mainFacadePlanner, List<FacadeGenerator> additionalFacadePlanners)
        {
            var facadeSelector = CreateInstance<AllSameFacadeSelector>();
            facadeSelector.name = "AllSameFacadeSelector";
            facadeSelector.MainFacadeGenerator = mainFacadePlanner;

            return facadeSelector;
        }
    }
}