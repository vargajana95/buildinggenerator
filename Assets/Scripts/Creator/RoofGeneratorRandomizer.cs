﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BuildingGenerator.Roof;
using UnityEngine;
using UnityExtensions;

namespace BuildingGenerator.Creator
{
    public abstract class RoofGeneratorRandomizer: ScriptableObject
    {
        [ReorderableList(hideFooterButtons = true)]
        public List<string> groupNames;
        
        public Dictionary<string, Material> CommonMaterials { get; set; } = new Dictionary<string, Material>();

        public abstract RoofGenerator CreateRoofGenerator();

        public abstract bool CanGenerate(List<Vector2> polygon);

        protected void SetMaterials(RoofGenerator roofGenerator, MaterialContainerRandomizer materialContainerRandomizer)
        {
            var materialSelectors = materialContainerRandomizer.fieldValues;/*.Select(f => f.value).ToList();*/
            
            var selectors = materialSelectors.Where(m => m.value != null).ToList();
            selectors.Sort((m1, m2) => m1.value.Order.CompareTo(m2.value.Order));

            var previousMaterials = new Dictionary<string, Material>(CommonMaterials);

            foreach (var materialSelector in selectors)
            {
                var material = materialSelector.value.GetOneOrFromPrevious(previousMaterials);
                previousMaterials[materialSelector.value.groupName] = material;
                var fieldInfo = roofGenerator.Materials.GetType().GetField(materialSelector.propertyName,
                    BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                fieldInfo?.SetValue(roofGenerator.Materials, material);
            }
        }
    }
}