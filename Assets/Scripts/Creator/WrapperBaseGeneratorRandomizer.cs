﻿using BuildingGenerator.Base;
using BuildingGenerator.Common.Attributes;
using UnityEngine;

namespace BuildingGenerator.Creator
{
    [CreateAssetMenu(fileName = "WrapperBaseGeneratorRandomizer", menuName = "BuildingGeneration/Randomizer/BaseGenerator/WrapperBaseGeneratorRandomizer")]
    public class WrapperBaseGeneratorRandomizer: BaseGeneratorRandomizer
    {
        [Expandable] [SerializeField] private BaseGenerator baseGenerator;

        public BaseGenerator BaseGenerator
        {
            get => baseGenerator;
            set => baseGenerator = value;
        }

        public override BaseGenerator CreateBaseGenerator()
        {
            return baseGenerator;
        }
    }
}