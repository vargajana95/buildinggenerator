﻿using BuildingGenerator.Base;
using UnityEngine;

namespace BuildingGenerator.Creator
{
    public abstract class BaseGeneratorRandomizer: ScriptableObject
    {
        public abstract BaseGenerator CreateBaseGenerator();
    }
}