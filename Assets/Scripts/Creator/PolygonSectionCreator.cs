﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Base;
using BuildingGenerator.Common;
using BuildingGenerator.Facade;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace BuildingGenerator.Creator
{
    [CreateAssetMenu(fileName = "PolygonSectionCreator",
        menuName = "BuildingGeneration/Skyscraper/PolygonSectionCreator", order = 1)]
    public class PolygonSectionCreator : SkyscraperSectionCreator
    {
        [SerializeField] private List<Vector2> polygon = new List<Vector2>
        {
            new Vector2(25, -25),
            new Vector2(-25, -25),
            new Vector2(-25, 25),
            new Vector2(25, 25)
        };

        public List<Vector2> Polygon
        {
            get => polygon;
            set => polygon = value;
        }

        public override List<Section> CreateSections(int minHeight, int maxHeight, FacadeGenerator facadeGenerator, Dictionary<string, Material> commonMaterials)
        {
            var baseGenerator = CreateInstance<PolygonBaseGenerator>();
            baseGenerator.Polygon = Polygon;

            var sections = new List<Section>
            {
                new Section
                {
                    height = Random.Range(minHeight, maxHeight),
                    facadeGenerator = Instantiate(facadeGenerator),
                    baseGenerator = baseGenerator
                }
            };

            return sections;
        }
    }
}