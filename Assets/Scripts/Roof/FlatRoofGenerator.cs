﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Helpers;
using ProceduralToolkit;
using UnityEngine;
using CompoundMeshDraft = BuildingGenerator.Common.CompoundMeshDraft;
using MeshDraft = BuildingGenerator.Common.MeshDraft;
using Random = UnityEngine.Random;

namespace BuildingGenerator.Roof
{
    [CreateAssetMenu(fileName = "FlatRoofGenerator", menuName = "BuildingGeneration/RoofGenerators/FlatRoofGenerator",
        order = 1)]
    public class FlatRoofGenerator : RoofGenerator
    {
        [NonSerialized] private float wallDepth = 1f;
        [NonSerialized] private float wallHeight = 1.5f;
        [NonSerialized] private float wallTopOverhang = 0.3f;
        [NonSerialized] private float wallTopHeight = 0.5f;

        [SerializeField] private int maxPropsCount;
        [SerializeField] private List<MeshMaterialBundle> props;

        [NonSerialized] private readonly List<Rect> placedPropRects = new List<Rect>();


        private const float textureWidth = 10;
        private const float textureHeight = 10;

        public MaterialContainer materialContainer;

        public override MaterialContainerBase Materials => materialContainer;

        [Serializable]
        public class MaterialContainer : MaterialContainerBase
        {
            [SerializeField] [ForMesh("RoofPlane")] 
            private Material roofPlaneMaterial;

            [SerializeField] [ForMesh("Walls")] [ForMesh("RoofBase", 0)] [ForMesh("RoofBase", 1)]
            private Material roofWallsMaterial;

        }

        protected override CompoundMeshDraft Generate()
        {
            placedPropRects.Clear();

            var roofBase = ConstructRoofBase(out var offsetPolygon);
            roofBase.name = "RoofBase";
            roofBase.RecalculateUV(textureWidth, textureHeight);
            var roofDraft = new CompoundMeshDraft().Add(roofBase);

            var wallsDraft = ConstructWalls(offsetPolygon, out var innerPolygon);
            roofDraft.Add(wallsDraft);

            var generatedProps = GenerateProps(innerPolygon);
            roofDraft.Add(generatedProps);

            var roofPlane = ConstructRoofPlane(offsetPolygon);

            return roofDraft.Add(roofPlane)
                .Paint(Color.white);
        }

        private MeshDraft ConstructRoofPlane(List<Vector2> offsetPolygon)
        {
            var roofPolygon3 = offsetPolygon.ConvertAll(v => v.ToVector3XZ());
            var tessellator = new Tessellator();
            tessellator.AddContour(roofPolygon3);
            tessellator.Tessellate(normal: Vector3.up);

            var roofPlane = new MeshDraft(tessellator.ToMeshDraft().ToMesh())
                .Move(Vector3.up * roofConfig.thickness);
            for (var i = 0; i < roofPlane.vertexCount; i++)
            {
                roofPlane.normals.Add(Vector3.up);
            }

            roofPlane.RecalculateUV(textureWidth, textureHeight);
            roofPlane.name = "RoofPlane";

            return roofPlane;
        }

        private CompoundMeshDraft GenerateProps(List<Vector2> contour)
        {
            if (contour.Count < 3)
            {
                return new CompoundMeshDraft();
            }

            var triangles = GetTriangles(contour);

            var propsMeshDraft = new CompoundMeshDraft();
            for (var i = 0; i < maxPropsCount; i++)
            {
                var propToPlace = RandomE.GetRandom(props);
                if (propToPlace == null || propToPlace.mesh == null)
                {
                    continue;
                }
                //Try 5 times to get a position which is not occupied
                for (var j = 0; j < 5; j++)
                {
                    var rotation = Quaternion.Euler(0, Random.Range(0, 4) * 90, 0);
                    var position = Utils.GetRandomPointInTriangle(RandomE.GetRandom(triangles));
                    var rotatedMin = rotation * propToPlace.mesh.bounds.min;
                    var rotatedMax = rotation * propToPlace.mesh.bounds.max;
                    var boundsMin = new Vector2(Math.Min(rotatedMin.x, rotatedMax.x),
                        Math.Min(rotatedMin.z, rotatedMax.z));
                    var boundsMax = new Vector2(Math.Max(rotatedMin.x, rotatedMax.x),
                        Math.Max(rotatedMin.z, rotatedMax.z));

                    var propRect = new Rect(position.x + boundsMin.x, position.y + boundsMin.y,
                        boundsMax.x - boundsMin.x, boundsMax.y - boundsMin.y);

                    if (!Utils.PolygonIsInsidePolygon(contour, propRect.GetCorners()))
                    {
                        continue;
                    }

                    if (!IsRectOccupied(propRect))
                    {
                        var propDraft = new MeshDraft(propToPlace.mesh);
                        materialContainer.AvailableMaterials[propDraft.name] = propToPlace.materials;

                        propsMeshDraft.Add(propDraft.Rotate(rotation).Move(position.ToVector3XZ()));
                        placedPropRects.Add(propRect);
                        break;
                    }
                }
            }

            propsMeshDraft.Move(Vector3.up * roofConfig.thickness);

            return propsMeshDraft;
        }

        private bool IsRectOccupied(Rect rect)
        {
            foreach (var placedPropRect in placedPropRects)
            {
                if (placedPropRect.Overlaps(rect))
                {
                    return true;
                }
            }

            return false;
        }

        private MeshDraft ConstructWalls(List<Vector2> roofPolygon, out List<Vector2> innerPolygon)
        {
            var wallsMesh = new CompoundMeshDraft();
            var pathOffsetter = new PathOffsetter();
            pathOffsetter.AddPath(roofPolygon);
            var innerPolygons = new List<List<Vector2>>();
            pathOffsetter.Offset(ref innerPolygons, -wallDepth);

            foreach (var innerPoly in innerPolygons)
            {
                innerPoly.Reverse();

                var wallBase = ConstructWallBase(roofPolygon, innerPoly);
                wallsMesh.Add(wallBase);

                var wallTop = ConstructWallTop(roofPolygon, innerPoly);
                wallsMesh.Add(wallTop.Move(Vector3.up * wallHeight));
            }

            if (innerPolygons.Count == 0)
            {
                innerPolygon = new List<Vector2>();
            }
            else
            {
                innerPolygon = innerPolygons[0];
            }


            var wallsMeshDraft = wallsMesh.ToMeshDraft();
            wallsMeshDraft.Move(Vector3.up * roofConfig.thickness);
            wallsMeshDraft.name = "Walls";
            wallsMeshDraft.RecalculateUV(textureWidth,textureHeight);
            return wallsMeshDraft;
        }

        private MeshDraft ConstructWallTop(List<Vector2> roofPolygon, List<Vector2> innerPolygon)
        {
            var outerPolygon = Geometry.OffsetPolygon(roofPolygon, wallTopOverhang);

            var wallTop = new MeshDraft();

            wallTop.AddFlatQuadBand(outerPolygon.ToVector3XZList(),
                outerPolygon.ToVector3XZList().Move(Vector3.up * wallTopHeight), true);

            wallTop.AddFlatQuadBand(innerPolygon.ToVector3XZList().Move(Vector3.up * wallTopHeight),
                innerPolygon.ToVector3XZList(), true);

            //todo this is not working properly. Should create the top with another method
            var innerPoly = Geometry.OffsetPolygon(roofPolygon, -wallDepth);

            wallTop.Add(new MeshDraft()
                .AddFlatQuadBand(roofPolygon.ToVector3XZList(),
                    outerPolygon.ToVector3XZList(), true));

            wallTop.Add(new MeshDraft()
                .AddFlatQuadBand(outerPolygon.ToVector3XZList(),
                    innerPoly.ToVector3XZList(), true).Move(Vector3.up * wallTopHeight));

            return wallTop;
        }

        private MeshDraft ConstructWallBase(List<Vector2> roofPolygon, List<Vector2> offsetPolygon)
        {
            var wallsMesh = new MeshDraft();
            wallsMesh.AddFlatQuadBand(roofPolygon.ToVector3XZList(),
                roofPolygon.ToVector3XZList().Move(Vector3.up * wallHeight), true);

            wallsMesh.AddFlatQuadBand(
                offsetPolygon.ToVector3XZList().Move(Vector3.up * wallHeight), offsetPolygon.ToVector3XZList(), true);

            return wallsMesh;
        }

        private List<List<Vector2>> GetTriangles(List<Vector2> polygon)
        {
            var tessellator = new Tessellator();
            tessellator.AddContour(polygon.ToVector3XZList());
            tessellator.Tessellate(normal: Vector3.up);

            List<List<Vector2>> triangles = new List<List<Vector2>>();
            for (var i = 0; i < tessellator.indices.Length; i += 3)
            {
                var v1 = tessellator.vertices[tessellator.indices[i]].Position;
                var v2 = tessellator.vertices[tessellator.indices[i + 1]].Position;
                var v3 = tessellator.vertices[tessellator.indices[i + 2]].Position;
                triangles.Add(new List<Vector2>()
                {
                    new Vector2(v1.X, v1.Z),
                    new Vector2(v2.X, v2.Z),
                    new Vector2(v3.X, v3.Z),
                });
            }

            return triangles;
        }
    }
}