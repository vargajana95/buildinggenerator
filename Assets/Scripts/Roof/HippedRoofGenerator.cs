﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Helpers;
using BuildingGenerator.Common.Layout;
using BuildingGenerator.Facade;
using ProceduralToolkit;
using ProceduralToolkit.Skeleton;
using UnityEngine;
using static BuildingGenerator.Common.Layout.LayoutBuilderFunctions;
using CompoundMeshDraft = BuildingGenerator.Common.CompoundMeshDraft;
using MeshDraft = BuildingGenerator.Common.MeshDraft;

namespace BuildingGenerator.Roof
{
    [CreateAssetMenu(fileName = "StraightSkeletonRoofGenerator",
        menuName = "BuildingGeneration/RoofGenerators/StraightSkeletonRoofGenerator", order = 1)]
    public class HippedRoofGenerator : RoofGenerator
    {
        [SerializeField] private float roofPitch = 50;
        [SerializeField] private float chimneyHeight = 2;
        [SerializeField] private int chimneyCount = 1;
        [SerializeField] private List<Mesh> chimneyTopMeshes;
        [SerializeField] private AtticConfig atticConfig;

        [SerializeField] private MaterialContainer materialContainer;

        public float RoofPitch
        {
            get => roofPitch;
            set => roofPitch = value;
        }

        public float ChimneyHeight
        {
            get => chimneyHeight;
            set => chimneyHeight = value;
        }

        public int ChimneyCount
        {
            get => chimneyCount;
            set => chimneyCount = value;
        }


        private StraightSkeleton straightSkeleton;

        public override MaterialContainerBase Materials => materialContainer;

        [Serializable]
        public class MaterialContainer : MaterialContainerBase
        {
            [SerializeField] [ForMesh("RoofTop")] [ForMesh("Attic", 4)]
            private Material roofTopMaterial;

            [SerializeField] [ForMesh("RoofBase", 0)] [ForMesh("RoofBase", 1)]
            private Material roofBaseMaterial;

            [SerializeField] [ForMesh("Chimneys", 1)]
            private Material chimneyMaterial;

            [SerializeField] [ForMesh("Chimneys", 0)]
            private Material chimneyTopMaterial;

            [SerializeField] [ForMesh("Attic", 0)] [ForMesh("Attic", 1)]
            private Material atticWallMaterial;

            [SerializeField] [ForMesh("Attic", 2)] private Material windowFrameMaterial;

            [SerializeField] [ForMesh("Attic", 3)] private Material glassMaterial;
        }


        protected override CompoundMeshDraft Generate()
        {
            var roofDraft = new CompoundMeshDraft();

            var roofBase = ConstructRoofBase(out var basePolygonOffset);
            roofBase.name = "RoofBase";
            Utils.ApplyUVCoordinates(roofBase, 10, 10);
            roofDraft.Add(roofBase);

            CreateStraightSkeleton(basePolygonOffset);
            var roofTop = ConstructRoofTop(basePolygonOffset);
            roofDraft.Add(roofTop);

            var chimneys = CreatChimneys();
            chimneys.name = "Chimneys";
            roofDraft.Add(chimneys);


            var attics = GenerateAttics(GetMaxLengthSide(straightSkeleton.polygons));
            if (attics != null)
            {
                roofDraft.Add(attics);
            }

            return roofDraft.Paint(Color.white);
        }

        private MeshDraft CreatChimneys()
        {
            var chimneysMeshDraft = new CompoundMeshDraft();
            for (int i = 0; i < chimneyCount; i++)
            {
                var chimney = GenerateChimney();
                chimneysMeshDraft.Add(chimney);
            }

            chimneysMeshDraft.MergeDraftsWithTheSameName();
            var chimneys = chimneysMeshDraft.ToMeshDraftWithSubMeshes();
            Utils.ApplyUVCoordinates(chimneys, 10, 10);
            return chimneys;
        }

        private MeshDraft ConstructRoofTop(List<Vector2> roofPolygon2)
        {
            var roofTop = new MeshDraft {name = "RoofTop"};
            foreach (var skeletonPolygon2 in straightSkeleton.polygons)
            {
                roofTop.Add(ConstructRoofTopSide(skeletonPolygon2, roofPitch));
            }

            Utils.ApplyUVCoordinates(roofTop, 10, 10);

            roofTop.Move(Vector3.up * roofConfig.thickness);
            roofTop.name = "RoofTop";
            return roofTop;
        }

        private void CreateStraightSkeleton(List<Vector2> roofPolygon2)
        {
            var skeletonGenerator = new StraightSkeletonGenerator();
            straightSkeleton = skeletonGenerator.Generate(roofPolygon2);
        }

        private static MeshDraft ConstructRoofTopSide(List<Vector2> skeletonPolygon2, float roofPitch)
        {
            Vector2 edgeA = skeletonPolygon2[0];
            Vector2 edgeB = skeletonPolygon2[1];
            Vector2 edgeDirection2 = (edgeB - edgeA).normalized;
            Vector3 roofNormal = CalculateRoofNormal(edgeDirection2, roofPitch);

            var skeletonPolygon3 = skeletonPolygon2.ConvertAll(v => v.ToVector3XZ());

            var tessellator = new Tessellator();
            tessellator.AddContour(skeletonPolygon3);
            tessellator.Tessellate(normal: Vector3.up);
            var contourDraft = new MeshDraft().Add(tessellator.ToMeshDraft());

            for (var i = 0; i < contourDraft.vertexCount; i++)
            {
                Vector2 vertex = contourDraft.vertices[i].ToVector2XZ();
                float height = CalculateVertexHeight(vertex, edgeA, edgeDirection2, roofPitch);
                contourDraft.vertices[i] = new Vector3(vertex.x, height, vertex.y);
                contourDraft.normals.Add(roofNormal);
            }

            return contourDraft;
        }

        private static Vector3 CalculateRoofNormal(Vector2 edgeDirection2, float roofPitch)
        {
            return Quaternion.AngleAxis(roofPitch, edgeDirection2.ToVector3XZ()) * Vector3.up;
        }


        private CompoundMeshDraft GenerateChimney()
        {
            var skeletonPolygons = straightSkeleton.polygons;
            var chimneyMeshDraft = new CompoundMeshDraft();
            var skeletonPolygon2 = RandomE.GetRandom(skeletonPolygons);

            Vector2 edgeA = skeletonPolygon2[0];
            Vector2 edgeB = skeletonPolygon2[1];
            Vector2 edgeDirection2 = (edgeB - edgeA).normalized;

            var skeletonPolygon3 = skeletonPolygon2.ConvertAll(v => v.ToVector3XZ());

            var tessellator = new Tessellator();
            tessellator.AddContour(skeletonPolygon3);
            tessellator.Tessellate(normal: Vector3.up);
            var triangles = GetTriangles(tessellator);

            for (var i = 0; i < 100; i++)
            {
                //Try it 100 times
                var chimneyOrigin = Utils.GetRandomPointInTriangle(RandomE.GetRandom(triangles));
                var chimneyWidth = UnityEngine.Random.Range(1f, 3);
                var chimneyRect = new Rect(chimneyOrigin.x - chimneyWidth / 2, chimneyOrigin.y - chimneyWidth / 2,
                    chimneyWidth, chimneyWidth);

                if (!Utils.IsRectInPolygon(skeletonPolygon2, chimneyRect))
                {
                    continue;
                }
                
                var chimneyFullHeight =
                    chimneyHeight + CalculateVertexHeight(chimneyOrigin, edgeA, edgeDirection2, roofPitch);
                var chimneyTopMesh = RandomE.GetRandom(chimneyTopMeshes);
                var chimneyTopMeshDraft = new MeshDraft(chimneyTopMesh) {name = "ChimneyTop"};
                var scaleFactor = chimneyWidth / chimneyTopMesh.bounds.size.x;
                chimneyMeshDraft.Add(chimneyTopMeshDraft).Scale(scaleFactor + 0.2f)
                    .Move(Vector3.up * chimneyFullHeight / 2);
                var chimneySides = MeshDraft.PartialBox(Vector3.right * chimneyWidth,
                    Vector3.forward * chimneyWidth,
                    Vector3.up * chimneyFullHeight,
                    Directions.XAxis | Directions.ZAxis | Directions.Up, true);
                chimneySides.name = "ChimneySides";
                chimneyMeshDraft.Add(chimneySides)
                    .Move(chimneyOrigin.ToVector3XZ() + Vector3.up * (chimneyFullHeight / 2 + roofConfig.thickness))
                    .Paint(Color.white);
                break;
            }

            return chimneyMeshDraft;
        }

        private List<List<Vector2>> GetTriangles(Tessellator tessellator)
        {
            List<List<Vector2>> triangles = new List<List<Vector2>>();
            for (var i = 0; i < tessellator.indices.Length; i += 3)
            {
                var v1 = tessellator.vertices[tessellator.indices[i]].Position;
                var v2 = tessellator.vertices[tessellator.indices[i + 1]].Position;
                var v3 = tessellator.vertices[tessellator.indices[i + 2]].Position;
                triangles.Add(new List<Vector2>()
                {
                    new Vector2(v1.X, v1.Z),
                    new Vector2(v2.X, v2.Z),
                    new Vector2(v3.X, v3.Z),
                });
            }

            return triangles;
        }

        private List<Vector2> GetMaxLengthSide(List<List<Vector2>> polygons)
        {
            var max = 0;
            for (var i = 0; i < polygons.Count; i++)
            {
                if ((polygons[i][0] - polygons[i][1]).magnitude > (polygons[max][0] - polygons[max][1]).magnitude)
                {
                    max = i;
                }
            }

            return polygons[max];
        }
        
        private static float CalculateVertexHeight(Vector2 vertex, Vector2 edgeA, Vector2 edgeDirection, float roofPitch)
        {
            float distance = Distance.PointLine(vertex, edgeA, edgeDirection);
            return Mathf.Tan(roofPitch*Mathf.Deg2Rad)*distance;
        }

        private CompoundMeshDraft GenerateAttics(List<Vector2> polygon)
        {
            var roofHeight =
                CalculateVertexHeight(polygon[2], polygon[0], (polygon[1] - polygon[0]).normalized, roofPitch);
            var atticHeight = atticConfig.atticHeight + atticConfig.atticRoofHeight + atticConfig.atticRoofThickness;
            if (atticHeight + atticConfig.atticStartY >= roofHeight)
            {
                //The given window block is too high for this roof. Won't generate it.
                return null;
            }

            var minMax = GetAtticsMinMaxPoint(polygon, atticHeight);

            var edgeWidth = (minMax.max - minMax.min).magnitude;

            var windowBlockDepth =
                atticHeight /
                Mathf.Tan(roofPitch * Mathf.Deg2Rad) + atticConfig.atticOffset;


            var layoutBuilder = new LayoutBuilder()
            {
                Size = new Vector3(edgeWidth, atticHeight, windowBlockDepth + 2 * atticConfig.atticRoofOverhang)
            };

            var widthVector = Vector3.right * atticConfig.atticWidth;
            var depthVector = Vector3.forward * windowBlockDepth;
            var heightVector = Vector3.up * atticConfig.atticHeight;
            var atticWidth = atticConfig.atticWidth + 2 * atticConfig.atticRoofOverhang;
            var attic = GenerateAttic(widthVector, depthVector, heightVector)
                .ToMeshDraftWithSubMeshes();
            var draft = layoutBuilder
                .SplitX(
                    WithFloatingSize(atticConfig.atticPadding - atticConfig.atticMargin, None),
                    WithRepeat(
                        WithFloatingSize(atticConfig.atticMargin, None),
                        WithSize(atticWidth,
                            MeshDraft(attic,
                                Name("Attic"))),
                        WithFloatingSize(atticConfig.atticMargin, None)
                    ),
                    WithFloatingSize(atticConfig.atticPadding - atticConfig.atticMargin, None)
                ).Build();

            var forwardVector = (polygon[1] - polygon[0]).Perp().ToVector3XZ();
            var atticsStartX = Vector2.Dot(minMax.min - polygon[0], (polygon[1] - polygon[0]).normalized);
            draft.Move(Vector3.up * ( /*-atticHeight / 2 + */atticConfig.atticStartY + roofConfig.thickness)
                       + Vector3.right * (atticsStartX /*- atticWidth / 2*/)
                       - Vector3.forward * (windowBlockDepth - atticConfig.atticOffset - atticConfig.atticRoofOverhang
                                            + atticConfig.atticStartY /
                                            Mathf.Tan(roofPitch * Mathf.Deg2Rad)));

            draft.Rotate(Quaternion.LookRotation(forwardVector));
            draft.Move(polygon[0].ToVector3XZ());

            draft.MergeDraftsWithTheSameName();

            foreach (var meshDraft in draft)
            {
                Utils.ApplyUVCoordinates(meshDraft, 5, 5);
            }

            return draft;
        }

        private (Vector2 min, Vector2 max) GetAtticsMinMaxPoint(List<Vector2> polygon, float atticHeight)
        {
            var lineA = polygon[0] + (polygon[0] - polygon[1]).Perp().normalized * (
                atticHeight +
                atticConfig.atticStartY) * (1 / Mathf.Tan(roofPitch * Mathf.Deg2Rad));

            var segmentsIntersection =
                Utils.SegmentsIntersect(polygon[0], polygon.Last(), lineA, lineA + (polygon[1] - polygon[0]));

            var min = segmentsIntersection.intersectionPoint;
            var segmentsIntersection2 =
                Utils.SegmentsIntersect(polygon[1], polygon[2], lineA, lineA + (polygon[1] - polygon[0]));
            var max = segmentsIntersection2.intersectionPoint;

            return (min, max);
        }

        private CompoundMeshDraft GenerateAttic(Vector3 widthVector, Vector3 depthVector, Vector3 heightVector)
        {
            var attic = new CompoundMeshDraft();
            List<Vector2> windowBlockPolygon = new List<Vector2>()
            {
                -depthVector.ToVector2XZ() / 2 - widthVector.ToVector2XZ() / 2,
                depthVector.ToVector2XZ() / 2 - widthVector.ToVector2XZ() / 2,
                depthVector.ToVector2XZ() / 2 + widthVector.ToVector2XZ() / 2,
                -depthVector.ToVector2XZ() / 2 + widthVector.ToVector2XZ() / 2
            };
            ;
            var windowBlock = MeshDraft.PartialBox(widthVector,
                depthVector, heightVector, Directions.XAxis | Directions.Down, true);

            windowBlock.name = "AtticWall";
            attic.Add(windowBlock.Paint(Color.white));

            var windowBlockFacade = ConstructWindowBlockFacade();
            windowBlockFacade.Move(depthVector - widthVector / 2 - depthVector / 2 - heightVector / 2);
            attic.Add(windowBlockFacade.Paint(Color.white));

            var roof = ConstructWindowBlockRoof(windowBlockPolygon);
            roof.Move( /*currentWindowPos.ToVector3XZ() +*/ Vector3.up * atticConfig.atticHeight / 2);
            roof.name = "AtticRoof";
            attic.Add(roof.Paint(Color.red));

            foreach (var meshDraft in attic)
            {
                meshDraft.tangents.Clear();
            }

            attic.Move(-Vector3.up * (atticConfig.atticRoofHeight + atticConfig.atticRoofThickness) / 2 +
                       depthVector / 2);
            // attic.Move(Vector3.left * (atticConfig.atticRoofOverhang) / 2); 
            attic.MergeDraftsWithTheSameName();
            return attic;
        }

        private CompoundMeshDraft ConstructWindowBlockFacade()
        {
            var facade = atticConfig.atticFacadePlanner.Generate(atticConfig.atticWidth, atticConfig.atticHeight);

            return facade;
        }

        private MeshDraft ConstructWindowBlockRoof(List<Vector2> windowBlockPolygon)
        {
            var roof = new MeshDraft();
            var windowBlockRoofBase = ConstructWindowBlockRoofBase(windowBlockPolygon, atticConfig.atticRoofThickness,
                atticConfig.atticRoofOverhang, out var roofPolygon2);
            roof.Add(windowBlockRoofBase);

            var roofTop = new MeshDraft();

            var roofPolygon3 = roofPolygon2.ConvertAll(v => v.ToVector3XZ());

            roofTop.AddQuad(roofPolygon3[0], roofPolygon3[1],
                roofPolygon3[1] + Vector3.up * atticConfig.atticRoofHeight +
                Vector3.right * (atticConfig.atticWidth / 2 + atticConfig.atticRoofOverhang),
                roofPolygon3[0] + Vector3.up * atticConfig.atticRoofHeight +
                Vector3.right * (atticConfig.atticWidth / 2 + atticConfig.atticRoofOverhang), true);
            roofTop.AddQuad(roofPolygon3[2], roofPolygon3[3],
                roofPolygon3[3] + Vector3.up * atticConfig.atticRoofHeight -
                Vector3.right * (atticConfig.atticWidth / 2 + atticConfig.atticRoofOverhang),
                roofPolygon3[2] + Vector3.up * atticConfig.atticRoofHeight -
                Vector3.right * (atticConfig.atticWidth / 2 + atticConfig.atticRoofOverhang), true);

            roofTop.AddTriangle(roofPolygon3[1], roofPolygon3[2],
                roofPolygon3[1] + Vector3.up * atticConfig.atticRoofHeight +
                Vector3.right * (atticConfig.atticWidth / 2 + atticConfig.atticRoofOverhang), true);

            roofTop.Move(Vector3.up * atticConfig.atticRoofThickness);

            //TODO
            for (var i = 0; i < roofTop.vertices.Count; i++)
            {
                roofTop.uv.Add(Vector2.zero);
            }

            roof.Add(roofTop);

            return roof;
        }

        //TODO user own MeshDraft
        private MeshDraft ConstructWindowBlockRoofBase(List<Vector2> polygon, float thickness,
            float overhang, out List<Vector2> roofPolygon2)
        {
            roofPolygon2 = Geometry.OffsetPolygon(polygon, overhang);
            var roofPolygon3 = roofPolygon2.ConvertAll(v => v.ToVector3XZ());

            var roofDraft = new MeshDraft();
            if (thickness > 0)
            {
                roofDraft.Add(ConstructWindowBlockBorder(roofPolygon2, roofPolygon3, thickness));
            }

            if (overhang > 0)
            {
                roofDraft.Add(ConstructWindowBlockOverhang(polygon, roofPolygon3));
            }

            return roofDraft;
        }

        private MeshDraft ConstructWindowBlockBorder(List<Vector2> roofPolygon2,
            List<Vector3> roofPolygon3, float thickness)
        {
            List<Vector3> upperRing = roofPolygon2.ConvertAll(v => v.ToVector3XZ() + Vector3.up * thickness);
            return new MeshDraft().AddFlatQuadBand(roofPolygon3, upperRing, true);
        }

        private MeshDraft ConstructWindowBlockOverhang(List<Vector2> polygon,
            List<Vector3> roofPolygon3)
        {
            List<Vector3> lowerRing = polygon.ConvertAll(v => v.ToVector3XZ());
            return new MeshDraft().AddFlatQuadBand(lowerRing, roofPolygon3, true);
        }

        [Serializable]
        public class AtticConfig
        {
            public float atticWidth = 2f;
            public float atticHeight = 3f;
            public float atticOffset = 1f;
            public float atticPadding = 4f;
            public float atticMargin = 2.5f;
            public float atticStartY = 1.5f;
            public float atticRoofHeight = 1f;
            public float atticRoofOverhang = 0.15f;
            public float atticRoofThickness = 0.15f;
            public FacadeGenerator atticFacadePlanner;
        }
    }
}