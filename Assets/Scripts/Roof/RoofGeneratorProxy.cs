﻿using System;
using System.Collections.Generic;
using BuildingGenerator.Common;
using UnityEngine;

namespace BuildingGenerator.Roof
{
    public class RoofGeneratorProxy: RoofGenerator
    {
        public Func<List<Vector2>, RoofGenerator> RoofGeneratorSelectFunc { get; set; }

        public override MaterialContainerBase Materials => materialContainer;

        private MaterialContainerBase materialContainer;

        protected override CompoundMeshDraft Generate()
        {
            var roofGenerator = RoofGeneratorSelectFunc(basePolygon);
            materialContainer = roofGenerator.Materials;
            return roofGenerator.Generate(basePolygon);
        }
    }
}