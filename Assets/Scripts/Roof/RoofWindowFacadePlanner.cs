﻿using System;
using System.Collections.Generic;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Layout.V1;
using BuildingGenerator.Facade;
using UnityEngine;
using UnityEngine.WSA;
using Random = UnityEngine.Random;

namespace BuildingGenerator.Roof
{
    [CreateAssetMenu(fileName = "RoofWindowFacadePlanner", menuName = "BuildingGeneration/FacadePlanner/RoofWindowFacadePlanner")]
    public class RoofWindowFacadePlanner : FacadeGenerator
    {
        [SerializeField] private Mesh windowMesh;


        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var layoutBuilder = new LayoutBuilderV1()
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };

            return layoutBuilder.SplitX()
                .WithSize(0.1f, Wall)
                .WithFloatingSize(1, WindowTile)
                .WithSize(0.1f, Wall)
                .And()
                .Build();

        }
        private void WindowTile(LayoutBuilderV1 l)
        {
            l.SplitY()
                .WithSize(0.2f, Wall)
                .WithFloatingSize(1, Window)
                .WithSize(0.2f, Wall);
        }
        
        private void Window(LayoutBuilderV1 l)
        {
            l.Scale().Z(0.2f)
                .And()
                .Mesh(windowMesh, "Window");
        }
        
        private void Wall(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder.QuadPrimitive(Color.white, "Wall");
        }
    }
}