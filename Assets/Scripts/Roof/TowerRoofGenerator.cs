﻿using System;
using System.Collections.Generic;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Helpers;
using ProceduralToolkit;
using UnityEngine;
using CompoundMeshDraft = BuildingGenerator.Common.CompoundMeshDraft;
using MeshDraft = BuildingGenerator.Common.MeshDraft;

namespace BuildingGenerator.Roof
{
    [CreateAssetMenu(fileName = "TowerRoofGenerator",
        menuName = "BuildingGeneration/RoofGenerators/TowerRoofGenerator")]
    public class TowerRoofGenerator : RoofGenerator
    {
        [SerializeField] private Mesh towerMesh;

        [SerializeField] private float baseHeight = 2.5f;
        
        [SerializeField] private MaterialContainer materialContainer;
        
        public override MaterialContainerBase Materials => materialContainer;

        [Serializable]
        public class MaterialContainer : MaterialContainerBase
        {
            [SerializeField] [ForMesh("RoofBase", 0)] [ForMesh("RoofBase", 1)]
            private Material roofBaseMaterial;

            [SerializeField] [ForMesh("RoofPlain")]
            private Material roofPlainMaterial;
            
            [SerializeField] [ForMesh("TowerBase")]
            private Material towerBaseMaterial;

            [SerializeField] [ForMesh("Tower", 0)]
            private Material towerMaterial;
        }

        public override bool CanGenerate(List<Vector2> polygon)
        {
            if (polygon.Count < 4)
            {
                return false;
            }

            var center = Utils.GetPolygonCenter(polygon);
            var closesPointsToCenter = GetClosesPointsInPolygonToPoint(polygon, center);
            if (closesPointsToCenter.Count != 4)
            {
               // Debug.Log("Base is not symmetric!");
                return false;
            }

            var width = (closesPointsToCenter[2] - closesPointsToCenter[1]).magnitude;
            var depth = (closesPointsToCenter[1] - closesPointsToCenter[0]).magnitude;
            var sideSize = Mathf.Min(width, depth);

            if (sideSize * sideSize / Utils.PolygonArea(polygon) < 0.4f)
            {
                return false;
            }

            return true;
        }

        private List<Vector2> GetClosesPointsInPolygonToPoint(List<Vector2> polygon, Vector2 center)
        {
            var closestPoints = new List<Vector2>();
            var closestDistance = (polygon[0] - center).sqrMagnitude;
            foreach (var point in polygon)
            {
                if ((point - center).sqrMagnitude < closestDistance)
                {
                    closestPoints.Clear();
                    closestPoints.Add(point);
                    closestDistance = (point - center).sqrMagnitude;
                }
                else if (Mathf.Approximately((point - center).sqrMagnitude, closestDistance))
                {
                    closestPoints.Add(point);
                }
            }

            return closestPoints;
        }

        protected override CompoundMeshDraft Generate()
        {
            var roofBase = ConstructRoofBase(out var roofPolygon2);
            roofBase.name = "RoofBase";
            Utils.ApplyUVCoordinates(roofBase, 10, 10);
            var roofDraft = new CompoundMeshDraft().Add(roofBase);

            var roofPlain = ConstructRoofPlain(roofPolygon2);

            roofDraft.Add(roofPlain);

            var towerMeshDraft = ConstructTower();

            roofDraft.Add(towerMeshDraft);

            return roofDraft.Paint(Color.white);
        }

        private CompoundMeshDraft ConstructTower()
        {
            var center = Utils.GetPolygonCenter(basePolygon);
            var closesPointsToCenter = GetClosesPointsInPolygonToPoint(basePolygon, center);
            if (closesPointsToCenter.Count != 4)
            {
                Debug.LogError("Base is not symmetric!");
            }

            var towerRoofMeshDraft = new CompoundMeshDraft();
            var width = (closesPointsToCenter[2] - closesPointsToCenter[1]).magnitude;
            var depth = (closesPointsToCenter[1] - closesPointsToCenter[0]).magnitude;
            var sideSize = Mathf.Min(width, depth);
            var widthVector = (closesPointsToCenter[2] - closesPointsToCenter[1]).normalized.ToVector3XZ() * sideSize;
            var depthVector = (closesPointsToCenter[1] - closesPointsToCenter[0]).normalized.ToVector3XZ() * sideSize;
            var heightVector = Vector3.up * baseHeight;
            var towerBase =
                MeshDraft.PartialBox(widthVector, depthVector, heightVector, Directions.All & ~Directions.Down);
            towerBase.name = "TowerBase";
            Utils.ApplyUVCoordinates(towerBase, 10, 10);
            var scale = (sideSize / 2) / towerMesh.bounds.size.x;
            var towerMeshDraft = new MeshDraft(towerMesh).Scale(scale);
            towerMeshDraft.name = "Tower";
            towerRoofMeshDraft.Add(towerBase.Move(heightVector / 2 + Vector3.up * roofConfig.thickness));
            towerRoofMeshDraft.Add(towerMeshDraft.Move(heightVector + Vector3.up * roofConfig.thickness));

            return towerRoofMeshDraft.Move(center.ToVector3XZ());
        }

        private MeshDraft ConstructRoofPlain(List<Vector2> roofPolygon2)
        {
            var roofPolygon3 = roofPolygon2.ConvertAll(v => v.ToVector3XZ());

            var tessellator = new Tessellator();
            tessellator.AddContour(roofPolygon3);
            tessellator.Tessellate(normal: Vector3.up);

            var roofTop = new MeshDraft(tessellator.ToMeshDraft().ToMesh())
                .Move(Vector3.up * roofConfig.thickness);
            //todo remove this
            for (var i = 0; i < roofTop.vertexCount; i++)
            {
                roofTop.uv.Add(Vector2.zero);
                roofTop.normals.Add(Vector3.up);
            }

            roofTop.name = "RoofPlain";
            Utils.ApplyUVCoordinates(roofTop, 10, 10);
            return roofTop;
        }
    }
}