﻿using System;
using System.Collections.Generic;
using BuildingGenerator.Common;
using ProceduralToolkit;
using UnityEngine;
using CompoundMeshDraft = BuildingGenerator.Common.CompoundMeshDraft;
using MeshDraft = BuildingGenerator.Common.MeshDraft;

namespace BuildingGenerator.Roof
{
    public abstract class RoofGenerator: ScriptableObject
    {
        protected List<Vector2> basePolygon;
        [SerializeField] protected RoofConfig roofConfig;
        public virtual MaterialContainerBase Materials { get; } = new EmptyMaterialContainer();

        public virtual bool CanGenerate(List<Vector2> polygon)
        {
            return true;
        }
        
        protected MeshDraft ConstructRoofBase(out List<Vector2> roofPolygon2)
        {
            roofPolygon2 = Geometry.OffsetPolygon(basePolygon, roofConfig.overhang);
            var roofPolygon3 = roofPolygon2.ConvertAll(v => v.ToVector3XZ());

            var roofDraft = new MeshDraft();
            if (roofConfig.thickness > 0)
            {
                roofDraft.AddToSeparateSubMesh(ConstructBorder(roofPolygon3, roofConfig));
            }
            if (roofConfig.overhang > 0)
            {
                roofDraft.AddToSeparateSubMesh(ConstructOverhang(basePolygon, roofPolygon3));
            }
            return roofDraft;
        }

        private static MeshDraft ConstructBorder(List<Vector3> roofPolygon3, RoofConfig roofConfig)
        {
            List<Vector3> upperRing = roofPolygon3.ConvertAll(v => v + Vector3.up*roofConfig.thickness);
            return new MeshDraft().AddFlatQuadBand(roofPolygon3, upperRing, true);
        }

        private static MeshDraft ConstructOverhang(List<Vector2> foundationPolygon, List<Vector3> roofPolygon3)
        {
            List<Vector3> lowerRing = foundationPolygon.ConvertAll(v => v.ToVector3XZ());
            return new MeshDraft().AddFlatQuadBand(lowerRing, roofPolygon3, true);
        }
        
        public CompoundMeshDraft Generate(List<Vector2> basePolygon)
        {
            this.basePolygon = basePolygon;

            return Generate();
        }

        protected abstract CompoundMeshDraft Generate();
        
        [Serializable]
        public class RoofConfig
        {
            public float thickness;
            public float overhang;
        }
    }
}