﻿using System;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Layout;
using UnityEngine;
using static BuildingGenerator.Common.Layout.LayoutBuilderFunctions;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "SkyscraperFacadePlanner",
        menuName = "BuildingGeneration/FacadePlanner/SkyscraperFacadePlanner")]
    public class SkyscraperFacadePlanner : FacadeGenerator
    {
        [SerializeField] private MaterialContainer materialContainer;

        private const float floorHeight = 3f;
        private const float windowWidth = 2f;
        private const float windowBlockWidth = 12f;
        private const float windowFrameSize = 0.1f;

        public override MaterialContainerBase Materials => materialContainer;

        [Serializable]
        public class MaterialContainer : MaterialContainerBase
        {
            [SerializeField] [ForMesh("Wall")] private Material wallMaterial;
            [SerializeField] [ForMesh("Glass")] private Material glassMaterial;

            [SerializeField] [ForMesh("WindowFrame")]
            private Material windowFrameMaterial;
        }


        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var l = new LayoutBuilder()
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };

            return l
                .SetUV(5,5)
                .SplitY(
                    WithRepeat(
                    NoSpaceFallback(Wall),
                        WithFloatingSize(floorHeight, Floor)
                    )
                )
                .Build();
        }

        private void Floor(LayoutBuilder l)
        {
            l
                .SplitX(
                    WithRepeat(
                        WithFloatingSize(windowBlockWidth, WindowsBlock),
                        NoSpaceFallback(FallbackFloor)
                    )
                );
        }

        private void FallbackFloor(LayoutBuilder l)
        {
            l
                .SplitX(
                    WithRepeat(
                        NoSpaceFallback(
                            SplitX(
                                WithRepeat(
                                    NoSpaceFallback(Wall),
                                    WithFloatingSize(windowBlockWidth / 4, WindowsBlock)
                                )
                            )),
                        WithFloatingSize(windowBlockWidth / 2, WindowsBlock)
                    )
                );
        }

        private void WindowsBlock(LayoutBuilder l)
        {
            l
                .SplitY(
                    WithSize(0.5f, Wall),
                    WithFloatingSize(0.1f,
                        SplitX(
                            WithSize(0.5f, Wall),
                            WithRepeat(
                                WithFloatingSize(windowWidth, Windows)
                            ),
                            WithSize(0.5f, Wall)
                        )
                    ),
                    WithSize(0.5f, Wall)
                );
        }

        private void Windows(LayoutBuilder l)
        {
            l
                .Offset(windowFrameSize,
                    OffsetBottom(WindowFrame),
                    OffsetTop(WindowFrame),
                    OffsetLeft(WindowFrame),
                    OffsetRight(WindowFrame),
                    OffsetMiddle(WindowGlass)
                );
        }

        private void WindowGlass(LayoutBuilder l)
        {
            l
                .Extrude(-0.2f,
                    Left(WindowFrame),
                    Right(WindowFrame),
                    Top(WindowFrame),
                    Bottom(WindowFrame),
                    Back(Glass)
                );
        }

        private void Glass(LayoutBuilder l)
        {
            l
                .QuadPrimitive(Name("Glass"));
        }

        private void WindowFrame(LayoutBuilder l)
        {
            l
                .QuadPrimitive(Name("WindowFrame"));
        }

        private void Wall(LayoutBuilder l)
        {
            l
                .QuadPrimitive(Name("Wall"));
        }
    }
}