﻿using System;
using System.Collections.Generic;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Layout.V1;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "TemplateFacadePlanner",
        menuName = "BuildingGeneration/FacadePlanner/TemplateFacadePlanner", order = 10000)]
    public class TemplateFacadePlanner : FacadeGenerator
    {
        [SerializeField] private MaterialContainer materialContainer;

        public override MaterialContainerBase Materials => materialContainer;

        [Serializable]
        public class MaterialContainer: MaterialContainerBase
        {
            [SerializeField] [ForMesh("Wall")] private Material wallMaterial;
        }


        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var l = new LayoutBuilderV1()
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };


            return l
                .SetUV(10, 10)
                .Add(Wall)
                .Build();
        }


        private void Wall(LayoutBuilderV1 l)
        {
            l.QuadPrimitive(Color.white, "Wall");
        }
    }
}