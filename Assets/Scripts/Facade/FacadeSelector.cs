﻿using System.Collections.Generic;
using BuildingGenerator.Base;
using BuildingGenerator.Common.Attributes;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    public abstract class FacadeSelector : ScriptableObject
    {
        [SerializeField] [Expandable] private FacadeGenerator mainFacadeGenerator;

        protected BuildingBase buildingBase;

        public FacadeGenerator MainFacadeGenerator
        {
            get => mainFacadeGenerator;
            set => mainFacadeGenerator = value;
        }

        public List<FacadeGenerator> GetFacadeGenerators(BuildingBase buildingBase)
        {
            this.buildingBase = buildingBase;
            return GetFacadeGenerators();
        }

        protected abstract List<FacadeGenerator> GetFacadeGenerators();
    }
}