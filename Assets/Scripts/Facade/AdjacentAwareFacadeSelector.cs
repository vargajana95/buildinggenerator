﻿using System.Collections.Generic;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "AdjacentAwareFacadeSelector",
        menuName = "BuildingGeneration/FacadeSelectors/AdjacentAwareFacadeSelector")]
    public class AdjacentAwareFacadeSelector : FacadeSelector
    {
        [SerializeField] private FacadeGenerator facadePlannerForAdjacentSides;

        public FacadeGenerator FacadePlannerForAdjacentSides
        {
            get => facadePlannerForAdjacentSides;
            set => facadePlannerForAdjacentSides = value;
        }

        protected override List<FacadeGenerator> GetFacadeGenerators()
        {
            var facadeGenerators = new List<FacadeGenerator>();
            foreach (var side in buildingBase)
            {
                if (side.IsAdjacent)
                {
                    facadeGenerators.Add(facadePlannerForAdjacentSides);
                }
                else
                {
                    facadeGenerators.Add(MainFacadeGenerator);
                }
            }

            return facadeGenerators;
        }
    }
}