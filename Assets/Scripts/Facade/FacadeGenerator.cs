﻿using BuildingGenerator.Common;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    public abstract class FacadeGenerator : ScriptableObject
    {
        public abstract CompoundMeshDraft Generate(float facadeWidth, float facadeHeight);

        public virtual MaterialContainerBase Materials => new EmptyMaterialContainer();
    }
}