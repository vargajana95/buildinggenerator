﻿using System;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Layout;
using UnityEngine;
using static BuildingGenerator.Common.Layout.LayoutBuilderFunctions;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "SubstanceFacadePlanner",
        menuName = "BuildingGeneration/FacadePlanner/SubstanceFacadePlanner")]
    public class SubstanceFacadePlanner : FacadeGenerator
    {
        [SerializeField] private MaterialContainer materialContainer;
        [SerializeField] private Vector2 textureSize;
        [SerializeField] private Mesh doorArchMesh;

        public float groundFloorHeight = 5.5f;

        private const float tileWidth = 3.1f;
        private const float doorWidth = 2.1f;
        private const float wallInset = 0.4f;

        public override MaterialContainerBase Materials => materialContainer;

        [Serializable]
        public class MaterialContainer : MaterialContainerBase
        {
            [SerializeField] [ForMesh("Wall")] private Material material;
            [SerializeField] [ForMesh("Door")] private Material doorMaterial;

            [SerializeField] [ForMesh("DoorFrame")]
            private Material groundWallMaterial;
        }


        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var l = new LayoutBuilder()
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };

            return l
                .SplitY(
                    WithSize(groundFloorHeight, GroundFloor),
                    WithFloatingSize(1, Wall)
                )
                .Build();
        }

        private void GroundFloor(LayoutBuilder l)
        {
            l
                .SetUV(10,10)
                .SplitX(
                    WithSize(0.5f, GroundWall),
                    WithRepeat(
                        WithFloatingSize(tileWidth, Tile)
                    ),
                    WithSize(0.5f, GroundWall)
                );
        }


        private void Tile(LayoutBuilder l)
        {
            l.SplitX(
                WithFloatingSize(1, DoorFrame),
                WithSize(doorWidth, DoorTile),
                WithFloatingSize(1, DoorFrame)
            );
        }

        private void DoorTile(LayoutBuilder l)
        {
            l
                .SplitY(
                    WithFloatingSize(1, Door),
                    WithSize(l.Size.x / 2, Arcs),
                    WithSize(0.5f, DoorFrame)
                );
        }

        private void Door(LayoutBuilder l)
        {
            l
                .Translate(Z(-wallInset))
                .QuadPrimitive(Name("Door"));
        }

        private void Arcs(LayoutBuilder l)
        {
            l
                .Scale(Z(wallInset))
                .Translate(Z(-wallInset))
                .SplitX(
                    WithFloatingSize(1,
                        Rotate(0, 0, 90, new Vector3(0.5f, 0.5f, 0)),
                        Add(ArcAsset)
                    ),
                    WithFloatingSize(1, ArcAsset)
                )
                .QuadPrimitive(Name("Door"));
        }

        private void ArcAsset(LayoutBuilder l)
        {
            l
                .Mesh(doorArchMesh, false,
                    Name("DoorFrame"),
                    RecalculateUVs()
                );
        }

        private void DoorFrame(LayoutBuilder l)
        {
            l
                .Scale(Z(wallInset))
                .Translate(Z(-wallInset))
                .Extrude(wallInset,
                    Front(QuadPrimitive(Name("DoorFrame"))),
                    Left(QuadPrimitive(Name("DoorFrame"))),
                    Right(QuadPrimitive(Name("DoorFrame")))
                );
        }

        private void GroundWall(LayoutBuilder l)
        {
            l.QuadPrimitive(Name("DoorFrame"));
        }


        private void Wall(LayoutBuilder l)
        {
            var textureWidth = l.Size.x / (int) Math.Round(l.Size.x / textureSize.x, MidpointRounding.ToEven);
            var textureHeight = l.Size.y / (int) Math.Round(l.Size.y / textureSize.y, MidpointRounding.ToEven);
            l
                .SetUV(textureWidth, textureHeight)
                .QuadPrimitive(Name("Wall"));
        }
    }
}