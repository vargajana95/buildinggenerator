﻿using BuildingGenerator.Common;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "EmptyFacadePlanner",
        menuName = "BuildingGeneration/FacadePlanner/EmptyFacadePlanner", order = 10000)]
    public class EmptyFacadePlanner : FacadeGenerator
    {
        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            return new CompoundMeshDraft();
            
        }
    }
}