﻿using System;
using System.Collections.Generic;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Layout;
using UnityEngine;
using static BuildingGenerator.Common.Layout.LayoutBuilderFunctions;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "SecondFacadePlanner",
        menuName = "BuildingGeneration/FacadePlanner/SecondFacadePlanner", order = 1)]
    public class SecondFacadePlanner : FacadeGenerator
    {
        private const float sliceWidth = 3.1f;
        private const float floorHeight = 4.5f;
        private const float windowWidth = 2f;
        private const float windowHeight = 3.2f;
        private const float groundFloorHeight = 6f;

        [SerializeField] private Mesh windowMesh;
        [SerializeField] private Mesh escapeStairMesh;
        [SerializeField] private int stairPosition;

        [SerializeField] private MaterialContainer materialContainer;

        public override MaterialContainerBase Materials => materialContainer;

        [Serializable]
        public class MaterialContainer : MaterialContainerBase
        {
            [SerializeField] [ForMesh("Window")] Material windowFrameMaterial;

            [SerializeField] [ForMesh("Window", 1)]
            Material glassMaterial;

            [SerializeField] [ForMesh("Stair")] Material stairMaterial;

            [SerializeField] [ForMesh("Wall")] Material wallMaterial;
        }


        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var layoutBuilder = new LayoutBuilder()
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };

            return layoutBuilder
                .SetUV(10, 10)
                .SplitX(
                    WithFloatingSize(sliceWidth, Wall),
                    WithRepeat(
                        WithFloatingSize(sliceWidth, WindowSlice),
                        WithFloatingSize(sliceWidth, Wall)
                    )
                    // WithSize(sliceWidth, WindowSlice),
                    // WithFloatingSize(sliceWidth, SideWall)
                )
                .Build();
        }

        void WindowSlice(LayoutBuilder l)
        {
            l.SplitY(
                WithFloatingSize(0.1f, Wall),
                WithSize(groundFloorHeight, GroundFloor(l.Index)),
                WithRepeat(
                    WithSize(floorHeight, WindowTile(l.Index))
                ),
                WithFloatingSize(0.1f,  Wall),
                NoSpaceFallback(Wall)
            );
        }

        Rule GroundFloor(int n)
        {
            if (n / 2 == stairPosition)
            {
                return l => l.SplitY(
                        WithSize(4f, Wall),
                        WithSize(1.5f, GroundFloorWindowTile),
                        WithFloatingSize(1, Wall)
                    )
                    .Mesh(escapeStairMesh, false,
                        Name("Stair"),
                        Translate(RelativeY(-0.2f)),
                        Scale(
                            RelativeX(1.4f),
                            RelativeZ(2f),
                            RelativeY(0.9f)),
                        CenterX(),
                        RecalculateUVs()
                    );
            }

            return l => l.SplitY(
                WithSize(4f, Wall),
                WithSize(1.5f, GroundFloorWindowTile),
                WithFloatingSize(1, Wall)
            );
        }

        void GroundFloorWindowTile(LayoutBuilder l)
        {
            l.SplitX(
                WithFloatingSize(1, Wall),
                WithSize(windowWidth,
                    SplitY(
                        WithFloatingSize(1, Wall),
                        WithSize(1f, Window),
                        WithFloatingSize(1, Wall))
                ),
                WithFloatingSize(1, Wall)
            );
        }


        private Rule WindowTile(int sliceIndex)
        {
            return l =>
            {
                l.SplitX(
                    WithFloatingSize(0.1f, Wall),
                    WithSize(windowWidth,
                        SplitY(
                            WithFloatingSize(0.1f, Wall),
                            WithSize(windowHeight, Window),
                            WithFloatingSize(0.1f, Wall))
                    ),
                    WithFloatingSize(0.1f, Wall)
                );
                if (sliceIndex / 2 == stairPosition)
                {
                    l.Mesh(escapeStairMesh, false,
                        Name("Stair"),
                        Translate(
                            RelativeY(-0.7f)
                        ),
                        Scale(
                            RelativeX(1.4f),
                            RelativeZ(2f),
                            RelativeY(1.2f)
                        ),
                        CenterX(),
                        RecalculateUVs()
                    );
                }
            };
        }

        void Window(LayoutBuilder l)
        {
            l
                .Scale(Z(0.5f))
                .Mesh(windowMesh, false,
                    Name("Window"),
                    RecalculateUVs()
                );
        }

        void Wall(LayoutBuilder l)
        {
            l.QuadPrimitive(Name("Wall"));
        }
    }
}