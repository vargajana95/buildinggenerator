﻿using System;
using BuildingGenerator.Common.Attributes;
using ProceduralToolkit.Buildings;

namespace BuildingGenerator.Facade
{
    [Serializable]
    public class FacadeOverrideWrapper
    {
        [Expandable] public FacadeGenerator facadePlanner;
    }
}