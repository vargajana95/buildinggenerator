﻿using System;
using System.Collections.Generic;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Layout;
using UnityEngine;
using static BuildingGenerator.Common.Layout.LayoutBuilderFunctions;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "FifthFacadePlannerV2",
        menuName = "BuildingGeneration/FacadePlanner/FifthFacadePlannerV2")]
    public class FifthFacadePlannerV2 : FacadeGenerator
    {
        private const float blockWidth = 5f;
        private const float windowBlockExtrude = -0.8f;
        private const float balconyBlockExtrude = -2f;
        private const float tileHeight = 4.5f;
        private const float windowWidth = 2f;
        private const float windowHeight = 3.2f;
        private const float entranceDoorHeight = 5f;
        private const float balconyDoorHeight = 4f;

        [SerializeField] private Mesh windowMesh;
        [SerializeField] private Mesh windowMesh2;
        [SerializeField] private Mesh doorMesh;

        private const float balconyFenceOffset = 0.1f;
        private const float balconyBottomHeight = 0.2f;
        private const float balconyFenceHeight = 1.8f;

        [SerializeField] private MaterialContainer materialContainer;

        public override MaterialContainerBase Materials => materialContainer;

        [Serializable]
        public class MaterialContainer : MaterialContainerBase
        {
            [SerializeField] [ForMesh("Window1")] [ForMesh("Window2")]
            Material windowFrameMaterial;

            [SerializeField] [ForMesh("Window1", 1)] [ForMesh("Window2", 1)]
            Material glassMaterial;

            [SerializeField] [ForMesh("Door")] [ForMesh("Door", 1)]
            Material doorMaterial;

            [SerializeField] [ForMesh("Wall")] Material wallMaterial;
            [SerializeField] [ForMesh("Ledge")] Material ledgeMaterial;

            [SerializeField] [ForMesh("BalconyFence")]
            Material balconyFenceMaterial;

            [SerializeField] [ForMesh("BalconyBottom")]
            Material balconyBottomMaterial;

            [SerializeField] [ForMesh("EntranceBox")]
            Material entranceBoxMaterial;
        }

        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var layoutBuilder = new LayoutBuilder()
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };

            if (layoutBuilder.Size.y < tileHeight / 2 + tileHeight)
            {
                return layoutBuilder
                    .SetUV(10, 10)
                    .SplitX(
                        WithFloatingSize(1, Wall)
                    ).Build();
            }

            return layoutBuilder
                .SplitX(
                    NoSpaceFallback(SingleLedgeBlockFacade),
                    WithFloatingSize(blockWidth, WindowBlock),
                    WithFloatingSize(blockWidth, LedgeWindowBlock),
                    WithFloatingSize(blockWidth, WindowBlock),
                    WithRepeat(WithFloatingSize(blockWidth, BalconyBlock),
                        WithFloatingSize(blockWidth, WindowBlock)),
                    WithFloatingSize(blockWidth, LedgeWindowBlock),
                    WithFloatingSize(blockWidth, WindowBlock)
                )
                .SetUV(10, 10)
                .Build();
          
        }

        private void SingleLedgeBlockFacade(LayoutBuilder l)
        {
            l
                .SplitX(
                    NoSpaceFallback(WindowOnlyFacade),
                    WithFloatingSize(blockWidth, WindowBlock),
                    WithFloatingSize(blockWidth, LedgeWindowBlock),
                    WithFloatingSize(blockWidth, WindowBlock)
                );
        }

        private void WindowOnlyFacade(LayoutBuilder l)
        {
            l
                .SplitX(
                    WithRepeat(
                        NoSpaceFallback(Wall),
                        WithFloatingSize(blockWidth, WindowBlock)
                    )
                );
        }

        private void BalconyBlock(LayoutBuilder l)
        {
            l.Extrude(balconyBlockExtrude,
                Left(Wall),
                Right(Wall),
                Top(Wall),
                Back(Balconies)
            );
        }

        private void Balconies(LayoutBuilder l)
        {
            l.SplitY(
                WithSize(tileHeight + tileHeight / 2, Entrance),
                WithFloatingSize(1,
                    SplitY(
                        WithRepeat(WithFloatingSize(tileHeight, BalconyTile))
                    )
                )
            );
        }

        private void BalconyTile(LayoutBuilder l)
        {
            l.SplitX(
                    WithFloatingSize(1, Wall),
                    WithSize(windowWidth,
                        SplitY(
                            WithSize(balconyDoorHeight, Door),
                            WithFloatingSize(1, Wall)
                        )
                    ),
                    WithSize(windowWidth,
                        SplitY(WithFloatingSize(1, Wall),
                            WithSize(windowHeight, Window),
                            WithFloatingSize(1, Wall)
                        )
                    ),
                    WithFloatingSize(1, Wall)
                )
                .Add(Balcony);
        }

        private void Balcony(LayoutBuilder l)
        {
            l
                .Translate(Y(-l.Size.y))
                .SplitY(
                    WithFloatingSize(1, None),
                    WithSize(balconyBottomHeight, BalconyBottom)
                );
        }

        private void BalconyBottom(LayoutBuilder l)
        {
            l
                .Extrude(-balconyBlockExtrude - 0.2f,
                    Bottom(BalconyBottomMaterial),
                    Front(BalconyBottomMaterial),
                    Top(BalconyFences)
                );
        }

        private void BalconyFences(LayoutBuilder l)
        {
            l
                .Add(
                    Translate(Y(balconyFenceOffset)),
                    Scale(Y(0.2f)),
                    Add(BalconyFenceTop),
                    Translate(Z(balconyFenceHeight)),
                    Add(
                        SplitX(
                            WithRepeat(
                                WithFloatingSize(0.5f, BalconyFenceMiddle)
                            )
                        )
                    ),
                    Add(BalconyFenceBottom)
                )
                .Add(BalconyBottomMaterial);
        }

        private void BalconyFenceBottom(LayoutBuilder l)
        {
            l.Scale(
                    X(l.Size.x),
                    Z(0.2f))
                .BoxPrimitive(Name("BalconyFence")
                );
        }

        private void BalconyFenceTop(LayoutBuilder l)
        {
            l
                .BoxPrimitive(Name("BalconyFence"),
                    Scale(
                        Z(0.2f)
                    )
                );
        }

        private void BalconyFenceMiddle(LayoutBuilder l)
        {
            l
                .CylinderPrimitive(
                    Name("BalconyFence"),
                    Scale(
                        Y(0.15f),
                        X(0.15f),
                        Z(balconyFenceHeight)
                    ),
                    CenterX(),
                    CenterY()
                );
        }


        private void BalconyBottomMaterial(LayoutBuilder l)
        {
            l.QuadPrimitive(
                Name("BalconyBottom"),
                SetUV(5, 5)
            );
        }

        private void Entrance(LayoutBuilder l)
        {
            l
                .Add(EntranceDoors)
                .BoxPrimitive(
                    Name("EntranceBox"),
                    Translate(Y(l.Size.y - 0.8f - balconyBottomHeight)),
                    Scale(
                        Y(0.8f),
                        Z(-balconyBlockExtrude),
                        RelativeX(1.2f),
                        RelativeZ(1.5f)
                    ),
                    CenterX()
                );
        }

        private void EntranceDoors(LayoutBuilder l)
        {
            l.SplitX(
                WithFloatingSize(1, Wall),
                WithSize(windowWidth,
                    SplitY(
                        WithSize(entranceDoorHeight, Door),
                        WithFloatingSize(1, Wall)
                    )
                ),
                WithSize(windowWidth,
                    SplitY(
                        WithSize(entranceDoorHeight, Door),
                        WithFloatingSize(1, Wall)
                    )
                ),
                WithFloatingSize(1, Wall)
            );
        }

        private void Door(LayoutBuilder l)
        {
            l.Scale(Z(0.3f))
                .Mesh(doorMesh, false,
                    Name("Door"),
                    SetUV(1, 1)
                );
        }

        private void WindowBlock(LayoutBuilder l)
        {
            l.SplitY(
                WithSize(tileHeight / 2, Wall),
                WithFloatingSize(1,
                    SplitY(
                        WithRepeat(
                            WithFloatingSize(tileHeight, NormalWindowTile)
                        )
                    )
                )
            );
        }

        private void NormalWindowTile(LayoutBuilder l)
        {
            l.SplitX(
                // Replacement(
                //    Wall
                // ),
                WithFloatingSize(0.1f, Wall),
                WithSize(windowWidth,
                    SplitY(
                        WithFloatingSize(1, Wall),
                        WithSize(windowHeight, Window2),
                        WithFloatingSize(1, Wall)
                    )
                ),
                WithSize(windowWidth,
                    SplitY(
                        WithFloatingSize(1, Wall),
                        WithSize(windowHeight, Window2),
                        WithFloatingSize(1, Wall)
                    )
                ),
                WithFloatingSize(0.1f, Wall)
            );
        }

        private void Window(LayoutBuilder l)
        {
            l.Scale(Z(0.3f))
                .Mesh(windowMesh, false,
                    Name("Window1"),
                    SetUV(1, 1)
                );
        }

        private void Window2(LayoutBuilder l)
        {
            l.Scale(
                Z(0.3f)
            ).Mesh(windowMesh2, false,
                Name("Window2"),
                SetUV(1, 1)
            );
        }

        private void LedgeWindowBlock(LayoutBuilder l)
        {
            l.Extrude(windowBlockExtrude,
                Left(Wall),
                Right(Wall),
                Top(Wall),
                Back(LedgeWindows)
            );
        }

        private void LedgeWindows(LayoutBuilder l)
        {
            l
                .SplitY(
                    WithSize(tileHeight / 2, Wall),
                    WithFloatingSize(1, SplitY(
                            WithRepeat(
                                WithFloatingSize(tileHeight, LedgeNormalWindowTile)
                            )
                        )
                    )
                );
        }

        private void LedgeNormalWindowTile(LayoutBuilder l)
        {
            l.SplitY(
                WithFloatingSize(1, Wall),
                WithSize(windowHeight,
                    lb => lb.SplitX(
                        WithFloatingSize(1, Wall),
                        WithSize(windowWidth, Window),
                        WithSize(windowWidth, Window),
                        WithFloatingSize(1, Wall)
                    ).Add(WindowLedge)
                ),
                WithFloatingSize(1, Wall)
            );
        }

        private void WindowLedge(LayoutBuilder l)
        {
            l
                .Scale(
                    RelativeX(0.8f),
                    Y(0.2f),
                    Z(0.5f))
                .Translate(Y(-0.2f))
                .CenterX()
                .BoxPrimitive(
                    UseColor(Color.yellow),
                    Name("Ledge")
                );
        }

        private void Wall(LayoutBuilder l)
        {
            l.QuadPrimitive(
                Name("Wall")
            );
        }
    }
}