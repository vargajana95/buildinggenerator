﻿using BuildingGenerator.Common;
using BuildingGenerator.Common.Layout.V1;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "EscapeStairsFacadePlanner",
        menuName = "BuildingGeneration/FacadePlanner/EscapeStairsFacadePlanner", order = 6)]
    public class EscapeStairsFacadePlanner : FacadeGenerator
    {
        private const float blockWidth = 5f;
        private const float stairsBlockWidth = 8f;
        private const float stairBlockExtrude = -2f;
        private const float tileHeight = 4.5f;
        private const float windowWidth = 2f;
        private const float windowHeight = 3.2f;

        [SerializeField] private Mesh windowMesh;
        [SerializeField] private Mesh windowMesh2;
        [SerializeField] private Mesh escapeStairMesh;


        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var layoutBuilder = new LayoutBuilderV1()
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };

            return layoutBuilder
                .SetUV(10,10)
                .SplitX()
                .WithSize(blockWidth, WindowBlock)
                .WithFloatingSize(1, StairsBlock)
                .WithSize(blockWidth, WindowBlock)
                .Build();
        }

        private void StairsBlock(LayoutBuilderV1 l)
        {
            l.Extrude(stairBlockExtrude).Left(Wall).Right(Wall).Top(Wall).Back(a =>
            {
                a.SplitX()
                    .WithFloatingSize(1, Wall)
                    .WithSize(stairsBlockWidth, b =>
                    {
                        b.SplitY()
                            .WithRepeat().WithFloatingSize(tileHeight, StairsTile);
                    })
                    .WithFloatingSize(1, Wall);
            });
        }

        private void StairsTile(LayoutBuilderV1 l)
        {
            l.SplitY()
                .WithFloatingSize(1, Wall)
                .WithSize(windowHeight, a =>
                {
                    a.SplitX()
                        .WithFloatingSize(1, Wall)
                        .WithSize(windowWidth, Window2)
                        .WithSize(windowWidth, Window2)
                        .WithFloatingSize(1, Wall);
                    if (l.Index != 0)
                    {
                        a.Add(Stair);
                    }
                })
                .WithFloatingSize(1, Wall);
        }

        private void Stair(LayoutBuilderV1 l)
        {
            l.Translate().RelativeY(-1.3f).And().Scale().RelativeY(2.1f).RelativeZ(2).And().Mesh(escapeStairMesh);
        }

        private void WindowBlock(LayoutBuilderV1 l)
        {
            l.SplitY()
                .WithRepeat().WithFloatingSize(tileHeight, WindowTile);
        }

        private void WindowTile(LayoutBuilderV1 l)
        {
            l.SplitX()
                .WithFloatingSize(1, Wall)
                .WithSize(windowWidth,
                    a =>
                    {
                        a.SplitY().WithFloatingSize(1, Wall).WithSize(windowHeight, Window).WithFloatingSize(1, Wall);
                    })
                .WithFloatingSize(1, Wall);
        }


        private void Window2(LayoutBuilderV1 l)
        {
            l.Scale().Z(0.3f).And().Mesh(windowMesh2);
        }

        private void Window(LayoutBuilderV1 l)
        {
            l.Scale().Z(0.3f).And().Mesh(windowMesh);
        }

        private void Wall(LayoutBuilderV1 l)
        {
            l.QuadPrimitive(Color.white, "Wall");
        }
    }
}