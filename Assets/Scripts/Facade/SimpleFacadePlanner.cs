﻿using System.Collections.Generic;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Layout.V1;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "SimpleFacadePlanner",
        menuName = "BuildingGeneration/FacadePlanner/SimpleFacadePlanner", order = 10000)]
    public class SimpleFacadePlanner : FacadeGenerator
    {

        [SerializeField] private Mesh windowMesh;

        

        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var l = new LayoutBuilderV1()
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };

            
            
            return l.SplitY()
                .WithRepeat().WithFloatingSize(5f, Floor)
                .Build();
            
        }
        
        private void Floor(LayoutBuilderV1 l)
        {
            l.SplitX()
                .WithRepeat()
                .WithFloatingSize(1f, Wall)
                .WithSize(4f, Window)
                .WithFloatingSize(1f, Wall);
        }

       
        private void Window(LayoutBuilderV1 l)
        {
            l.Scale().Z(0.3f).And().Mesh(windowMesh);
        }

        private void Wall(LayoutBuilderV1 l)
        {
            l.QuadPrimitive(Color.white, "Wall");
        }
    }
}