﻿using System;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Layout.V1;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "FirstFacadePlanner", menuName = "BuildingGeneration/FacadePlanner/FirstFacadePlanner",
        order = 1)]
    public class FirstFacadePlanner : FacadeGenerator
    {
        public float groundFloorHeight { get; set; } = 5.5f;
        public float floorHeight { get; set; } = 4.5f;

        public float tileWidth { get; set; } = 3.1f;
        public float doorWidth { get; set; } = 2.1f;
        public float windowWidth { get; set; } = 1.4f;
        public float wallInset { get; set; } = 0.4f;

        [SerializeField] private Mesh windowMesh;
        [SerializeField] private Mesh windowOrnamentRoundMesh;
        [SerializeField] private Mesh windowOrnamentTriangleMesh;
        [SerializeField] private Mesh windowModMesh;
        [SerializeField] private Mesh doorArchMesh;
        [SerializeField] private Mesh ledgeAsset;

        private LayoutBuilderV1 layoutBuilder;

        public Mesh WindowOrnamentTriangleMesh
        {
            get => windowOrnamentTriangleMesh;
            set => windowOrnamentTriangleMesh = value;
        }

        public MaterialContainer materials;

        public override MaterialContainerBase Materials => materials;

        [Serializable]
        public class MaterialContainer : MaterialContainerBase
        {
            [ForMesh("Wall")] public Material wallMaterial;

            [ForMesh("DoorFrame")]
            public Material doorFrameMaterial;

            [ForMesh("Door")] public Material doorMaterial;

           [ForMesh("WindowOrnament")]
            public Material windowOrnamentMaterial;

            [ForMesh("DoorMod")] public Material doorModMaterial;
            [ForMesh("Ledge")] public Material ledgeMaterial;

            [ForMesh("WindowLedge")]
            public Material windowLedgeMaterial;

            [ForMesh("Balcony")] public Material balconyMaterial;

            [ForMesh("Window")]
            public Material windowFrameMaterial;
            [ForMesh("Window", 1)]
            public Material glassMaterial;
        }

        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            layoutBuilder = new LayoutBuilderV1()
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };

            return layoutBuilder
                .SetUV(10, 10)
                .SplitY()
                .WithSize(groundFloorHeight, a => Floor(a, a.Index))
                .WithSize(floorHeight, a => Floor(a, a.Index))
                .WithSize(floorHeight, a => Floor(a, a.Index))
                .WithRepeat()
                .WithFloatingSize(floorHeight, a => Floor(a, a.Index))
                .And()
                .WithSize(floorHeight, a => Floor(a, 999))
                .WithFloatingSize(0.1f, Wall)
                .WithSize(0.5f, a => a.Scale().Z(0.3f).And().Add(LedgeAsset))
                .And()
                .Build();
        }


        private void Floor(LayoutBuilderV1 layoutBuilder, int floorIndex)
        {
            if (layoutBuilder.Index == 0)
            {
                layoutBuilder.Add(a => SubFloor(a, floorIndex));
            }

            else if (layoutBuilder.Index == 2)
            {
                layoutBuilder.SplitY()
                    .WithFloatingSize(1, (a) => a.Add(b => SubFloor(b, floorIndex)).Add(Balcony))
                    .WithSize(0.5f, TopLedge);
            }
            else
            {
                layoutBuilder.SplitY()
                    .WithSize(1f, (a) => BottomLedge(a, floorIndex))
                    .WithFloatingSize(1, (a) => SubFloor(a, floorIndex))
                    .WithSize(0.5f, TopLedge);
            }
        }

        private void Balcony(LayoutBuilderV1 l)
        {
            l
                .SetUV(4, 4)
                .Scale().Y(2).Z(1).And()
                .Translate().Y(-0.3f)
                .And() //.BoxPrimitive(Color.green)
                .SplitY().WithSize(0.2f, BalconyBeams).WithSize(0.3f, BalconyFloor).WithSize(1f, RailingBox);
        }

        private void RailingBox(LayoutBuilderV1 l)
        {
            l.BoxPrimitive(Color.white, "Balcony");
        }

        private void BalconyFloor(LayoutBuilderV1 l)
        {
            l.BoxPrimitive(Color.white, "Balcony");
        }

        private void BalconyBeams(LayoutBuilderV1 l)
        {
            l.SplitX().WithRepeat().WithFloatingSize(0.4f,
                a => a.Scale().X(0.2f).RelativeZ(0.9f).And().CenterX().BoxPrimitive(Color.white, "Balcony"));
        }


        private void SubFloor(LayoutBuilderV1 layoutBuilder, int floorIndex)
        {
            Rule wallType;
            if (floorIndex == 0)
            {
                wallType = SolidWall;
            }
            else
            {
                wallType = Wall;
            }

            layoutBuilder.SplitX()
                .WithSize(0.5f, wallType)
                .WithRepeat().WithFloatingSize(tileWidth, a => Tile(a, floorIndex))
                .And()
                .WithSize(0.5f, wallType);
        }

        private void Tile(LayoutBuilderV1 layoutBuilder, int floorIndex)
        {
            if (floorIndex == 0)
            {
                layoutBuilder.SplitX()
                    .WithFloatingSize(1, SolidWall)
                    .WithSize(doorWidth, DoorTile)
                    .WithFloatingSize(1, SolidWall);
            }
            else
            {
                layoutBuilder.SplitX()
                    .WithFloatingSize(1, Wall)
                    .WithSize(windowWidth, a => WindowTile(a, floorIndex))
                    .WithFloatingSize(1, Wall);
            }
        }


        private void WindowTile(LayoutBuilderV1 layoutBuilder, int floorIndex)
        {
            if (floorIndex == 1 || floorIndex == 999)
            {
                layoutBuilder.Add(Window);
            }
            else if (floorIndex == 2)
            {
                layoutBuilder
                    .Add(Window)
                    .Add(WindowOrnamentRound);
            }
            else
            {
                layoutBuilder
                    .Add(Window)
                    .Add(WindowLedge)
                    .Add(WindowOrnamentTriangle);
            }
        }

        private void WindowOrnamentTriangle(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder.Translate().RelativeY(1f)
                .And()
                .Scale().RelativeX(1.7f).Y(1.2f).Z(0.3f)
                .And()
                .CenterX()
                .Mesh(windowOrnamentTriangleMesh, "WindowOrnament").KeepOriginalUVs(false);
        }

        private void WindowLedge(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder
                .Scale().RelativeX(1.5f).Y(0.2f).Z(0.1f)
                .And()
                .Translate().Y(-0.2f)
                .And()
                .CenterX()
                .BoxPrimitive(Color.white, "WindowLedge");
        }

        private void WindowOrnamentRound(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder.Translate().RelativeY(1f)
                .And()
                .Scale().RelativeX(1.7f).Y(1.2f).Z(0.4f)
                .And()
                .CenterX()
                .Mesh(windowOrnamentRoundMesh, "WindowOrnament", true)
                .KeepOriginalUVs(false)
                .And()
                .SplitX().WithFloatingSize(1, WindowMod).WithSize(windowWidth, layoutBuilder.None)
                .WithFloatingSize(1, WindowMod).And();
        }

        private void WindowMod(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder.Scale().X(0.2f).RelativeY(1.3f).RelativeZ(0.6f)
                .And()
                .Translate().RelativeY(-1.0f)
                .And()
                .CenterX()
                .Mesh(windowModMesh, "WindowOrnament").KeepOriginalUVs(false);
        }

        private void Window(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder.Scale()
                .Z(0.2f)
                .And()
                .Mesh(windowMesh, "Window").KeepOriginalUVs(false);
        }


        private void DoorTile(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder.SplitY().WithFloatingSize(1, Door).WithSize(layoutBuilder.Size.x / 2, Arcs)
                .WithSize(0.5f, ArcTop);
        }

        private void ArcTop(LayoutBuilderV1 l)
        {
            l
                .QuadPrimitive(Color.white, "DoorFrame")
                .And()
                .Scale().X(0.5f).Z(0.3f)
                .And()
                .CenterX()
                .Mesh(windowModMesh, "DoorMod").KeepOriginalUVs(false);
        }

        private void Arcs(LayoutBuilderV1 l)
        {
            l.Scale().Z(wallInset)
                .And()
                .Translate().Z(-wallInset)
                .And()
                //.BoxPrimitive(Color.green)
                .SplitX().WithFloatingSize(1, a => a.Rotate(0, 0, 90).Translate().X(a.Size.x).And().Add(ArcAsset))
                .WithFloatingSize(1, ArcAsset)
                .And()
                .QuadPrimitive(Color.grey, "Door");
        }

        private void ArcAsset(LayoutBuilderV1 l)
        {
            l
                .Mesh(doorArchMesh, "DoorFrame").KeepOriginalUVs(false);
        }


        private void Door(LayoutBuilderV1 l)
        {
            l.Translate().Z(-wallInset).And()
                .QuadPrimitive(Color.grey, "Door");
        }

        private void SolidWall(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder
                .Scale().Z(wallInset)
                .And()
                .Translate().Z(-wallInset)
                .And()
                .BoxPrimitive(Color.white, "DoorFrame");
        }

        private void Wall(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder.QuadPrimitive(Color.white, "Wall");
        }

        private void BottomLedge(LayoutBuilderV1 l, int floorIndex)
        {
            if (floorIndex == 1)
            {
                l.SplitY().WithFloatingSize(1, SolidWall)
                    .WithFloatingSize(1, a => a.Scale().Z(0.2f).And().Add(LedgeAsset));
            }
            else if (floorIndex == 999)
            {
                l.SplitY().WithFloatingSize(1, WallStripe)
                    .WithFloatingSize(1, a => a.Scale().Z(0.2f).And().Add(LedgeAsset));
            }
            else
            {
                l.Add(WallStripe);
            }
        }

        private void TopLedge(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder.Add(WallStripe);
        }

        private void LedgeAsset(LayoutBuilderV1 l)
        {
            l.Mesh(ledgeAsset, "Ledge").KeepOriginalUVs(false);
        }

        private void WallStripe(LayoutBuilderV1 l)
        {
            l.SplitX().WithSize(0.5f, Wall).WithFloatingSize(1, Wall).WithSize(0.5f, Wall);
        }
    }
}