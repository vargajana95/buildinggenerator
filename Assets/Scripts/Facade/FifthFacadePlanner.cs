﻿using System;
using System.Collections.Generic;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Layout.V1;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "FifthFacadePlanner",
        menuName = "BuildingGeneration/FacadePlanner/FifthFacadePlanner", order = 5)]
    public class FifthFacadePlanner : FacadeGenerator
    {
        private const float blockWidth = 5f;
        private const float windowBlockExtrude = -0.8f;
        private const float balconyBlockExtrude = -2f;
        private const float tileHeight = 4.5f;
        private const float windowWidth = 2f;
        private const float windowHeight = 3.2f;
        private const float entranceDoorHeight = 5f;
        private const float balconyDoorHeight = 4f;

        [SerializeField] private Mesh windowMesh;
        [SerializeField] private Mesh windowMesh2;
        [SerializeField] private Mesh doorMesh;

        private const float balconyFenceOffset = 0.1f;
        private const float balconyBottomHeight = 0.2f;
        private const float balconyFenceHeight = 1.8f;

        [SerializeField] private MaterialContainer materialContainer;

        public override MaterialContainerBase Materials => materialContainer;

        [Serializable]
        public class MaterialContainer : MaterialContainerBase
        {
            [SerializeField] [ForMesh("Window1")] List<Material> windowMaterials = new List<Material> {null, null};
            [SerializeField] [ForMesh("Window2")] List<Material> window2Materials = new List<Material> {null, null};
            [SerializeField] [ForMesh("Door")] List<Material> doorMaterials = new List<Material> {null, null};
            [SerializeField] [ForMesh("Wall")] Material wallMaterial;
            [SerializeField] [ForMesh("Ledge")] Material ledgeMaterial;

            [SerializeField] [ForMesh("BalconyFence")]
            Material balconyFenceMaterial;

            [SerializeField] [ForMesh("BalconyBottom")]
            Material balconyBottomMaterial;

            [SerializeField] [ForMesh("EntranceBox")]
            Material entranceBoxMaterial;
        }

        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var layoutBuilder = new LayoutBuilderV1()
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };

            if (layoutBuilder.Size.x >= blockWidth * 4.5f)
            {
                return layoutBuilder
                    .SetUV(10, 10)
                    .SplitX()
                    .WithFloatingSize(blockWidth, WindowBlock)
                    .WithFloatingSize(blockWidth, LedgeWindowBlock)
                    .WithFloatingSize(blockWidth, WindowBlock)
                    .WithRepeat().WithFloatingSize(blockWidth, BalconyBlock).WithFloatingSize(blockWidth, WindowBlock)
                    .And()
                    .WithFloatingSize(blockWidth, LedgeWindowBlock)
                    .WithFloatingSize(blockWidth, WindowBlock)
                    .Build();
            }

            if (layoutBuilder.Size.x >= blockWidth * 2.5f)
            {
                return layoutBuilder.SplitX()
                    .WithFloatingSize(blockWidth, WindowBlock)
                    .WithFloatingSize(blockWidth, LedgeWindowBlock)
                    .WithFloatingSize(blockWidth, WindowBlock)
                    .Build();
            }

            if (layoutBuilder.Size.x >= blockWidth)
            {
                return layoutBuilder.SplitX()
                    .WithRepeat().WithFloatingSize(blockWidth, WindowBlock)
                    .Build();
            }

            return layoutBuilder.SplitX()
                .WithFloatingSize(1, Wall)
                .Build();
        }

        private void BalconyBlock(LayoutBuilderV1 l)
        {
            l.Extrude(balconyBlockExtrude).Left(Wall).Right(Wall).Top(Wall).Back(Balconies);
        }

        private void Balconies(LayoutBuilderV1 l)
        {
            l.SplitY()
                .WithSize(tileHeight + tileHeight / 2, Entrance)
                .WithFloatingSize(1, (lb) =>
                {
                    lb.SplitY()
                        .WithRepeat().WithFloatingSize(tileHeight, BalconyTile);
                });
        }

        private void Entrance(LayoutBuilderV1 l)
        {
            l
                .Add(EntranceDoors)
                .Translate().Y(l.Size.y - 0.8f - balconyBottomHeight)
                .And()
                .Scale().Y(0.8f).Z(-balconyBlockExtrude)
                .And()
                .Scale().RelativeX(1.2f).RelativeZ(1.5f)
                .And()
                .CenterX()
                .BoxPrimitive(Color.white, "EntranceBox");
        }

        private void EntranceDoors(LayoutBuilderV1 l)
        {
            l.SplitX()
                .WithFloatingSize(1, Wall)
                .WithSize(windowWidth,
                    a => { a.SplitY().WithSize(entranceDoorHeight, Door).WithFloatingSize(1, Wall); })
                .WithSize(windowWidth,
                    a => { a.SplitY().WithSize(entranceDoorHeight, Door).WithFloatingSize(1, Wall); })
                .WithFloatingSize(1, Wall);
        }


        private void BalconyTile(LayoutBuilderV1 l)
        {
            l.SplitX()
                .WithFloatingSize(1, Wall)
                .WithSize(windowWidth,
                    a =>
                    {
                        a.SplitY()
                            .WithSize(balconyDoorHeight, Door)
                            .WithFloatingSize(1, Wall);
                    })
                .WithSize(windowWidth,
                    a =>
                    {
                        a.SplitY().WithFloatingSize(1, Wall).WithSize(windowHeight, Window).WithFloatingSize(1, Wall);
                    })
                .WithFloatingSize(1, Wall)
                .And().Add(Balcony);
        }

        private void Balcony(LayoutBuilderV1 l)
        {
            l
                .Translate().Y(-l.Size.y /*- balconyYOffset*/)
                .And()
                .SplitY()
                .WithFloatingSize(1, l.None)
                .WithSize(balconyBottomHeight, BalconyBottom);
        }

        private void BalconyBottom(LayoutBuilderV1 l)
        {
            l
                .Extrude(-balconyBlockExtrude - 0.2f)
                .Bottom(BalconyBottomMaterial)
                .Front(BalconyBottomMaterial)
                .Top(BalconyFences);
        }

        private void BalconyFences(LayoutBuilderV1 l)
        {
            l
                .Add(BalconyBottomMaterial)
                .Translate().Y(balconyFenceOffset)
                .And()
                .Scale().Y(0.2f)
                .And()
                .SplitX()
                .WithRepeat()
                .WithFloatingSize(0.5f, BalconyFenceMiddle)
                .And()
                .And()
                .Add(BalconyFenceBottom)
                .Translate()
                .Z(balconyFenceHeight)
                .And()
                .Add(BalconyFenceTop);
        }

        private void BalconyFenceBottom(LayoutBuilderV1 l)
        {
            l
                .BoxPrimitive(Color.white, "BalconyFence")
                .Scale().X(l.Size.x).Z(0.2f)
                .And()
                .CenterX();
        }

        private void BalconyFenceTop(LayoutBuilderV1 l)
        {
            l
                .BoxPrimitive(Color.white, "BalconyFence")
                .Scale().X(l.Size.x).Z(0.2f)
                .And()
                .CenterX();
        }

        private void BalconyFenceMiddle(LayoutBuilderV1 l)
        {
            l
                .Scale().Y(0.15f).X(0.15f).Z(balconyFenceHeight)
                .And()
                .CenterX()
                .CenterY()
                .CylinderPrimitive(Color.white, "BalconyFence");
        }


        private void BalconyBottomMaterial(LayoutBuilderV1 l)
        {
            l.QuadPrimitive(Color.white, "BalconyBottom").SetUV(5, 5);
        }

        private void LedgeWindowBlock(LayoutBuilderV1 l)
        {
            l.Extrude(windowBlockExtrude).Left(Wall).Right(Wall).Top(Wall).Back(LedgeWindows);
        }


        private void LedgeWindows(LayoutBuilderV1 l)
        {
            l
                .SplitY()
                .WithSize(tileHeight / 2, Wall)
                .WithFloatingSize(1, (lb) =>
                {
                    lb.SplitY()
                        .WithRepeat().WithFloatingSize(tileHeight, LedgeNormalWindowTile);
                });
        }

        private void LedgeNormalWindowTile(LayoutBuilderV1 l)
        {
            l.SplitY()
                .WithFloatingSize(1, Wall)
                .WithSize(windowHeight,
                    a =>
                    {
                        a.SplitX().WithFloatingSize(1, Wall).WithSize(windowWidth, Window).WithSize(windowWidth, Window)
                            .WithFloatingSize(1, Wall)
                            .And().Add(WindowLedge);
                    })
                .WithFloatingSize(1, Wall);
        }

        private void Door(LayoutBuilderV1 l)
        {
            l.Scale().Z(0.3f).And().Mesh(doorMesh, "Door").KeepOriginalUVs(false).SetUV(1, 1);
        }

        private void Window2(LayoutBuilderV1 l)
        {
            l.Scale().Z(0.3f).And().Mesh(windowMesh2, "Window2").KeepOriginalUVs(false).SetUV(1, 1);
        }

        private void WindowLedge(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder
                .Scale().RelativeX(0.8f).Y(0.2f).Z(0.5f)
                .And()
                .Translate().Y(-0.2f)
                .And()
                .CenterX()
                .BoxPrimitive(Color.white, "Ledge");
        }

        private void WindowBlock(LayoutBuilderV1 l)
        {
            l.SplitY()
                .WithSize(tileHeight / 2, Wall)
                .WithFloatingSize(1, (lb) =>
                {
                    lb.SplitY()
                        .WithRepeat().WithFloatingSize(tileHeight, NormalWindowTile);
                });
        }

        private void NormalWindowTile(LayoutBuilderV1 l)
        {
            l.SplitX()
                .WithFloatingSize(1, Wall)
                .WithSize(windowWidth,
                    a =>
                    {
                        a.SplitY().WithFloatingSize(1, Wall).WithSize(windowHeight, Window2).WithFloatingSize(1, Wall);
                    })
                .WithSize(windowWidth,
                    a =>
                    {
                        a.SplitY().WithFloatingSize(1, Wall).WithSize(windowHeight, Window2).WithFloatingSize(1, Wall);
                    })
                .WithFloatingSize(1, Wall);
        }

        private void Window(LayoutBuilderV1 l)
        {
            l.Scale().Z(0.3f).And().Mesh(windowMesh, "Window1").KeepOriginalUVs(false).SetUV(1, 1);
        }

        private void Wall(LayoutBuilderV1 l)
        {
            l.QuadPrimitive(Color.white, "Wall");
        }
    }
}