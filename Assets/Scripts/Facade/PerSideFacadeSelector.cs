﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "PerSideFacadeSelector",
        menuName = "BuildingGeneration/FacadeSelectors/PerSideFacadeSelector")]
    public class PerSideFacadeSelector : FacadeSelector
    {
        [Serializable]
        public class FacadeStringDictionary : SerializableDictionary<int, FacadeOverrideWrapper>
        {
        }
        
        public FacadeStringDictionary facadeGeneratorOverrides;

        protected override List<FacadeGenerator> GetFacadeGenerators()
        {
            var facadeGenerators = new List<FacadeGenerator>();
            foreach (var side in buildingBase)
            {
                if (facadeGeneratorOverrides.ContainsKey(side.Index))
                {
                    facadeGenerators.Add(facadeGeneratorOverrides[side.Index].facadePlanner);
                }
                else
                {
                    facadeGenerators.Add(MainFacadeGenerator);
                }
                
            }

            return facadeGenerators;
        }
    }
}