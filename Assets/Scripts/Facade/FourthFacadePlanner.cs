﻿using BuildingGenerator.Common.Layout.V1;
using ProceduralToolkit;
using UnityEngine;
using CompoundMeshDraft = BuildingGenerator.Common.CompoundMeshDraft;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "FourthFacadePlanner", menuName = "BuildingGeneration/FacadePlanner/FourthFacadePlanner")]
    public class FourthFacadePlanner : FacadeGenerator
    {
        [SerializeField] private float groundFloorHeight = 3.837827f;

        [SerializeField] private Mesh roundWindowMesh;
        [SerializeField] private Mesh partialArchMesh;
        [SerializeField] private Mesh fullArchMesh;
        [SerializeField] private Mesh pillarMesh;
        [SerializeField] private Mesh topLedgeMesh;

        [SerializeField] private float towerWidth = 2.994168f;
        [SerializeField] private float groundArchDiam = 1.531235f;

        private float facadeWidth;

        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var layoutBuilder = new LayoutBuilderV1()
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };

            this.facadeWidth = facadeWidth;

            return layoutBuilder
                .SplitX()
                .WithSize(towerWidth, Tower)
                .WithFloatingSize(1, a => Facade(a, 2))
                .WithSize(towerWidth, Tower)
                .And()
                .Build();
        }
        private void Tower(LayoutBuilderV1 l)
        {
            l.Extrude(0.5f)
                .Front(a => Facade(a, 1))
                .All(Wall, Directions.Left | Directions.Right | Directions.Up);
        }

        private const float bigRailHeight = 0.3f;

        private void Facade(LayoutBuilderV1 l, int n)
        {
            float midRailOffset = (facadeWidth - towerWidth * 2 - 1) * 0.5f;

            l.SplitY()
                .WithSize(groundFloorHeight, a => Ground(a, n))
                .WithSize(bigRailHeight,
                    a => a.Extrude(0.1f).All(Wall))
                .WithFloatingSize(3, a => One(a, n))
                .WithSize(bigRailHeight,
                    a => a.Extrude(0.1f).All(Wall))
                .WithSize(midRailOffset, a => Three(a, n))
                .WithSize(1.4f,
                    a => a.Scale().X(l.Size.x).Z(0.3f).And().Mesh(topLedgeMesh))
                .WithSize(2, a => Four(a, n));

        }
        
       
        
        private void Four(LayoutBuilderV1 l, int n)
        {
            if (n > 0)
            {
                l.Add(FourAll);
            }
            else
            {
                l.Add(Wall);
            }
        }
        
        private void FourAll(LayoutBuilderV1 l)
        {
            l.SplitY()
                .WithFloatingSize(1, Wall)
                .WithSize(1,
                    a => a.SplitX().WithRepeat().WithFloatingSize(0.7f, Wall).WithSize(1, RoundWindow).And()
                        .WithFloatingSize(0.7f, Wall))
                .WithFloatingSize(1, Wall);
        }
        
        private void RoundWindow(LayoutBuilderV1 l)
        {
            l.Add(Wall)
                .Scale().Z(0.2f).And()
                .Mesh(roundWindowMesh);
        }
        
        private void One(LayoutBuilderV1 l, int n)
        {
            if (n == 1)
            {
                l.Add(Tower1);
            } else if (n == 2)
            {
                l.Add(Central1);
            }
            else
            {
                l.Add(Wall);
            }
        }

        private const float windowWidth = 1.2f;

        private void Tower1(LayoutBuilderV1 l)
        {
            l.SplitX()
                .WithFloatingSize(1, Pillar1Hack)
                .WithSize(0.2f, Wall)
                .WithSize(windowWidth, a => a.SplitY().WithFloatingSize(1, RectWindow).WithSize(1.35f, Wall))
                .WithSize(0.2f, Wall)
                .WithFloatingSize(1, Pillar1Hack);
        }
        
        private void Pillar1Hack(LayoutBuilderV1 l)
        {
            float midRailOffset = (facadeWidth - towerWidth * 2 - 1) * 0.5f;

            l.Add(Wall)
                .Scale().Y(l.Size.y + bigRailHeight + midRailOffset).Z(0.4f)
                .And()
                .Add(Pillar);
        }
        
        private void Pillar(LayoutBuilderV1 l)
        {
            l.Add(PillarTop).Mesh(pillarMesh);
        }
        
        private void PillarTop(LayoutBuilderV1 l)
        {
            var cylinderRadius = l.Size.x * 0.5f;
            l.Translate().Y(l.Size.y-cylinderRadius+0.01f).And()
                .Scale().Y(cylinderRadius).And().BoxPrimitive(Color.white)
                .Translate().RelativeX(-0.25f).And()
                .Scale().RelativeX(1.5f).RelativeZ(1.2f).And()
                .SplitX()
                .WithSize(cylinderRadius, a => a.CylinderPrimitive(Color.white))
                .WithFloatingSize(1, l.None)
                .WithSize(cylinderRadius, a => a.CylinderPrimitive(Color.white));
        }

        private const float centralMargin = 0.4f;

        
        private void Central1(LayoutBuilderV1 l)
        {
            l.SplitY()
                .WithFloatingSize(1, a =>
                {
                    a.SplitX().WithSize(centralMargin, Wall).WithRepeat().WithFloatingSize(windowWidth, RectWindow)
                        .WithFloatingSize(0.4f, C1Pillar).And()
                        .WithFloatingSize(windowWidth, RectWindow)
                        .WithSize(centralMargin, Wall);
                })
                .WithSize(0.7f, Wall);
        }
        
        private void C1Pillar(LayoutBuilderV1 l)
        {
            l.Add(Wall)
                .Scale().Z(0.3f).And()
                .Add(Pillar);
        }
        
        private void Three(LayoutBuilderV1 l, int n)
        {
            if (n == 1)
            {
                l.Add(Tower3);
            } else if (n == 2)
            {
                l.Add(CentralThree);
            }
            else
            {
                l.Add(Wall);
            }
        }
        
        private void CentralThree(LayoutBuilderV1 l)
        {
            l.SplitY()
                .WithFloatingSize(1, a =>
                {
                    a.SplitX()
                        .WithSize(centralMargin, Wall)
                        .WithFloatingSize(1,
                            b => b.Add(Central3Windows).Scale().Z(0.6f).And().Translate().Z(-0.6f).And()
                                .Mesh(partialArchMesh))
                        .WithSize(centralMargin, Wall);
                })
                .WithSize(0.5f, Wall);
        }


        private const float archWindowSetback = 0.3f;

        private void Central3Windows(LayoutBuilderV1 l)
        {
            l
                .SplitX().WithRepeat().WithFloatingSize(windowWidth,
                    a => a.Extrude(-archWindowSetback).Back(RectWindow).Bottom(Wall))
                .WithFloatingSize(0.4f,
                    a => a.Translate().Z(-archWindowSetback).And().Extrude(archWindowSetback - 0.001f).All(Wall)).And()
                .WithFloatingSize(windowWidth, a => a.Extrude(-archWindowSetback).Back(RectWindow).Bottom(Wall));
        }
        
        private void Tower3(LayoutBuilderV1 l)
        {
            float midRailOffset = (facadeWidth - towerWidth * 2 - 1) * 0.5f;

            l.SplitX()
                .WithFloatingSize(1, Wall)
                .WithSize(windowWidth, a =>
                {
                    a.SplitY()
                        .WithFloatingSize(0.3f, Wall)
                        .WithSize(midRailOffset - 0.7f, RectWindow)
                        .WithFloatingSize(1, Wall);
                })
                .WithFloatingSize(1, Wall);
        }

        private const float rWFrameWidth = 0.03f;
        private void RectWindow(LayoutBuilderV1 l)
        {
            l.SplitY().WithRepeat()
                .WithSize(rWFrameWidth, Wall)
                .WithFloatingSize(0.5f, a =>
                {
                    a.SplitX().WithRepeat().WithSize(rWFrameWidth, Wall).WithFloatingSize(0.35f, GlassPane).And()
                        .WithSize(rWFrameWidth, Wall);
                }).And().WithSize(rWFrameWidth, Wall);

        }

        private void GlassPane(LayoutBuilderV1 l)
        {
            l.Extrude(-0.05f).Back(Glass).All(Wall, Directions.XAxis | Directions.YAxis);
        }


        private void Glass(LayoutBuilderV1 l)
        {
            ColorUtility.TryParseHtmlString("#a8ccd7", out var color);
            l.QuadPrimitive(color, "Glass");
        }
        
        private void Ground(LayoutBuilderV1 l, int n)
        {
            if (n == 0)
            {
                l.Add(GroundWall);
            } else if (n == 1)
            {
                l.Add(TowerGround);
            }
            else
            {
                l.Add(CentralGround);
            }
        }
        
        private void CentralGround(LayoutBuilderV1 l)
        {
            l.SplitX()
                .WithFloatingSize(0.25f, GroundWall)
                .WithSize(groundArchDiam, a => a.Add(ArchDoor).Add(GroundArch))
                .WithFloatingSize(1, GroundWall)
                .WithSize(groundArchDiam, a => a.Add(ArchDoor).Add(GroundArch))
                .WithFloatingSize(0.25f, GroundWall);
        }

        private const float groundRailDepth = 0.05f;
        private const float constAboveArches  = 1.2f;
        
        private void ArchDoor(LayoutBuilderV1 l)
        {
            l.Translate().Z(-0.4f).And()
                .SplitY()
                .WithFloatingSize(1, Door)
                .WithSize(groundRailDepth, GroundRail)
                .WithSize(constAboveArches, Wall);
        }
        
        private void Door(LayoutBuilderV1 l)
        {
            l.SplitX()
                .WithSize(0.3f, Wall)
                .WithFloatingSize(1, a => a.Extrude(-0.2f).All(Wall, Directions.XAxis | Directions.YAxis).Back(Doorwall))
                .WithSize(0.3f, Wall);
        }
        
        
        private void Doorwall(LayoutBuilderV1 l)
        {
            ColorUtility.TryParseHtmlString("#ff0000", out var color);
            l.QuadPrimitive(color, "Doorwall");
        }
        private void TowerGround(LayoutBuilderV1 l)
        {
            l.SplitX()
                .WithFloatingSize(1, GroundWall)
                .WithSize(groundArchDiam, a => a.Add(SetbackWindow).Add(GroundArch))
                .WithFloatingSize(1, GroundWall);
        }

        private const float groundWindowOffset = 0.2f;
        
        private void SetbackWindow(LayoutBuilderV1 l)
        {
            l.Translate().Z(-0.3f).And()
                .SplitX()
                .WithSize(groundWindowOffset, Wall)
                .WithFloatingSize(1, a =>
                {
                    a.SplitY().WithSize(0.8f, Wall)
                        .WithFloatingSize(1, ArchWindowInnerBottom)
                        .WithSize(a.Size.x / 2, ArchWindowInnerTop)
                        .WithSize(constAboveArches - groundArchDiam / 2 + groundWindowOffset, Wall);
                })
                .WithSize(groundWindowOffset, Wall);
        }
        
        private const float archWindowSetBack2 = 0.1f;
        
        private void ArchWindowInnerTop(LayoutBuilderV1 l)
        {
            l.Scale().Z(archWindowSetBack2).And()
                .Translate().Z(-archWindowSetBack2).And()
                .Mesh(fullArchMesh).And().Add(Glass);
            //.Scale()
        }
        
        private void ArchWindowInnerBottom(LayoutBuilderV1 l)
        {
            l.Extrude(-archWindowSetBack2).Back(Glass).Left(Wall).Right(Wall).Bottom(Wall);
        }
        
        private void GroundWall(LayoutBuilderV1 l)
        {
            l.SplitY()
                .WithFloatingSize(1, Wall)
                .WithSize(groundRailDepth, GroundRail)
                .WithSize(constAboveArches, Wall);
        }
        
                
        private void GroundArch(LayoutBuilderV1 l)
        {
            l.SplitY()
                .WithFloatingSize(1, ArchWindowBottomOuter)
                .WithSize(groundRailDepth, ArchWindowBottomMiddle)
                .WithSize(groundArchDiam / 2, ArchWindowTopOuter)
                .WithSize(constAboveArches - groundArchDiam / 2, Wall);
        }
        
        private void GroundRail(LayoutBuilderV1 l)
        {
            l//.Scale().X(l.Size.x + groundRailDepth * 2).And()
               // .Translate().X(-groundRailDepth - 0.001f).And()
                .Extrude(groundRailDepth).All(Wall);
        }
        
        private const float archWindowOuterDepth = 0.4f;
        
        private void ArchWindowTopOuter(LayoutBuilderV1 l)
        {
            l.Scale().Z(archWindowOuterDepth).And()
                .Translate().RelativeZ(-1).And()
                .Mesh(fullArchMesh);
        }
        private void ArchWindowBottomMiddle(LayoutBuilderV1 l)
        {
            l.Extrude(-archWindowOuterDepth).Left( GroundRail).Right(GroundRail);
        }
        
        private void ArchWindowBottomOuter(LayoutBuilderV1 l)
        {
            l.Extrude(-archWindowOuterDepth).Left(Wall).Right(Wall).Bottom(Wall);
        }
        
      
        
        
        
       
        
        
        
        
        
        private void Wall(LayoutBuilderV1 l)
        {
            l.QuadPrimitive(Color.white, "Wall");
        }
    }
}