﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "AllSameFacadeSelector",
        menuName = "BuildingGeneration/FacadeSelectors/AllSameFacadeSelector")]
    public class AllSameFacadeSelector: FacadeSelector
    {
        protected override List<FacadeGenerator> GetFacadeGenerators()
        {
            return Enumerable.Repeat(MainFacadeGenerator, buildingBase.Polygon.Count).ToList();
        }
    }
}