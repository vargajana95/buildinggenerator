﻿using System;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Layout;
using ProceduralToolkit;
using UnityEngine;
using static BuildingGenerator.Common.Layout.LayoutBuilderFunctions;
using CompoundMeshDraft = BuildingGenerator.Common.CompoundMeshDraft;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "ThirdFacadePlanner", menuName = "BuildingGeneration/FacadePlanner/ThirdFacadePlanner")]
    public class ThirdFacadePlanner : FacadeGenerator
    {
        [SerializeField] private Mesh windowMesh;
        [SerializeField] private Mesh keyStoneMesh;
        [SerializeField] private Mesh pillarMesh;

        private const float groundHeight = 6;
        private const float firstFloorHegiht = 5f;
        private const float floorHeight = 4;

        private const float wallFXPadL = 1.3f;
        private const float wallFXPadR = 1.2f;
        private const float wallFWindowWidth = 1.6f;
        private const float stepHeight = 0.1f;
        private const float stepRoofHeight = 0.6f;
        private const float leftGroundWallWidth = 1.2f;
        private const float rightGroundWallWidth = 0.7f;
        private const float groundWallHeight = 0.1f;
        private const float signHeight = 0.8f;
        private const float groundSignDepth = 0.4f;
        private const float stepDepth = 4;
        private const float pillarWidth = 0.8f;
        private const float pillarOffset = 1.2f;
        private const float pH = 1f;

        private const float openingWindowHeight = 2.2f;
        private const float fixedWindowHeight = 0.8f;
        private const float keyStoneHeight = 0.6f;
        private const float keyStoneDepth = 0.2f;

        private const float windowDepth = 0.4f;
        private const float windowBottomWallHeight = 0.4f;
        private const float groundWindowWidth = 1.8f;

        private const float groundFloorDWTopPanel = 0.6f;
        private const float groundWoodHeight = 1.2f;

        private const float gPW = 0.2f;
        private const float doorW = 0.3f;

        [SerializeField] private MaterialContainer materialContainer;

        public override MaterialContainerBase Materials => materialContainer;

        [Serializable]
        public class MaterialContainer : MaterialContainerBase
        {
            [SerializeField] [ForMesh("Window")] Material windowFrameMaterial;

            [SerializeField] [ForMesh("Window", 1)] [ForMesh("Glass")]
            Material glassMaterial;

            [SerializeField] [ForMesh("KeyStone")] Material keyStoneMaterial;

            [SerializeField] [ForMesh("Wall")] Material wallMaterial;
            [SerializeField] [ForMesh("Pillar")] Material pillarMaterial;
            [SerializeField] [ForMesh("StepRoof")] Material stepRoofMaterial;

            [SerializeField] [ForMesh("GroundWall")]
            Material groundWallMaterial;

            [SerializeField] [ForMesh("Pot")] Material potMaterial;
            [SerializeField] [ForMesh("Step")] Material stepMaterial;
            [SerializeField] [ForMesh("Sign")] Material signMaterial;
        }


        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var layoutBuilder = new LayoutBuilder
            {
                Size = new Vector3(facadeWidth, facadeHeight, 1)
            };

            return layoutBuilder
                .SetUV(10, 10)
                .SplitY(
                    WithSize(groundHeight, Ground),
                    WithSize(firstFloorHegiht, WallFacade(1)),
                    WithRepeat(WithFloatingSize(floorHeight, WallFacade(0)))
                )
                .Build();
        }

        private Rule WallFacade(int h)
        {
            return l =>
                l.SplitX(
                    NoSpaceFallback(Wall),
                    WithSize(wallFXPadL, Wall),
                    WithFloatingSize(1,
                        SplitX(
                            NoSpaceFallback(
                                SplitX(
                                    NoSpaceFallback(Wall),
                                    WithFloatingSize(0.1f, Wall),
                                    WithSize(wallFWindowWidth, WallWindow(h)),
                                    WithFloatingSize(0.1f, Wall)
                                )
                            ),
                            WithSize(wallFWindowWidth, WallWindow(h)),
                            WithRepeat(
                                NoSpaceFallback(Wall),
                                WithFloatingSize(1.5f, Wall),
                                WithSize(wallFWindowWidth, WallWindow(h * 2)),
                                WithFloatingSize(1.5f, Wall)
                            ),
                            WithSize(wallFWindowWidth, WallWindow(h))
                        )
                    ),
                    WithSize(wallFXPadR, Wall)
                );
        }

        private Rule WallWindow(int n)
        {
            return l =>
                l.SplitY(
                    WithSize(windowBottomWallHeight, Wall),
                    WithSize(openingWindowHeight + n * fixedWindowHeight, WallWindow),
                    WithSize(keyStoneHeight,
                        Wall,
                        KS
                    ),
                    WithFloatingSize(1, Wall)
                );
        }

        private void WallWindow(LayoutBuilder l)
        {
            l.Scale(Z(windowDepth))
                .Mesh(windowMesh, false,
                    Name("Window"),
                    RecalculateUVs()
                );
        }

        private void KS(LayoutBuilder l)
        {
            l
                .Translate(
                    Y(-0.07f)
                )
                .Scale(
                    RelativeX(1.2f),
                    Z(keyStoneDepth)
                )
                .CenterX()
                .Mesh(keyStoneMesh, false,
                    Name("KeyStone"),
                    RecalculateUVs()
                );
        }

        private void Ground(LayoutBuilder l)
        {
            l.SplitY(
                WithSize(stepHeight, Step),
                WithFloatingSize(1,
                    Pillars,
                    SplitY(
                        WithFloatingSize(1,
                            SplitX(
                                NoSpaceFallback(Wall),
                                WithSize(leftGroundWallWidth, Wall),
                                WithFloatingSize(1, GroundFloorWindows),
                                WithSize(rightGroundWallWidth, Wall)
                            )
                        ),
                        WithSize(groundWallHeight, Wall),
                        WithSize(signHeight, Sign(groundSignDepth))
                    )
                ),
                WithSize(stepRoofHeight, StepRoof)
            );
        }


        private void StepRoof(LayoutBuilder l)
        {
            l.Extrude(stepDepth,
                All(StepRoofMaterial, Directions.All & ~Directions.Back)
            );
        }

        private void Pillars(LayoutBuilder l)
        {
            l.Translate(Z(stepDepth - pillarOffset - pillarWidth))
                .SplitX(
                    WithSize(pillarWidth, Pillar),
                    WithRepeat(
                        WithFloatingSize(3, None),
                        WithSize(pillarWidth, Pillar)
                    )
                );
        }

        private void Pillar(LayoutBuilder l)
        {
            l.Scale(
                    X(pillarWidth),
                    Z(pillarWidth)
                )
                .Mesh(
                    pillarMesh, false,
                    Name("Pillar"),
                    RecalculateUVs()
                );
        }

        private Rule Sign(float extrudeAmount)
        {
            return l =>
            {
                ColorUtility.TryParseHtmlString("#444750", out var color);
                l.Extrude(extrudeAmount, All(QuadPrimitive(UseColor(color), Name("Sign"))));
            };
        }

        private void Step(LayoutBuilder l)
        {
            l.Extrude(stepDepth,
                All(Decking, Directions.All & ~Directions.Up),
                Top(a => a.Add(Plant).Translate(X(a.Size.x - pH)).Add(Plant).Add(Decking)));
        }

        private void Plant(LayoutBuilder l)
        {
            l.Scale(
                    X(pH),
                    Y(pH)
                )
                .Extrude(pH, All(Pot, Directions.All & ~Directions.Back));
        }

        private void Decking(LayoutBuilder l)
        {
            ColorUtility.TryParseHtmlString("#804000", out var color);
            l.QuadPrimitive(UseColor(color), Name("Step"));
        }

        private void GroundFloorWindows(LayoutBuilder l)
        {
            l.SplitX(
                WithRepeat(
                    NoSpaceFallback(Wall),
                    WithFloatingSize(groundWindowWidth, GroundFloorDoorOrWindow)
                )
            );
        }

        private void GroundFloorDoorOrWindow(LayoutBuilder l)
        {
            if (l.Index == l.ParentSplitCount - 1)
            {
                l.Add(DoorPanel);
            }
            else
            {
                l.Add(GroundFloorWindow);
            }
        }

        private void GroundFloorWindow(LayoutBuilder l)
        {
            l.SplitY(
                WithSize(groundWoodHeight, GroundWood),
                WithFloatingSize(1, GroundWindowPanel),
                WithSize(groundFloorDWTopPanel, GroundWindowPanel)
            );
        }

        private void GroundWindowPanel(LayoutBuilder l)
        {
            l.SplitX(
                WithSize(gPW, GroundWood),
                WithFloatingSize(1, GroundWindowPanel2),
                WithSize(gPW, GroundWood),
                WithFloatingSize(1, GroundWindowPanel2),
                WithSize(gPW, GroundWood)
            );
        }

        private void GroundWindowPanel2(LayoutBuilder l)
        {
            l.SplitY(
                WithSize(gPW, GroundWood),
                WithFloatingSize(1,
                    Extrude(-0.03f,
                        All(GroundWood, Directions.XAxis | Directions.YAxis),
                        Back(Glass))
                ),
                WithSize(gPW, GroundWood)
            );
        }

        private void Glass(LayoutBuilder l)
        {
            ColorUtility.TryParseHtmlString("#a8ccd7", out var color);
            l.QuadPrimitive(UseColor(color), Name("Glass"));
        }

        private void DoorPanel(LayoutBuilder l)
        {
            l.SplitY(
                WithFloatingSize(1, Door),
                WithSize(groundFloorDWTopPanel, GroundWindowPanel)
            );
        }

        private void Door(LayoutBuilder l)
        {
            l.Add(DoorHandle)
                .SplitY(
                    WithSize(doorW, GroundWood),
                    WithFloatingSize(0.5f, DoorPane),
                    WithSize(doorW, GroundWood),
                    WithFloatingSize(1f, DoorPane),
                    WithSize(doorW, GroundWood)
                );
        }

        private void DoorHandle(LayoutBuilder l)
        {
            ColorUtility.TryParseHtmlString("#000000", out var color);
            l.Translate(
                    X(doorW / 4),
                    RelativeY(0.5f)
                )
                .Scale(
                    X(doorW / 2),
                    RelativeY(0.2f)
                )
                .Extrude(0.1f,
                    All(QuadPrimitive(UseColor(color), Name("GroundWall")))
                );
        }

        private void DoorPane(LayoutBuilder l)
        {
            l.SplitX(
                WithSize(doorW, GroundWood),
                WithFloatingSize(1f, DoorPaneS),
                WithSize(doorW, GroundWood),
                WithFloatingSize(1f, DoorPaneS),
                WithSize(doorW, GroundWood)
            );
        }

        private void DoorPaneS(LayoutBuilder l)
        {
            l.Extrude(-0.03f,
                All(GroundWood, Directions.XAxis | Directions.YAxis | Directions.Back)
            );
        }

        private void GroundWood(LayoutBuilder l)
        {
            ColorUtility.TryParseHtmlString("#cccccc", out var color);

            l.QuadPrimitive(
                UseColor(color),
                Name("GroundWall")
            );
        }

        private void Wall(LayoutBuilder l)
        {
            l.QuadPrimitive(Name("Wall"));
        }

        private void StepRoofMaterial(LayoutBuilder l)
        {
            l.QuadPrimitive(Name("StepRoof"));
        }

        private void Pot(LayoutBuilder l)
        {
            l.QuadPrimitive(Name("Pot"));
        }
    }
}