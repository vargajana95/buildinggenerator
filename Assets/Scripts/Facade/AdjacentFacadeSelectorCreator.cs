﻿using System.Collections.Generic;
using BuildingGenerator.Creator;
using UnityEngine;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "AdjacentFacadeSelectorRandomizer",
        menuName = "BuildingGeneration/Randomizer/FacadeSelector/AdjacentFacadeSelectorRandomizer")]
    public class AdjacentFacadeSelectorCreator: FacadeSelectorCreator
    {
        public override int AdditionalFacadePlannerCount => 1;

        public override FacadeSelector CreateFacadeSelector(FacadeGenerator mainFacadePlanner, List<FacadeGenerator> additionalFacadePlanners)
        {
            var facadeSelector = CreateInstance<AdjacentAwareFacadeSelector>();
            facadeSelector.name = "AdjacentAwareFacadeSelector";
            facadeSelector.MainFacadeGenerator = mainFacadePlanner;
            facadeSelector.FacadePlannerForAdjacentSides = additionalFacadePlanners[0];

            return facadeSelector;
        }
    }
}