﻿using BuildingGenerator.Common;
using BuildingGenerator.Common.Layout;
using UnityEngine;
using static BuildingGenerator.Common.Layout.LayoutBuilderFunctions;

namespace BuildingGenerator.Facade
{
    [CreateAssetMenu(fileName = "TestFacadePlanner", menuName = "BuildingGeneration/FacadePlanner/TestFacadePlanner")]
    public class TestFacadePlanner : FacadeGenerator
    {
        [SerializeField] private Mesh windowMesh;
        [SerializeField] private float windowWidth;
        [SerializeField] private float windowHeight;

        public override CompoundMeshDraft Generate(float facadeWidth, float facadeHeight)
        {
            var layoutBuilder = new LayoutBuilder(new Vector3(facadeWidth, facadeHeight, 1));


            return layoutBuilder
                .SplitX(
                    NoSpaceFallback(SingleWindow),
                    WithFloatingSize(0.1f, Wall),
                    WithSize(windowWidth * 2, DoubleWindow),
                    WithFloatingSize(0.1f, Wall)
                )
                .Build();
        }

        private void Window(LayoutBuilder l)
        {
            l.SplitY(
                WithFloatingSize(0.1f, Wall),
                WithSize(windowHeight, Mesh(windowMesh, false)),
                WithFloatingSize(0.1f, Wall)
            );
        }   
        private void SingleWindow(LayoutBuilder l)
        {
            l.SplitX(
                WithFloatingSize(0.1f, Wall),
                WithSize(windowWidth, Window),
                WithFloatingSize(0.1f, Wall)
            );
        }

        private void DoubleWindow(LayoutBuilder l)
        {
            l.SplitX(
                WithSize(windowWidth, Window),
                WithSize(windowWidth, Window)
            );
        }

        private void Wall(LayoutBuilder l)
        {
            l.QuadPrimitive(Name("Wall"));
        }
    }
}