﻿using System.Collections.Generic;
using BuildingGenerator.Common.Helpers;
using UnityEngine;

namespace BuildingGenerator.Selectors
{
    [CreateAssetMenu(fileName = "ListMaterialSelector", menuName = "BuildingGeneration/MaterialSelectors/ListMaterialSelector")]
    public class ListMaterialSelector: MaterialSelector
    {
        [SerializeField] private List<Material> materials;

        public override Material GetOne()
        {
            if (materials.Count == 0)
            {
                return null;
            }
            return materials.GetRandom();
        }

        public override Material GetOneOrFromPrevious(Dictionary<string, Material> previousSelections)
        {
            return GetOne();
        }
    }
}