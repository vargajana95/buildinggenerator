﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BuildingGenerator.Selectors
{
    public abstract class MaterialSelector: ScriptableObject
    {
        public string groupName;
        
        public string FieldName { get; set; }

        public virtual int Order => 0;
        public abstract Material GetOne();
        public abstract Material GetOneOrFromPrevious(Dictionary<string, Material> previousSelections);
        
        [ContextMenu("DeleteAsset")]
        private void DeleteAsset()
        {
            var assetPath = AssetDatabase.GetAssetPath(this);
            var allSubassets = AssetDatabase.LoadAllAssetsAtPath(assetPath);
            foreach (var subasset in allSubassets)
            {
                if (subasset.name == this.name)
                {
                    DestroyImmediate(subasset, true);
            AssetDatabase.SaveAssets();
                }
            }

        }
    }
}