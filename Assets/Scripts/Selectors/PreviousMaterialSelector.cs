﻿using System.Collections.Generic;
using UnityEngine;

namespace BuildingGenerator.Selectors
{
    [CreateAssetMenu(fileName = "PreviousMaterialSelector", menuName = "BuildingGeneration/MaterialSelectors/PreviousMaterialSelector")]
    public class PreviousMaterialSelector: MaterialSelector
    {
        public override int Order => 1000;

        public override Material GetOne()
        {
            throw new System.NotImplementedException("Cannot call GetOne on PreviousMaterialSelector!");
        }

        public override Material GetOneOrFromPrevious(Dictionary<string, Material> previousSelections)
        {
            previousSelections.TryGetValue(groupName, out var material);
            return material;
        }
    }
}