﻿using System.Collections.Generic;
using UnityEngine;

namespace BuildingGenerator.Common
{
    public struct House
    {
        public CompoundMeshDraft facadesMeshDraft;
        public CompoundMeshDraft roofMeshDraft;
        public Material[] facadeMaterials;
        public Material[] roofMaterials;
        public float facadeHeight;
    }
}