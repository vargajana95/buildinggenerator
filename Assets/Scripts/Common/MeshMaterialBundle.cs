﻿using System.Collections.Generic;
using UnityEngine;

namespace BuildingGenerator.Common
{
    [System.Serializable]
    public class MeshMaterialBundle
    {
        public Mesh mesh;
        public List<Material> materials;
    }
}