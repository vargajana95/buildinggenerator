﻿using System;
using BuildingGenerator.Base;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Facade;
using UnityEngine;

namespace BuildingGenerator.Common
{
    [Serializable]
    public class Section
    {
        public Quaternion rotation = Quaternion.identity;
        public int height = 50;
        [Expandable] public FacadeGenerator facadeGenerator;
        [Expandable] public BaseGenerator baseGenerator;
        public Vector2 origin;

        // public SkyscraperBaseGenerator baseGeneratorAsset;
        // public FacadePlanner facadeGeneratorAsset;
    }
}