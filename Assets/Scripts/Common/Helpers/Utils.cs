﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProceduralToolkit;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace BuildingGenerator.Common.Helpers
{
    public static class Utils
    {
        /// <summary>
        /// Determines if the given point is inside the polygon
        /// </summary>
        /// <param name="polygon">the vertices of polygon</param>
        /// <param name="point">the given point</param>
        /// <returns>true if the point is inside the polygon; otherwise, false</returns>
        public static bool IsPointInPolygon(List<Vector2> polygon, Vector2 point)
        {
            bool result = false;
            int j = polygon.Count - 1;
            for (int i = 0; i < polygon.Count; i++)
            {
                if (polygon[i].y < point.y && polygon[j].y >= point.y ||
                    polygon[j].y < point.y && polygon[i].y >= point.y)
                {
                    if (polygon[i].x + (point.y - polygon[i].y) / (polygon[j].y - polygon[i].y) *
                        (polygon[j].x - polygon[i].x) < point.x)
                    {
                        result = !result;
                    }
                }

                j = i;
            }

            return result;
        }

        //TODO Should also check if polygon is inside rectangle
        public static bool IsRectInPolygon(List<Vector2> polygon, Rect rect)
        {
            return IsPointInPolygon(polygon, rect.position)
                   && IsPointInPolygon(polygon, new Vector2(rect.xMin, rect.yMax))
                   && IsPointInPolygon(polygon, new Vector2(rect.xMax, rect.yMax))
                   && IsPointInPolygon(polygon, new Vector2(rect.xMax, rect.yMin));
        }

        public static Vector2 GetRandomPointInTriangle(List<Vector2> triangle)
        {
            var r1 = Random.value;
            var r2 = Random.value;
            return (1 - Mathf.Sqrt(r1)) * triangle[0] + (Mathf.Sqrt(r1) * (1 - r2)) * triangle[1] +
                   (r2 * Mathf.Sqrt(r1)) * triangle[2];
        }


        public static MeshDraft Add(this MeshDraft thisMeshDraft, ProceduralToolkit.MeshDraft draft)
        {
            if (draft == null) throw new ArgumentNullException("draft");

            if (thisMeshDraft.triangles.Count == 0)
            {
                thisMeshDraft.triangles.Add(new List<int>());
            }

            for (var i = 0; i < draft.triangles.Count; i++)
            {
                thisMeshDraft.triangles[0].Add(draft.triangles[i] + thisMeshDraft.vertices.Count);
            }

            thisMeshDraft.vertices.AddRange(draft.vertices);


            thisMeshDraft.normals.AddRange(draft.normals);
            thisMeshDraft.tangents.AddRange(draft.tangents);
            thisMeshDraft.uv.AddRange(draft.uv);
            thisMeshDraft.uv2.AddRange(draft.uv2);
            thisMeshDraft.uv3.AddRange(draft.uv3);
            thisMeshDraft.uv4.AddRange(draft.uv4);
            thisMeshDraft.colors.AddRange(draft.colors);
            return thisMeshDraft;
        }

        /// <summary>
        /// Gets all children of `SerializedProperty` at 1 level depth.
        /// </summary>
        /// <param name="serializedProperty">Parent `SerializedProperty`.</param>
        /// <returns>Collection of `SerializedProperty` children.</returns>
        public static IEnumerable<SerializedProperty> GetVisibleChildren(this SerializedProperty serializedProperty)
        {
            if (serializedProperty.objectReferenceValue == null)
            {
                yield break;
            }

            SerializedObject targetObject = new SerializedObject(serializedProperty.objectReferenceValue);

            SerializedProperty field = targetObject.GetIterator();

            field.NextVisible(true);


            while (field.NextVisible(false))
            {
                yield return field;
            }
        }

        public struct PolygonSplit
        {
            public List<Vector2> leftPolygon;
            public List<Vector2> rightPolygon;
            public List<Vector2> newEdge;
            public bool splitSuccessful;
            public List<int> leftPolygonCutIndices;
            public List<int> rightPolygonCutIndices;
        }

        public static PolygonSplit SplitPolygonByLine(List<Vector2> polygon, Vector2 lineA, Vector2 lineB)
        {
            var leftPolygon = new List<Vector2>();
            var rightPolygon = new List<Vector2>();
            var newEdge = new List<Vector2>();
            var splitSuccessful = false;
            var leftPolygonCutIndices = new List<int>();
            var rightPolygonCutIndices = new List<int>();

            for (var i = 0; i < polygon.Count; i++)
            {
                var side = GetSideOfPointOfLine(lineA, lineB, polygon[i]);
                if (side >= 0)
                {
                    leftPolygon.Add(polygon[i]);
                }

                if (side <= 0)
                {
                    rightPolygon.Add(polygon[i]);
                }

                if (side != 0)
                {
                    var segmentsIntersection = SegmentsIntersect(lineA, lineB, polygon[i], polygon.GetLooped(i + 1));
                    //Also check if it's not intersection at the end of the segment
                    if (segmentsIntersection.secondSegmentIntersect &&
                        !Mathf.Approximately(segmentsIntersection.t2, 1f))
                    {
                        leftPolygonCutIndices.Add(leftPolygon.Count);
                        rightPolygonCutIndices.Add(rightPolygon.Count);
                        leftPolygon.Add(segmentsIntersection.intersectionPoint);
                        rightPolygon.Add(segmentsIntersection.intersectionPoint);
                        newEdge.Add(segmentsIntersection.intersectionPoint);
                    }
                }
                else
                {
                    leftPolygonCutIndices.Add(leftPolygon.Count);
                    rightPolygonCutIndices.Add(rightPolygon.Count);
                    newEdge.Add(polygon[i]);
                }

                // if (newEdge.Count == 2)
                // {
                //     for (var j = i+1; j < polygon.Count; j++)
                //     {
                //         leftPolygon.Add(polygon[j]);
                //     }
                //
                //     break;
                // }
            }

            //Handle case when line crosses only one point of the polygon or two vertices
            if (rightPolygon.Count <= 2) rightPolygon.Clear();
            if (leftPolygon.Count <= 2) leftPolygon.Clear();
            if (newEdge.Count <= 1) newEdge.Clear();

            if (rightPolygon.Count > 0 && leftPolygon.Count > 0)
            {
                splitSuccessful = true;
            }
            else
            {
                newEdge.Clear();
            }

            return new PolygonSplit
            {
                leftPolygon = leftPolygon,
                rightPolygon = rightPolygon,
                newEdge = newEdge,
                splitSuccessful = splitSuccessful,
                leftPolygonCutIndices = leftPolygonCutIndices,
                rightPolygonCutIndices = rightPolygonCutIndices
            };
        }

        public static int GetSideOfPointOfLine(Vector2 lineA, Vector2 lineB, Vector2 point)
        {
            return Math.Sign(((lineB.x - lineA.x) * (point.y - lineA.y) - (lineB.y - lineA.y) * (point.x - lineA.x)));
        }

        public struct SegmentsIntersection
        {
            public Vector2 intersectionPoint;
            public bool linesIntersect;
            public float t1;
            public float t2;
            public bool firstSegmentIntersect;
            public bool secondSegmentIntersect;
            public bool segmentsIntersect;
        }

        public static SegmentsIntersection SegmentsIntersect(
            Vector2 segment1A, Vector2 segment1B, Vector2 segment2A, Vector2 segment2B)
        {
            // Get the segments' parameters.
            double dx12 = segment1B.x - segment1A.x;
            double dy12 = segment1B.y - segment1A.y;
            double dx34 = segment2B.x - segment2A.x;
            double dy34 = segment2B.y - segment2A.y;

            // Solve for t1 and t2
            double denominator = (dy12 * dx34 - dx12 * dy34);

            double t1 =
                ((segment1A.x - segment2A.x) * dy34 + (segment2A.y - segment1A.y) * dx34)
                / denominator;

            Vector2 intersection;
            if (double.IsInfinity(t1))
            {
                // The lines are parallel (or close enough to it).
                intersection = new Vector2(float.NaN, float.NaN);
                return new SegmentsIntersection()
                {
                    intersectionPoint = intersection,
                    t1 = float.NaN,
                    t2 = float.NaN,
                    linesIntersect = false,
                    segmentsIntersect = false,
                    firstSegmentIntersect = false,
                    secondSegmentIntersect = false
                };
            }

            double t2 =
                ((segment2A.x - segment1A.x) * dy12 + (segment1A.y - segment2A.y) * dx12)
                / -denominator;

            // Find the point of intersection.
            intersection = new Vector2((float)(segment1A.x + dx12 * t1), (float) (segment1A.y + dy12 * t1));


            // The segments intersect if t1 and t2 are between 0 and 1.
            return new SegmentsIntersection()
            {
                intersectionPoint = intersection,
                t1 = (float)t1,
                t2 = (float)t2,
                linesIntersect = true,
                segmentsIntersect = (t1 >= -0.00001) && (t1 <= 1.00001) && (t2 >= -0.00001) && (t2 <= 1.00001),
                firstSegmentIntersect = (t1 >= -0.00001) && (t1 <= 1.00001),
                secondSegmentIntersect = (t2 >= -0.00001) && (t2 <= 1.00001)
            };
        }

        public static GameObject CreateNewObjectWithName(string name)
        {
            GameObject housesGameObject = GameObject.Find(name);
            if (housesGameObject != null)
            {
                Object.DestroyImmediate(housesGameObject);
            }

            return new GameObject(name);
        }

        public static float DistanceFromPointToLine(Vector2 point, Vector2 lineA, Vector2 lineB)
        {
            return Mathf.Abs((lineB.x - lineA.x) * (lineA.y - point.y) - (lineA.x - point.x) * (lineB.y - lineA.y)) /
                   Mathf.Sqrt(Mathf.Pow(lineB.x - lineA.x, 2) + Mathf.Pow(lineB.y - lineA.y, 2));
        }

        public static Vector2 GetPolygonCenter(List<Vector2> polygon)
        {
            var center = new Vector2();
            foreach (var point in polygon)
            {
                center += point;
            }

            center.Set(center.x / polygon.Count, center.y / polygon.Count);
            return center;
        }

        public static List<Vector2> MovePolygon(List<Vector2> polygon, Vector2 vector)
        {
            var movedPolygon = new List<Vector2>();
            foreach (var point in polygon)
            {
                movedPolygon.Add(point + vector);
            }

            return movedPolygon;
        }

        public static bool PointIsOnSegment(Vector2 point, Vector2 segmentA, Vector2 segmentB)
        {
            var ab = Mathf.Sqrt((segmentB.x - segmentA.x) * (segmentB.x - segmentA.x) +
                                (segmentB.y - segmentA.y) * (segmentB.y - segmentA.y));
            var ap = Mathf.Sqrt((point.x - segmentA.x) * (point.x - segmentA.x) +
                                (point.y - segmentA.y) * (point.y - segmentA.y));
            var pb = Mathf.Sqrt((segmentB.x - point.x) * (segmentB.x - point.x) +
                                (segmentB.y - point.y) * (segmentB.y - point.y));
            if (Mathf.Approximately(ab, ap + pb))
            {
                return true;
            }

            return false;
        }

        public static bool SegmentContainsSegment(Vector2 segment1A, Vector2 segment1B, Vector2 segment2A,
            Vector2 segment2B)
        {
            return PointIsOnSegment(segment2A, segment1A, segment1B) &&
                   PointIsOnSegment(segment2B, segment1A, segment1B);
        }

        public static float PolygonArea(List<Vector2> polygon)
        {
            var vertices = new List<Vector2>(polygon);
            vertices.Add(vertices[0]);
            return Math.Abs(vertices.Take(vertices.Count - 1)
                .Select((p, i) => (p.x * vertices[i + 1].y) - (p.y * vertices[i + 1].x)).Sum() / 2);
        }

        //This only works for convex, non overlapping polygons
        public static List<Vector2> PolygonFindMutualEdge(List<Vector2> polygon1, List<Vector2> polygon2,
            out List<int> polygon1Indices, out List<int> polygon2Indices)
        {
            polygon1Indices = new List<int>();
            polygon2Indices = new List<int>();
            var mutualEdge = new List<Vector2>();

            for (var i = 0; i < polygon1.Count; i++)
            {
                for (var j = 0; j < polygon2.Count; j++)
                {
                    // if (polygon1[i] == polygon2[j])
                    if ((polygon1[i] - polygon2[j]).magnitude < 0.001f )
                    {
                        mutualEdge.Add(polygon1[i]);
                        polygon1Indices.Add(i);
                        polygon2Indices.Add(j);
                    }
                }
            }

            return mutualEdge;
            // return polygon1.Where(p1 => polygon2.Any(p2 => p1 == p2)).ToList();
        }

        public static List<Vector2> PolygonFindMutualEdge(List<Vector2> polygon1, List<Vector2> polygon2)
        {
            return PolygonFindMutualEdge(polygon1, polygon2, out _, out _);
        }

        public static void RemoveUnnecessaryPointsFromPolygon(List<Vector2> polygon)
        {
            for (var i = 0; i < polygon.Count;)
            {
                // if (PointIsOnSegment(polygon[i], polygon.GetLooped(i-1), polygon.GetLooped(i+1)))
                if (polygon[i] == polygon.GetLooped(i + 1) || polygon[i] == polygon.GetLooped(i - 1) ||
                    Mathf.Approximately(
                        Math.Abs(Vector2.Dot((polygon[i] - polygon.GetLooped(i + 1)).normalized,
                            (polygon[i] - polygon.GetLooped(i - 1)).normalized)), 1f))
                {
                    polygon.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }
        }

        public static List<Vector2> ScalePolygon(List<Vector2> polygon, float factor)
        {
            return polygon.Select(p => p * factor).ToList();
        }

        public static void RemoveDuplicatedPointsFromPolygon(List<Vector2> polygon)
        {
            for (var i = polygon.Count - 1; i >= 0; i--)
            {
                if ((polygon[i] - polygon.GetLooped(i - 1)).sqrMagnitude < 0.00001f)
                {
                    polygon.RemoveAt(i);
                }
            }
        }

        public static bool PolygonIsClockwise(List<Vector2> polygon)
        {
            var sum = 0f;
            for (var i = 0; i < polygon.Count; i++)
            {
                var nextPoint = polygon.GetLooped(i + 1);
                sum += (nextPoint.x - polygon[i].x)*(nextPoint.y + polygon[i].y);
            }

            return sum > 0;
        }

        public static List<List<Vector2>> OffsetPolygon(List<Vector2> polygon, float offset)
        {
            var pathOffsetter = new PathOffsetter();
            pathOffsetter.AddPath(polygon);
            var offsetPolygons = new List<List<Vector2>>();
            pathOffsetter.Offset(ref offsetPolygons, offset);
            
            offsetPolygons.ForEach(p => p.Reverse());

            return offsetPolygons;
        }
        
        public static void DrawDebugSphere(Vector3 position, string name = "Sphere")
        {
            var spheresParent = GameObject.Find("Spheres");
            var primitive = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            primitive.transform.SetParent(spheresParent.transform);
            primitive.transform.localScale *= 7;
            primitive.transform.position = position;
            primitive.name = name;
        }

        public static List<Vector3> ToVector3XZList(this List<Vector2> list)
        {
            return list.Select(p => p.ToVector3XZ()).ToList();
        }
        
        public static List<Vector3> Move(this List<Vector3> list, Vector3 vector)
        {
            for (var i = 0; i < list.Count; i++)
            {
                list[i] = list[i] + vector;
            }

            return list;
        }

        public static List<Vector2> GetCorners(this Rect rect)
        {
            return new List<Vector2>
            {
                new Vector2(rect.xMax, rect.yMin),
                rect.min,
                new Vector2(rect.xMin, rect.yMax),
                rect.max
                
            };
        }

        public static bool PolygonIsInsidePolygon(List<Vector2> polygon1, List<Vector2> polygon2)
        {
            foreach (var point in polygon2)
            {
                if (!IsPointInsidePolygon(point, polygon1))
                {
                    return false;
                }
            }

            return true;
        }
        
        public static bool IsPointInsidePolygon(Vector2 point, List<Vector2> points)
        {
            var numpoints = points.Count;

            if (numpoints < 3)
                return false;

            var it = 0;
            var first = points[it];
            var oddNodes = false;

            for (var i = 0; i < numpoints; i++)
            {
                var node1 = points[it];
                it++;
                var node2 = i == numpoints - 1 ? first : points[it];

                var x = point.x;
                var y = point.y;

                if (node1.y < y && node2.y >= y || node2.y < y && node1.y >= y)
                {
                    if (node1.x + (y - node1.y) / (node2.y - node1.y) * (node2.x - node1.x) < x)
                        oddNodes = !oddNodes;
                }
            }

            return oddNodes;
        }
        
        
        /// <summary>
        /// Returns a random element
        /// </summary>
        public static T GetRandom<T>(this IReadOnlyList<T> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }
            if (list.Count == 0)
            {
                throw new ArgumentException("Empty list");
            }
            return list[Random.Range(0, list.Count)];
        }

        public static void RecalculateUV(this MeshDraft mesh, float textureWidth, float textureHeight)
        {
            ApplyUVCoordinates(mesh, textureWidth, textureHeight);
        }
        
  
        public static void ApplyUVCoordinates(MeshDraft mesh, float textureWidth, float textureHeight)
        {
            List<Vector2> uv = new List<Vector2>(mesh.vertexCount);

            for (int i = 0; i < mesh.vertices.Count; i++)
            {
                var n1 = (Quaternion.LookRotation(mesh.normals[i]) * Vector3.right).normalized;
                var n2 = (Quaternion.LookRotation(mesh.normals[i]) * Vector3.up).normalized;

                var u = Vector3.Dot(mesh.vertices[i], n1);
                var v = Vector3.Dot(mesh.vertices[i], n2);
                var uvX = (u) / textureWidth;
                var uvY = (v) / textureHeight;
                uv.Add(new Vector2(uvX, uvY));
            }

            mesh.uv = uv;
        }
        
    }
}