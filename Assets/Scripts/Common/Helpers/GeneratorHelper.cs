﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BuildingGenerator.Common;
using UnityEngine;

namespace BuildingGenerator.Common.Helpers
{
    public static class GeneratorHelper
    {
        private static List<Material> FillWithSingleMaterial(CompoundMeshDraft compoundMeshDraft, Material material)
        {
            return Enumerable.Repeat(material, compoundMeshDraft.Sum(m => m.SubmeshCount)).ToList();
        }

        public static List<Material> CalculateMaterials(this CompoundMeshDraft compoundMeshDraft,
            Dictionary<string, List<Material>> availableMaterials, Material defaultMaterial)
        {
            var materials = new List<Material>();

            foreach (var draft in compoundMeshDraft)
            {
                if (availableMaterials.ContainsKey(draft.name))
                {
                    var meshDraftMaterialList = Enumerable.Repeat(defaultMaterial, draft.SubmeshCount).ToList();

                    var givenMaterialCount = Math.Min(availableMaterials[draft.name].Count, draft.SubmeshCount);
                    for (var i = 0; i < givenMaterialCount; i++)
                    {
                        var material= availableMaterials[draft.name][i];
                        if (material != null)
                        {
                            meshDraftMaterialList[i] = material;
                        }
                    }

                    materials.AddRange(meshDraftMaterialList);
                }
                else
                {
                    for (int i = 0; i < draft.SubmeshCount; i++)
                    {
                        materials.Add(defaultMaterial);
                    }
                }
            }

            return materials;
        }
    }
}