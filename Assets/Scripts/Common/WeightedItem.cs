﻿using System;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Creator;

namespace BuildingGenerator.Common
{
    [Serializable]
    public class WeightedItem
    {
        public float weight;
        [Expandable(headerEnd = 100)] public RoofGeneratorRandomizer item;
        
    }
}