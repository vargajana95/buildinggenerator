﻿using System.Collections.Generic;
using UnityEngine;

namespace BuildingGenerator.Common
{
    [ExecuteInEditMode]
    public class ShapeCreator : MonoBehaviour
    {
        [HideInInspector] public List<Vector2> points = new List<Vector2>();

        public float handleRadius = .5f;

        private IHasShape shapeHolder;

        private void OnEnable()
        {
            ResetPoints();
        }

        [ContextMenu("Reset Points")]
        private void ResetPoints()
        {
            shapeHolder = GetComponent<IHasShape>();
            if (shapeHolder != null)
            {
                points = shapeHolder.GetPoints();
            }
            else
            {
                points = new List<Vector2>();
            }
        }

        public void ShapeChanged()
        {
            shapeHolder?.ShapeChanged();
        }
    }
}