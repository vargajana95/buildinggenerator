﻿using System;
using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;
using static BuildingGenerator.Common.Layout.LayoutBuilderFunctions;

namespace BuildingGenerator.Common.Layout
{
    public delegate void Rule(LayoutBuilder l);

    public delegate void Modification(LayoutBuilder l, int i);

    public class LayoutBuilder
    {
        public Vector3 Size { get; set; } = Vector3.one;
        public Vector3 Position { get; set; }

        public readonly LayoutBuilder parent;

        private Quaternion rotation = Quaternion.identity;
        private Vector3 rotationOrigin = Vector3.zero;
        public Quaternion LookRotation { get; set; } = Quaternion.LookRotation(Vector3.forward);

        private float textureHeight = 3;
        private float textureWidth = 3;
        public string Name { get; set; }
        public Color Color { get; set; } = Color.white;
        public bool keepOriginalUVs = true;
        private Vector3 uvOrigin;
        private bool shouldOverrideUvOrigin;

        public int Index { get; set; }

        public int ParentSplitCount { get; set; }

        public List<Rule> Rules { get; set; } = new List<Rule>();
        private Primitive primitive;

        private readonly List<LayoutBuilder> children = new List<LayoutBuilder>();

        public LayoutBuilder()
        {
        }
        
        public LayoutBuilder(Vector3 size)
        {
            this.Size = size;
        }

        private LayoutBuilder(LayoutBuilder parent)
        {
            this.parent = parent;
        }


        public LayoutBuilder SplitX(Action<List<Parts>> split, params Action<List<Parts>>[] splits)
        {
            var parts = new List<Parts>();

            split(parts);
            foreach (var s in splits)
            {
                s(parts);
            }

            var child = AddChild();
            child.Rules.Add(layoutBuilder => { Split(parts, layoutBuilder, Direction.Horizontal); });

            return this;
        }

        public LayoutBuilder SplitY(Action<List<Parts>> split, params Action<List<Parts>>[] splits)
        {
            var parts = new List<Parts>();

            split(parts);
            foreach (var s in splits)
            {
                s(parts);
            }

            var child = AddChild();
            child.Rules.Add(layoutBuilder => { Split(parts, layoutBuilder, Direction.Vertical); });

            return this;
        }

        private static void Split(List<Parts> parts, LayoutBuilder splitLayout, Direction direction)
        {
            int index = 0;
            var floatSizeSum = 0f;
            var repeatingFloatSizeSum = 0f;
            var normalSize = 0f;
            var repeatingSize = 0f;
            Part replacementPart = null;
            Parts repeatingReplacementParts = null;
            Rule repeatingReplacementFunc = null;
            foreach (var p in parts)
            {
                foreach (var part in p.parts)
                {
                    if (p.isRepeating)
                    {
                        repeatingSize += Mathf.Max(0, part.size);
                        if (part.sizingType == SizingType.Floating)
                        {
                            repeatingFloatSizeSum += Mathf.Max(0, part.size);
                        }
                        else if (part.sizingType == SizingType.Replacement)
                        {
                            repeatingReplacementParts = p;
                            repeatingReplacementFunc = part.func;
                        }
                    }
                    else
                    {
                        normalSize += Mathf.Max(0, part.size);
                        if (part.sizingType == SizingType.Floating)
                        {
                            floatSizeSum += Mathf.Max(0, part.size);
                        }
                        else if (part.sizingType == SizingType.Replacement)
                        {
                            replacementPart = part;
                        }
                    }
                }
            }

            float actualSize = 0;
            if (direction == Direction.Vertical)
            {
                actualSize = splitLayout.Size.y;
            }
            else if (direction == Direction.Horizontal)
            {
                actualSize = splitLayout.Size.x;
            }

            var remaining = actualSize - normalSize;

            int repeatCount = 0;
            var floatSizeLeftover = 0f;
            if (remaining < 0 && replacementPart != null)
            {
                parts = new List<Parts>
                {
                    new Parts(new List<Part> {new Part(SizingType.Normal, actualSize, replacementPart.func)},
                        false)
                };
            }
            else
            {
                repeatCount = Math.Max(0, (int) (remaining / repeatingSize));//Math.Round(remaining / repeatingSize, MidpointRounding.AwayFromZero));
                float repeatPartSize;
                if (repeatingReplacementParts != null && remaining / repeatingSize < 1)
                {
                    repeatPartSize = remaining;
                    repeatingReplacementParts.isRepeating = false;
                    repeatingReplacementParts.parts = new List<Part>
                        {new Part(SizingType.Normal, repeatPartSize, repeatingReplacementFunc)};
                    floatSizeLeftover = 0;
                }
                else
                {
                    repeatPartSize = repeatCount * repeatingSize;
                    var leftover = remaining - repeatPartSize;
                    floatSizeLeftover = leftover / (floatSizeSum + repeatCount * repeatingFloatSizeSum);
                }
            }


            var childPosition = Vector3.zero; //splitLayout.Position;
            var splitCount = 0;

            for (var i = 0; i < parts.Count; i++)
            {
                var childSize = splitLayout.Size;
                var nextPosition = childPosition;


                for (int j = 0; j < (parts[i].isRepeating ? repeatCount : 1); j++)
                {
                    foreach (var part in parts[i].parts)
                    {
                        if (part.size <= 0) continue;

                        float size;
                        if (part.sizingType == SizingType.Floating)
                        {
                            size = part.size + floatSizeLeftover * part.size;
                        }
                        else
                        {
                            size = part.size;
                        }


                        if (direction == Direction.Vertical)
                        {
                            childSize.y = size;
                            nextPosition += Vector3.up * childSize.y;
                        }
                        else if (direction == Direction.Horizontal)
                        {
                            childSize.x = size;
                            nextPosition += Vector3.right * childSize.x;
                        }

                        var newChild = splitLayout.AddChild( /*childPosition, childSize*/);
                        newChild.Index = index++;
                        splitCount++;
                        var position = childPosition;
                        var s = childSize;
                        newChild.Size = s;
                        newChild.Position += position;
                        newChild.Rules.Add(part.func);
                        childPosition = nextPosition;
                    }
                }
            }

            foreach (var splitLayoutChild in splitLayout.children)
            {
                splitLayoutChild.ParentSplitCount = splitCount;
            }
        }


        public LayoutBuilder AddChild()
        {
            var newChild = new LayoutBuilder(this)
            {
                Size = Size,
                rotation = rotation,
                rotationOrigin = rotationOrigin,
                keepOriginalUVs = keepOriginalUVs,
                textureHeight = textureHeight,
                textureWidth = textureWidth
            };
            children.Add(newChild);

            return newChild;
        }

        public LayoutBuilder Add(Modification modification, params Modification[] modifications)
        {
            var child = AddChild();
            modification(child, 0);
            foreach (var m in modifications)
            {
                m(child, 0);
            }

            return this;
        }

        public LayoutBuilder Add(Rule rule, params Rule[] rules)
        {
            var props = new List<Rule>();
            props.Add(rule);
            props.AddRange(rules);

            foreach (var p in props)
            {
                var child = AddChild();
                child.Rules.Add(p);
            }

            return this;
        }

        public LayoutBuilder Mesh(Mesh mesh, bool recalculateNormals = false,
            params Modification[] properties)
        {
            if (recalculateNormals)
            {
                mesh.RecalculateNormals();
            }

            var child = AddChild();

            child.keepOriginalUVs = true;
            foreach (var property in properties)
            {
                property(child, 0);
            }

            child.primitive = new MeshPrimitive(child.Name, child.Color, mesh);

            return this;
        }

        public LayoutBuilder MeshDraft(MeshDraft mesh,
            params Modification[] properties)
        {
            var child = AddChild();

            child.keepOriginalUVs = true;
            foreach (var property in properties)
            {
                property(child, 0);
            }

            var name = child.Name ?? mesh.name;
            child.primitive = new MeshDraftPrimitive(name, child.Color, mesh);

            return this;
        }

        public LayoutBuilder CenterX()
        {
            Rules.Add(layoutBuilder =>
            {
                var p = layoutBuilder.Position;
                p.x += layoutBuilder.parent.Size.x / 2 - layoutBuilder.Size.x / 2;
                layoutBuilder.Position = p;
            });
            return this;
        }

        public LayoutBuilder CenterY()
        {
            Rules.Add(layoutBuilder =>
            {
                var p = layoutBuilder.Position;
                p.y += layoutBuilder.parent.Size.y / 2 - layoutBuilder.Size.y / 2;
                layoutBuilder.Position = p;
            });
            return this;
        }

        public CompoundMeshDraft Build()
        {
            var stack = new Stack<LayoutBuilder>();
            stack.Push(this);

            while (stack.Count > 0)
            {
                var layoutBuilder = stack.Pop();

                if (layoutBuilder.Rules.Count > 0)
                {
                    while (layoutBuilder.Rules.Count > 0)
                    {
                        layoutBuilder.Rules[0](layoutBuilder);

                        layoutBuilder.Rules.RemoveAt(0);
                    }
                }

                if (layoutBuilder.parent != null)
                {
                    layoutBuilder.LookRotation = layoutBuilder.parent.LookRotation * layoutBuilder.LookRotation;
                    if (layoutBuilder.shouldOverrideUvOrigin)
                    {
                        layoutBuilder.uvOrigin = layoutBuilder.parent.LookRotation * layoutBuilder.Position +
                                                 layoutBuilder.parent.Position;
                    }
                    else
                    {
                        layoutBuilder.uvOrigin = layoutBuilder.parent.uvOrigin;
                    }
                    layoutBuilder.Position = layoutBuilder.parent.LookRotation * layoutBuilder.Position +
                                             layoutBuilder.parent.Position;
                }

                foreach (var child in layoutBuilder.children)
                {
                    stack.Push(child);
                }
            }

            stack = new Stack<LayoutBuilder>();
            stack.Push(this);

            var finalMeshDraft = new CompoundMeshDraft();

            while (stack.Count > 0)
            {
                var layoutBuilder = stack.Pop();

                if (layoutBuilder.primitive != null)
                {
                    var meshDraft = layoutBuilder.primitive.GetMesh();
                    meshDraft
                        .Scale(layoutBuilder.Size)
                        .Rotate(layoutBuilder.rotation, Vector3.Scale(layoutBuilder.rotationOrigin, layoutBuilder.Size))
                        .Rotate(layoutBuilder.LookRotation)
                        .Move(layoutBuilder.Position);
                    if (!layoutBuilder.keepOriginalUVs)
                    {
                        layoutBuilder.ApplyUVCoordinates(meshDraft);
                    }

                    finalMeshDraft.Add(meshDraft);
                }

                foreach (var child in layoutBuilder.children)
                {
                    stack.Push(child);
                }
            }

            return finalMeshDraft;
        }

        private void ApplyUVCoordinates(MeshDraft mesh)
        {
            List<Vector2> uv = new List<Vector2>(mesh.vertexCount);

            for (int i = 0; i < mesh.vertices.Count; i++)
            {
                var n1 = (Quaternion.LookRotation(mesh.normals[i]) * Vector3.right).normalized;
                var n2 = (Quaternion.LookRotation(mesh.normals[i]) * Vector3.up).normalized;

                var u = Vector3.Dot(mesh.vertices[i] - uvOrigin, n1);
                var v = Vector3.Dot(mesh.vertices[i] - uvOrigin, n2);
                var uvX = (u) / textureWidth;
                var uvY = (v) / textureHeight;
                uv.Add(new Vector2(uvX, uvY));
            }

            mesh.uv = uv;
        }

        public LayoutBuilder Scale(Action<LayoutBuilder, ModificationType> scale,
            params Action<LayoutBuilder, ModificationType>[] scales)
        {
            Rule func = layoutBuilder =>
            {
                scale(layoutBuilder, ModificationType.Scale);
                foreach (var action in scales)
                {
                    action(layoutBuilder, ModificationType.Scale);
                }
            };

            if (children.Count > 0)
            {
                foreach (var child in children)
                {
                    func(child);
                }
            }
            else
            {
                func(this);
            }

            return this;
        }


        public LayoutBuilder Translate(Action<LayoutBuilder, ModificationType> translate,
            params Action<LayoutBuilder, ModificationType>[] translates)
        {
            Rule func = layoutBuilder =>
            {
                translate(layoutBuilder, ModificationType.Translate);
                foreach (var action in translates)
                {
                    action(layoutBuilder, ModificationType.Translate);
                }
            };

            if (children.Count > 0)
            {
                foreach (var child in children)
                {
                    func(child);
                }
            }
            else
            {
                func(this);
            }

            return this;
        }

        public LayoutBuilder Rotate(float x, float y, float z)
        {
            return Rotate(x, y, z, Vector3.zero);
        }

        public LayoutBuilder Rotate(float x, float y, float z, Vector3 origin)
        {
            if (children.Count > 0)
            {
                foreach (var child in children)
                {
                    child.rotationOrigin = origin;
                    child.rotation *= Quaternion.Euler(x, y, z);
                }
            }
            else
            {
                rotationOrigin = origin;
                rotation *= Quaternion.Euler(x, y, z);
            }

            return this;
        }

        public LayoutBuilder QuadPrimitive(params Modification[] properties)
        {
            var child = AddChild();

            foreach (var property in properties)
            {
                property(child, 0);
            }

            child.primitive = new QuadPrimitive(child.Name, child.Color);

            return this;
        }


        public LayoutBuilder BoxPrimitive(params Modification[] properties)
        {
            var child = AddChild();
            foreach (var property in properties)
            {
                property(child, 0);
            }

            child.primitive = new BoxPrimitive(child.Name, child.Color);

            return this;
        }

        public LayoutBuilder CylinderPrimitive(params Modification[] properties)
        {
            var child = AddChild();
            child.keepOriginalUVs = true;
            foreach (var property in properties)
            {
                property(child, 0);
            }

            child.primitive = new CylinderPrimitive(child.Name, child.Color);

            return this;
        }

        public LayoutBuilder SetUV(float tw, float th)
        {
            if (children.Count > 0)
            {
                foreach (var child in children)
                {
                    child.keepOriginalUVs = false;
                    child.textureWidth = tw;
                    child.textureHeight = th;
                    child.shouldOverrideUvOrigin = true;
                }
            }
            else
            {
                keepOriginalUVs = false;
                textureWidth = tw;
                textureHeight = th;
                shouldOverrideUvOrigin = true;
            }

            return this;
        }

        public LayoutBuilder KeepOriginalUV(bool b)
        {
            if (children.Count > 0)
            {
                foreach (var child in children)
                {
                    child.keepOriginalUVs = b;
                }
            }
            else
            {
                keepOriginalUVs = b;
            }

            return this;
        }

        public LayoutBuilder Extrude(float amount, Action<LayoutBuilder, float> side,
            params Action<LayoutBuilder, float>[] sides)
        {
            side(this, amount);
            foreach (var action in sides)
            {
                action(this, amount);
            }

            return this;
        }

        public LayoutBuilder Offset(float amount, Action<LayoutBuilder, OffsetSides> side,
            params Action<LayoutBuilder, OffsetSides>[] sides)
        {
            Rules.Add(layoutBuilder =>
            {
                var offsetSides = new OffsetSides();
                side(layoutBuilder, offsetSides);
                foreach (var action in sides)
                {
                    action(layoutBuilder, offsetSides);
                }

                layoutBuilder.SplitY(
                    WithSize(amount, offsetSides.bottom ?? None),
                    WithFloatingSize(1,
                        lb =>
                        {
                            lb.SplitX(
                                WithSize(amount, offsetSides.right ?? None),
                                WithFloatingSize(1, offsetSides.middle ?? None),
                                WithSize(amount, offsetSides.left ?? None)
                            );
                        }
                    ),
                    WithSize(amount, offsetSides.top ?? None)
                );
                // var rect = new List<Vector2>
                // {
                //     Vector2.zero,
                //     new Vector2(layoutBuilder.Size.x, 0),
                //     new Vector2(layoutBuilder.Size.x, layoutBuilder.Size.y),
                //     new Vector2(0, layoutBuilder.Size.y)
                // };
                // var offsetPolygon = Geometry.OffsetPolygon(rect, -amount);
                // foreach (var vector2 in rect)
                // {
                //     Debug.Log(vector2);
                // Utils.DrawDebugSphere(vector2.ToVector3XY());
                //     
                // }
                // side(layoutBuilder, offsetPolygon);
                // foreach (var action in sides)
                // {
                //     action(layoutBuilder, offsetPolygon);
                // }
            });
            return this;
        }

        public class OffsetSides
        {
            public Rule middle;
            public Rule top;
            public Rule bottom;
            public Rule left;
            public Rule right;
        }

        private enum Direction
        {
            None = 0,
            Horizontal = 1,
            Vertical = 2
        }

        public enum ModificationType
        {
            None = 0,
            Scale = 1,
            Translate = 2
        }
    }

    public enum SizingType
    {
        Normal,
        Floating,
        Replacement
    }

    public class Parts
    {
        public List<Part> parts;

        public bool isRepeating;

        public Parts(List<Part> parts, bool isRepeating)
        {
            this.parts = parts;
            this.isRepeating = isRepeating;
        }
    }

    public class Part
    {
        public readonly SizingType sizingType;

        public readonly float size;

        public readonly Rule func;

        public Part(SizingType sizingType, float size, Rule func)
        {
            this.sizingType = sizingType;
            this.size = size;
            this.func = func;
        }
    }

    internal abstract class Primitive
    {
        private readonly Color color;
        private readonly string name;

        protected Primitive(string name, Color color)
        {
            this.name = name;
            this.color = color;
        }

        protected abstract MeshDraft CreateMesh();

        public MeshDraft GetMesh()
        {
            var mesh = CreateMesh();
            mesh.Paint(color);
            mesh.name = name;
            return mesh;
        }
    }

    internal class QuadPrimitive : Primitive
    {
        public QuadPrimitive(string name, Color color) : base(name ?? "Quad", color)
        {
        }

        protected override MeshDraft CreateMesh()
        {
            var quad = MeshDraft.Quad(-Vector2.left, Vector2.left, Vector2.up);
            return quad;
        }
    }

    internal class BoxPrimitive : Primitive
    {
        public BoxPrimitive(string name, Color color) : base(name ?? "Box", color)
        {
        }

        protected override MeshDraft CreateMesh()
        {
            var box = MeshDraft.PartialBox(Vector3.right, Vector3.forward, Vector3.up,
                Directions.All, Vector3.zero);
            return box;
        }
    }

    internal class CylinderPrimitive : Primitive
    {
        public CylinderPrimitive(string name, Color color) : base(name ?? "Cylinder", color)
        {
        }

        protected override MeshDraft CreateMesh()
        {
            var cylinder = MeshDraft.Cylinder(0.5f, 10, 1);
            cylinder.Rotate(Quaternion.LookRotation(Vector3.up));
            cylinder.Move(new Vector3(1.0f / 2, 1.0f / 2, 1.0f / 2));

            return cylinder;
        }
    }

    internal class MeshPrimitive : Primitive
    {
        private readonly Mesh mesh;

        public MeshPrimitive(string name, Color color, Mesh mesh) : base(name ?? mesh.name, color)
        {
            this.mesh = mesh;
        }

        protected override MeshDraft CreateMesh()
        {
            var size = new Vector3(1f / mesh.bounds.size.x, 1f / mesh.bounds.size.y,
                1f / mesh.bounds.size.z);
            var newMeshDraft = new MeshDraft(mesh);

            newMeshDraft.Move(new Vector3(mesh.bounds.size.x / 2,
                mesh.bounds.size.y / 2, 0));

            newMeshDraft.Scale(size);

            return newMeshDraft;
        }
    }

    internal class MeshDraftPrimitive : Primitive
    {
        private readonly MeshDraft mesh;

        public MeshDraftPrimitive(string name, Color color, MeshDraft mesh) : base(name, color)
        {
            this.mesh = new MeshDraft().Add(mesh);
        }

        protected override MeshDraft CreateMesh()
        {
            var bounds = mesh.Bounds;
            var size = new Vector3(1f / bounds.size.x, 1f / bounds.size.y,
                1f / bounds.size.z);
            var newMeshDraft = mesh;

            newMeshDraft.Move(new Vector3(bounds.size.x / 2,
                bounds.size.y / 2, 0));

            newMeshDraft.Scale(size);

            return newMeshDraft;
        }
    }


    public static class MyExtensions
    {
        public static Rule Bind<T>(this Func<LayoutBuilder, T, LayoutBuilder> func, T arg)
        {
            return (p1) => func(p1, arg);
        }
    }
}