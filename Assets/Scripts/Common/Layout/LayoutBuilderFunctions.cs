﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProceduralToolkit;
using UnityEngine;

namespace BuildingGenerator.Common.Layout
{
    public static class LayoutBuilderFunctions
    {
        public static Modification Name(string name)
        {
            return (layoutBuilder, _) => { layoutBuilder.Name = name; };
        }

        public static Modification UseColor(Color color)
        {
            return (layoutBuilder, _) => { layoutBuilder.Color = color; };
        }

        public static Modification RecalculateUVs()
        {
            return (layoutBuilder, _) => { layoutBuilder.KeepOriginalUV(false); };
        }

        public static Modification Rotate(float x, float y, float z) =>
            (layoutBuilder, _) => layoutBuilder.Rotate(x, y, z);
        
        public static Modification Rotate(float x, float y, float z, Vector3 origin) =>
            (layoutBuilder, _) => layoutBuilder.Rotate(x, y, z, origin);

        public static Modification SetUV(float width, float height) =>
            (layoutBuilder, _) => layoutBuilder.SetUV(width, height);

        public static Modification KeepUVs()
        {
            return (layoutBuilder, _) => { layoutBuilder.KeepOriginalUV(true); };
        }

        public static Action<List<Parts>> WithSize(float size, Modification modification,
            params Modification[] modifications)
        {
            Rule f = layoutBuilder =>
            {
                modification(layoutBuilder, 0);
                foreach (var rule in modifications)
                {
                    rule(layoutBuilder, 0);
                }
            };
            return parts =>
            {
                parts.Add(new Parts(
                    new List<Part> {new Part(SizingType.Normal, size, f)}, false));
            };
        }

        public static Action<List<Parts>> WithSize(float size, Rule rule, params Rule[] rules)
        {
            Rule func;
            if (rules.Length > 0)
            {
                func = layoutBuilder => { layoutBuilder.Add(rule, rules); };
            }
            else
            {
                func = rule;
            }

            return parts =>
            {
                parts.Add(new Parts(
                    new List<Part> {new Part(SizingType.Normal, size, func)}, false));
            };
        }
        
        public static Action<List<Parts>> NoSpaceFallback(Rule rule)
        {
            return parts =>
            {
                parts.Add(new Parts(
                    new List<Part> {new Part(SizingType.Replacement, 0, rule)}, false));
            };
        }

        public static Action<List<Parts>> WithFloatingSize(float size, Modification modification,
            params Modification[] modifications)
        {
            Rule f = layoutBuilder =>
            {
                modification(layoutBuilder, 0);
                foreach (var rule in modifications)
                {
                    rule(layoutBuilder, 0);
                }
            };
            return parts =>
            {
                parts.Add(new Parts(
                    new List<Part> {new Part(SizingType.Floating, size, f)}, false));
            };
        }

        public static Action<List<Parts>> WithFloatingSize(float size, Rule rule, params Rule[] rules)
        {
            Rule func;
            if (rules.Length > 0)
            {
                func = layoutBuilder => { layoutBuilder.Add(rule, rules); };
            }
            else
            {
                func = rule;
            }

            return parts =>
            {
                parts.Add(new Parts(
                    new List<Part> {new Part(SizingType.Floating, size, func)}, false));
            };
        }

        public static Action<List<Parts>> WithRepeat(Action<List<Parts>> repeat,
            params Action<List<Parts>>[] repeats)
        {
            return parts =>
            {
                List<Parts> p = new List<Parts>();

                repeat(p);
                foreach (var r in repeats)
                {
                    r(p);
                }

                var newParts = p.SelectMany(pa => pa.parts).ToList();
                parts.Add(new Parts(newParts, true));
            };
        }

        public static Action<LayoutBuilder, LayoutBuilder.ModificationType> X(float x)
        {
            return (layoutBuilder, type) =>
            {
                if (type == LayoutBuilder.ModificationType.Scale)
                {
                    var s = layoutBuilder.Size;
                    s.x = x;
                    layoutBuilder.Size = s;
                }
                else if (type == LayoutBuilder.ModificationType.Translate)
                {
                    var o = layoutBuilder.Position;
                    o.x += x;
                    layoutBuilder.Position = o;
                }
            };
        }

        public static Action<LayoutBuilder, LayoutBuilder.ModificationType> Y(float y)
        {
            return (layoutBuilder, type) =>
            {
                if (type == LayoutBuilder.ModificationType.Scale)
                {
                    var s = layoutBuilder.Size;
                    s.y = y;
                    layoutBuilder.Size = s;
                }
                else if (type == LayoutBuilder.ModificationType.Translate)
                {
                    var o = layoutBuilder.Position;
                    o.y += y;
                    layoutBuilder.Position = o;
                }
            };
        }

        public static Action<LayoutBuilder, LayoutBuilder.ModificationType> Z(float z)
        {
            return (layoutBuilder, type) =>
            {
                if (type == LayoutBuilder.ModificationType.Scale)
                {
                    var s = layoutBuilder.Size;
                    s.z = z;
                    layoutBuilder.Size = s;
                }
                else if (type == LayoutBuilder.ModificationType.Translate)
                {
                    var o = layoutBuilder.Position;
                    o.z += z;
                    layoutBuilder.Position = o;
                }
            };
        }

        public static Action<LayoutBuilder, LayoutBuilder.ModificationType> RelativeX(float x)
        {
            return (layoutBuilder, type) =>
            {
                if (type == LayoutBuilder.ModificationType.Scale)
                {
                    var s = layoutBuilder.Size;
                    s.x *= x;
                    layoutBuilder.Size = s;
                }
                else if (type == LayoutBuilder.ModificationType.Translate)
                {
                    var o = layoutBuilder.Position;
                    o.x += x * layoutBuilder.Size.x;
                    layoutBuilder.Position = o;
                }
            };
        }

        public static Action<LayoutBuilder, LayoutBuilder.ModificationType> RelativeY(float y)
        {
            return (layoutBuilder, type) =>
            {
                if (type == LayoutBuilder.ModificationType.Scale)
                {
                    var s = layoutBuilder.Size;
                    s.y *= y;
                    layoutBuilder.Size = s;
                }
                else if (type == LayoutBuilder.ModificationType.Translate)
                {
                    var o = layoutBuilder.Position;
                    o.y += y * layoutBuilder.Size.y;
                    layoutBuilder.Position = o;
                }
            };
        }

        public static Action<LayoutBuilder, LayoutBuilder.ModificationType> RelativeZ(float z)
        {
            return (layoutBuilder, type) =>
            {
                if (type == LayoutBuilder.ModificationType.Scale)
                {
                    var s = layoutBuilder.Size;
                    s.z *= z;
                    layoutBuilder.Size = s;
                }
                else if (type == LayoutBuilder.ModificationType.Translate)
                {
                    var o = layoutBuilder.Position;
                    o.z += z * layoutBuilder.Size.z;
                    layoutBuilder.Position = o;
                }
            };
        }

        public static Modification Translate(
            Action<LayoutBuilder, LayoutBuilder.ModificationType> translate,
            params Action<LayoutBuilder, LayoutBuilder.ModificationType>[] translates)
        {
            return (layoutBuilder, _) => { layoutBuilder.Translate(translate, translates); };
        }

        public static Modification Scale(Action<LayoutBuilder, LayoutBuilder.ModificationType> scale,
            params Action<LayoutBuilder, LayoutBuilder.ModificationType>[] scales)
        {
            return (layoutBuilder, _) => { layoutBuilder.Scale(scale, scales); };
        }

        public static Rule QuadPrimitive(params Modification[] properties) =>
            layoutBuilder => layoutBuilder.QuadPrimitive(properties);

        public static Rule BoxPrimitive(params Modification[] properties) =>
            layoutBuilder => layoutBuilder.BoxPrimitive(properties);

        public static Rule CylinderPrimitive(params Modification[] properties) =>
            layoutBuilder => layoutBuilder.CylinderPrimitive(properties);

        public static Rule Mesh(Mesh mesh, bool recalculateNormals = false, params Modification[] properties) =>
            layoutBuilder => layoutBuilder.Mesh(mesh, recalculateNormals, properties);
        
        public static Rule MeshDraft(MeshDraft mesh, params Modification[] properties) =>
            layoutBuilder => layoutBuilder.MeshDraft(mesh, properties);

        public static Rule Extrude(float amount, Action<LayoutBuilder, float> side,
            params Action<LayoutBuilder, float>[] sides) =>
            (layoutBuilder) => layoutBuilder.Extrude(amount, side, sides);

        public static Modification CenterX() => (layoutBuilder, _) => layoutBuilder.CenterX();
        public static Modification CenterY() => (layoutBuilder, _) => layoutBuilder.CenterY();

        public static Rule SplitY(Action<List<Parts>> split, params Action<List<Parts>>[] splits) =>
            layoutBuilder => layoutBuilder.SplitY(split, splits);

        public static Rule SplitX(Action<List<Parts>> split, params Action<List<Parts>>[] splits) =>
            layoutBuilder => layoutBuilder.SplitX(split, splits);

        public static Modification Add(Rule rule, params Rule[] rules) =>
            (layoutBuilder, _) => layoutBuilder.Add(rule, rules);

        public static void None(LayoutBuilder layoutBuilder)
        {
            //layoutBuilder.meshDraft = new CompoundMeshDraft();
        }

        public static Action<LayoutBuilder, float> Front(Rule func)
        {
            return (layoutBuilder, amount) =>
            {
                var newChild = layoutBuilder.AddChild();

                // newChild.Funcs.Add(lb =>
                // {
                if (amount > 0)
                {
                    newChild.Position += Vector3.forward * amount;
                    newChild.Size = layoutBuilder.Size;
                }
                else
                {
                    newChild.Position += Vector3.right * layoutBuilder.Size.x;
                    newChild.Size = layoutBuilder.Size;
                }

                var sign = Math.Sign(amount);
                newChild.LookRotation = Quaternion.LookRotation(Vector3.forward * sign, Vector3.up);
                // });
                newChild.Rules.Add(func);
            };
        }

        public static Action<LayoutBuilder, float> Back(Rule func)
        {
            return (layoutBuilder, amount) =>
            {
                var newChild = layoutBuilder.AddChild();

                // newChild.Funcs.Add(lb =>
                // {
                if (amount > 0)
                {
                    newChild.Position += Vector3.right * layoutBuilder.Size.x;
                    newChild.Size = layoutBuilder.Size;
                }
                else
                {
                    newChild.Position += Vector3.forward * amount;
                    newChild.Size = layoutBuilder.Size;
                }

                var sign = Math.Sign(amount);
                newChild.LookRotation = Quaternion.LookRotation(Vector3.back * sign);
                // });
                newChild.Rules.Add(func);
            };
        }

        public static Action<LayoutBuilder, float> Top(Rule func)
        {
            return (layoutBuilder, amount) =>
            {
                var newChild =
                    layoutBuilder.AddChild();

                // newChild.Funcs.Add(lb =>
                // {
                newChild.Position += Vector3.forward * amount + Vector3.up * layoutBuilder.Size.y;
                newChild.Size = new Vector3(layoutBuilder.Size.x, Math.Abs(amount), 1);
                var sign = Math.Sign(amount);
                newChild.LookRotation = Quaternion.LookRotation(Vector3.up * sign, Vector3.back * sign);
                // });

                newChild.Rules.Add(func);
            };
        }

        public static Action<LayoutBuilder, float> Bottom(Rule func)
        {
            return (layoutBuilder, amount) =>
            {
                var newChild = layoutBuilder.AddChild();

                // newChild.Funcs.Add(lb =>
                // {
                newChild.Size = new Vector3(layoutBuilder.Size.x, Math.Abs(amount), 1);
                var sign = Math.Sign(amount);
                newChild.LookRotation = Quaternion.LookRotation(Vector3.down * sign, Vector3.forward * sign);
                // });
                newChild.Rules.Add(func);
            };
        }

        public static Action<LayoutBuilder, float> Right(Rule func)
        {
            return (layoutBuilder, amount) =>
            {
                var newChild = layoutBuilder.AddChild();

                // newChild.Funcs.Add(lb =>
                // {
                newChild.Size = new Vector3(Math.Abs(amount), layoutBuilder.Size.y, 1);
                var sign = Math.Sign(amount);
                newChild.LookRotation = Quaternion.LookRotation(Vector3.left * sign, Vector3.up);
                // });

                newChild.Rules.Add(func);
            };
        }

        public static Action<LayoutBuilder, float> Left(Rule func)
        {
            return (layoutBuilder, amount) =>
            {
                var newChild = layoutBuilder.AddChild();

                // newChild.Funcs.Add(lb =>
                // {
                newChild.Position += Vector3.forward * amount + Vector3.right * layoutBuilder.Size.x;
                newChild.Size = new Vector3(Math.Abs(amount), layoutBuilder.Size.y, 1);
                var sign = Math.Sign(amount);
                newChild.LookRotation = Quaternion.LookRotation(Vector3.right * sign, Vector3.up);
                // });

                newChild.Rules.Add(func);
            };
        }

        public static Action<LayoutBuilder, float> All(Rule func, Directions parts = Directions.All)
        {
            return (layoutBuilder, amount) =>
            {
                if (parts.HasFlag(Directions.Forward)) Front(func)(layoutBuilder, amount);
                if (parts.HasFlag(Directions.Back)) Back(func)(layoutBuilder, amount);
                if (parts.HasFlag(Directions.Down)) Bottom(func)(layoutBuilder, amount);
                if (parts.HasFlag(Directions.Up)) Top(func)(layoutBuilder, amount);
                if (parts.HasFlag(Directions.Left)) Left(func)(layoutBuilder, amount);
                if (parts.HasFlag(Directions.Right)) Right(func)(layoutBuilder, amount);
            };
        }

        public static Rule DebugQuad(Color color)
        {
            return l =>
            {
                l.QuadPrimitive(
                    UseColor(color),
                    Name(color + "Quad")
                );
            };
        }

        public static Action<LayoutBuilder, LayoutBuilder.OffsetSides> OffsetTop(Rule rule)
        {
            return (layoutBuilder, offsetSides) => { offsetSides.top = rule; };
        }

        public static Action<LayoutBuilder, LayoutBuilder.OffsetSides> OffsetBottom(Rule rule)
        {
            return (layoutBuilder, offsetSides) => { offsetSides.bottom = rule; };
        }

        public static Action<LayoutBuilder, LayoutBuilder.OffsetSides> OffsetLeft(Rule rule)
        {
            return (layoutBuilder, offsetSides) => { offsetSides.left = rule; };
        }

        public static Action<LayoutBuilder, LayoutBuilder.OffsetSides> OffsetRight(Rule rule)
        {
            return (layoutBuilder, offsetSides) => { offsetSides.right = rule; };
        }


        public static Action<LayoutBuilder, LayoutBuilder.OffsetSides> OffsetMiddle(Rule rule)
        {
            return (layoutBuilder, offsetSides) => { offsetSides.middle = rule; };
        }

        // public static Action<LayoutBuilderV2, List<Vector2>> OffsetTop(Rule func)
        // {
        //     return (layoutBuilder, offsetPolygon) =>
        //     {
        //         var newChild =
        //             layoutBuilder.AddChild();
        //
        //         newChild.Funcs.Add(lb =>
        //         {
        //             lb.Position += new Vector3(lb.Position.x , offsetPolygon[3].y, lb.Position.z);
        //             var offsetPolygonHeight = (offsetPolygon[3] - offsetPolygon[0]).magnitude;
        //             lb.Size = new Vector3(lb.Size.x, (lb.Size.y-offsetPolygonHeight)/2, lb.Size.z);
        //         });
        //
        //         newChild.Funcs.Add(func);
        //     };
        // }
    }
}