﻿using System;
using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;

namespace BuildingGenerator.Common.Layout.V1
{
    public delegate void Rule(LayoutBuilderV1 l);

    public class LayoutBuilderV1
    {
        public Vector3 Size { get; set; } = Vector3.one;
        private Vector3 originalSize;
        public Vector3 Position { get; set; }

        public Vector3 Origin { get; set; }

        private LayoutBuilderV1 parent;

        private SplitBuilder splitBuilder;
        private Quaternion rotation = Quaternion.identity;

        private Vector3 currentPosition;

        private Vector3 currentNormal;
        private Vector3 currentUp;
        private Quaternion lookRotation = Quaternion.LookRotation(Vector3.forward);
        private Quaternion currentLookRotation;

        private float textureHeight = 3;
        private float textureWidth = 3;
        private string textureName;
        private bool keepOriginalUVs = true;
        private Vector2 uvOrigin;

        public LayoutBuilderV1()
        {
        }

        private LayoutBuilderV1(LayoutBuilderV1 parent, Vector3 originalSize)
        {
            this.parent = parent;
            this.originalSize = originalSize;
        }

        public int Index { get; set; }

        public int ParentSplitCount { get; set; }

        private CompoundMeshDraft meshDraft;

        private Rule func;

        protected List<LayoutBuilderV1> children { get; set; } = new List<LayoutBuilderV1>();
        private Direction direction = Direction.NONE;


        public SplitBuilder SplitX()
        {
            return new SplitBuilder(this, Direction.HORIZONTAL);
        }

        public SplitBuilder SplitY()
        {
            return new SplitBuilder(this, Direction.VERTICAL);
        }

        public LayoutBuilderV1 AddChild()
        {
            var newChild = new LayoutBuilderV1(this, Size)
            {
                Size = Size,
                Position = Origin,
                rotation = rotation,
                textureHeight = textureHeight,
                textureWidth = textureWidth,
                keepOriginalUVs = keepOriginalUVs,
                uvOrigin = uvOrigin
                //normal = normal
            };
            children.Add(newChild);

            return newChild;
        }

        public LayoutBuilderV1 AddChild(Vector3 position, Vector3 size)
        {
            var newChild = AddChild();
            newChild.Position = position + Origin;
            newChild.originalSize = size;
            newChild.Size = size;

            return newChild;
        }


        public LayoutBuilderV1 Add(Rule func)
        {
            var child = AddChild();
            child.func = func;
            return this;
        }

        public LayoutBuilderV1 Mesh(Mesh mesh, string name = null, bool recalculateNormals = false)
        {
            if (recalculateNormals)
            {
                mesh.RecalculateNormals();
            }
            var newMeshDraft = new MeshDraft(mesh);
            if (name != null)
            {
                newMeshDraft.name = name;
            }

            var m = new CompoundMeshDraft().Add(newMeshDraft).Move(new Vector3(mesh.bounds.size.x / 2,
                mesh.bounds.size.y / 2 /*mesh.bounds.size.z / 2*/, 0));
            //m.tangents = new List<Vector4>();

            var size = new Vector3(Size.x / mesh.bounds.size.x, Size.y / mesh.bounds.size.y,
                Size.z / mesh.bounds.size.z);

            var child = AddChild(Vector3.zero, size);
            child.meshDraft = m;
            child.keepOriginalUVs = true;
            return child;
        }

        public LayoutBuilderV1 KeepOriginalUVs(bool v = true)
        {
            keepOriginalUVs = v;
            return this;
        }

        public LayoutBuilderV1 CenterX()
        {
            var p = Origin;
            p.x = originalSize.x / 2 - Size.x / 2;
            Origin = p;
            return this;
        }
        
        public LayoutBuilderV1 CenterY()
        {
            var p = Origin;
            p.y = originalSize.y / 2 - Size.y / 2;
            Origin = p;
            return this;
        }

        public CompoundMeshDraft Build()
        {
            var stack = new Stack<LayoutBuilderV1>();
            var finalMeshDraft = new CompoundMeshDraft();
            stack.Push(this);

            currentPosition = Position;
            currentNormal = Vector3.up;
            currentLookRotation = lookRotation;
            // currentPosition =
            //     currentLookRotation*
            //         (Position - new Vector3(Size.x/2, 0, 0)) + new Vector3(Size.x/2, 0, 0);

            while (stack.Count > 0)
            {
                var layoutBuilder = stack.Pop();
                layoutBuilder.splitBuilder?.Split();

                if (layoutBuilder.meshDraft != null)
                {
                    //TODO add rotate method with anchor
                    layoutBuilder.meshDraft
                        .Scale(layoutBuilder.Size) //.Move(-new Vector3(layoutBuilder.Size.x, 0, 0)/2)
                        .Rotate(layoutBuilder.rotation)
                        .Rotate(layoutBuilder.currentLookRotation)
                        // .Move(new Vector3(layoutBuilder.Size.x, 0, 0)/2)
                        .Move(layoutBuilder.currentPosition);

                    if (!layoutBuilder.keepOriginalUVs)
                    {
                        layoutBuilder.ApplyUVCoordinates();
                    }

                    finalMeshDraft.Add(layoutBuilder.meshDraft);
                }
                else if (layoutBuilder.children.Count > 0)
                {
                    for (var i = 0; i < layoutBuilder.children.Count; i++)
                    {
                        layoutBuilder.children[i].currentLookRotation =
                            layoutBuilder.currentLookRotation * layoutBuilder.children[i].lookRotation;


                        layoutBuilder.children[i].currentPosition =
                            layoutBuilder.currentLookRotation *
                            (layoutBuilder.children[i].Position) + layoutBuilder.currentPosition;

                        stack.Push(layoutBuilder.children[i]);
                        //finalMeshDraft.Add(layoutBuilder.children[i].Build());
                    }


                    //return finalMeshDraft;
                }
                else if (layoutBuilder.func != null)
                {
                    var layoutBuilderFunction = layoutBuilder.func;
                    layoutBuilder.func = null;
                    layoutBuilderFunction(layoutBuilder);
                    stack.Push(layoutBuilder);
                    //return layoutBuilder.Build();
                }
            }

            return finalMeshDraft;
        }

        private void ApplyUVCoordinates()
        {
            foreach (var mesh in meshDraft)
            {
                List<Vector2> uv = new List<Vector2>(mesh.vertexCount);

                for (int i = 0; i < mesh.vertices.Count; i++)
                {
                    var n1 = (Quaternion.LookRotation(mesh.normals[i]) * Vector3.right).normalized;
                    var n2 = (Quaternion.LookRotation(mesh.normals[i]) * Vector3.up).normalized;
                
                    // var u = Vector3.Project(mesh.vertices[i], v1).magnitude;
                    // var uSign = Mathf.Sign(Vector3.Dot(mesh.vertices[i], v1));
                
                    // var v = Vector3.Project(mesh.vertices[i], v2).magnitude;
                    // var vSign = Mathf.Sign(Vector3.Dot(mesh.vertices[i], v2));
                    var u = Vector3.Dot(mesh.vertices[i], n1);
                    var v = Vector3.Dot(mesh.vertices[i], n2);
                    var uvX = (u ) / textureWidth;
                    var uvY = (v ) / textureHeight;
                    uv.Add(new Vector2(uvX, uvY));
                }

                mesh.uv = uv;
            }
        }

        public ScaleBuilder Scale()
        {
            return new ScaleBuilder(this);
        }

        public TranslateBuilder Translate()
        {
            return new TranslateBuilder(this);
        }

        public LayoutBuilderV1 Rotate(float x, float y, float z)
        {
            rotation = Quaternion.Euler(x, y, z);
            return this;
        }

        public LayoutBuilderV1 QuadPrimitive(Color color, String name = null)
        {
            var quad = MeshDraft.Quad(-Vector2.left, Vector2.left, Vector2.up);
            quad.Paint(color);

            if (name != null)
            {
                quad.name = name;
            }

            var child = AddChild();
            child.meshDraft = new CompoundMeshDraft().Add(quad);


            return child;
        }

        public LayoutBuilderV1 BoxPrimitive(Color color, String name = null)
        {
            var box = MeshDraft.PartialBox(Vector3.right, Vector3.forward, Vector3.up,
                Directions.All, Vector3.zero);
            //box.Move(new Vector3(1.0f / 2, 1.0f / 2, 1.0f / 2));
            box.Paint(color);

            if (name != null)
            {
                box.name = name;
            }


            var child = AddChild();
            child.meshDraft = new CompoundMeshDraft().Add(box);
            //child.uvIsAlreadyDone = true;

            return child;
        }

        public LayoutBuilderV1 CylinderPrimitive(Color color, string name = null)
        {
            var cylinder = MeshDraft.Cylinder(0.5f, 10, 1);
            cylinder.Rotate(Quaternion.LookRotation(Vector3.up));
            cylinder.Move(new Vector3(1.0f / 2, 1.0f / 2, 1.0f / 2));
            cylinder.Paint(color);

            if (name != null)
            {
                cylinder.name = name;
            }

            keepOriginalUVs = true;

            var child = AddChild();
            child.meshDraft = new CompoundMeshDraft().Add(cylinder);

            return child;
        }

        public void None(LayoutBuilderV1 layoutBuilder)
        {
            layoutBuilder.meshDraft = new CompoundMeshDraft();
        }

        public LayoutBuilderV1 And()
        {
            if (parent == null)
            {
                throw new InvalidOperationException("Cannot call And on root!");
            }

            return parent;
        }

        public LayoutBuilderV1 SetUV(float textureWidth, float textureHeight)
        {
            keepOriginalUVs = false;
            this.textureWidth = textureWidth;
            this.textureHeight = textureHeight;
            uvOrigin = Position.ToVector2XY();
            return this;
        }

        public LayoutBuilderV1 SetTextureName(string textureName)
        {
            this.textureName = textureName;
            return this;
        }

        public ExtrudeBuilder Extrude(float size)
        {
            return new ExtrudeBuilder(size, this);
        }

        public class ScaleBuilder
        {
            private LayoutBuilderV1 layoutBuilder;

            public ScaleBuilder(LayoutBuilderV1 layoutBuilder)
            {
                this.layoutBuilder = layoutBuilder;
            }

            public ScaleBuilder X(float x)
            {
                var s = layoutBuilder.Size;
                s.x = x;
                layoutBuilder.Size = s;

                return this;
            }

            public ScaleBuilder Y(float y)
            {
                var s = layoutBuilder.Size;
                s.y = y;
                layoutBuilder.Size = s;

                return this;
            }

            public ScaleBuilder Z(float z)
            {
                var s = layoutBuilder.Size;
                s.z = z;
                layoutBuilder.Size = s;

                return this;
            }

            public ScaleBuilder RelativeX(float x)
            {
                var s = layoutBuilder.Size;
                s.x *= x;
                layoutBuilder.Size = s;

                return this;
            }

            public ScaleBuilder RelativeY(float y)
            {
                var s = layoutBuilder.Size;
                s.y *= y;
                layoutBuilder.Size = s;

                return this;
            }

            public ScaleBuilder RelativeZ(float z)
            {
                var s = layoutBuilder.Size;
                s.z *= z;
                layoutBuilder.Size = s;

                return this;
            }

            public LayoutBuilderV1 And()
            {
                return layoutBuilder;
            }
        }

        public class TranslateBuilder
        {
            private LayoutBuilderV1 layoutBuilder;

            public TranslateBuilder(LayoutBuilderV1 layoutBuilder)
            {
                this.layoutBuilder = layoutBuilder;
            }

            public TranslateBuilder X(float x)
            {
                var o = layoutBuilder.Origin;
                o.x = x;
                layoutBuilder.Origin = o;

                return this;
            }

            public TranslateBuilder Y(float y)
            {
                var o = layoutBuilder.Origin;
                o.y = y;
                layoutBuilder.Origin = o;

                return this;
            }

            public TranslateBuilder Z(float z)
            {
                var o = layoutBuilder.Origin;
                o.z = z;
                layoutBuilder.Origin = o;

                return this;
            }

            public TranslateBuilder RelativeX(float x)
            {
                var o = layoutBuilder.Origin;
                o.x = x * layoutBuilder.Size.x;
                layoutBuilder.Origin = o;

                return this;
            }

            public TranslateBuilder RelativeY(float y)
            {
                var o = layoutBuilder.Origin;
                o.y = y * layoutBuilder.Size.y;
                layoutBuilder.Origin = o;

                return this;
            }

            public TranslateBuilder RelativeZ(float z)
            {
                var o = layoutBuilder.Origin;
                o.z = z * layoutBuilder.Size.z;
                layoutBuilder.Origin = o;

                return this;
            }

            public LayoutBuilderV1 And()
            {
                return layoutBuilder;
            }
        }


        public class SplitBuilder
        {
            private LayoutBuilderV1 parentLayout;

            private LayoutBuilderV1 splitLayout;

            private List<Parts> parts = new List<Parts>();

            private Direction direction;

            public SplitBuilder(LayoutBuilderV1 layoutBuilder, Direction dir)
            {
                direction = dir;
                this.parentLayout = layoutBuilder;

                splitLayout = layoutBuilder.AddChild();
                splitLayout.direction = dir;
                splitLayout.splitBuilder = this;
            }


            public void Split()
            {
                int index = 0;
                var floatSizeSum = 0f;
                var repeatingFloatSizeSum = 0f;
                var normalSize = 0f;
                var repeatingSize = 0f;
                foreach (var p in parts)
                {
                    foreach (var part in p.parts)
                    {
                        if (p.isRepeating)
                        {
                            repeatingSize += part.size;
                            if (part.SizingType == SizingType.FLOATING)
                            {
                                repeatingFloatSizeSum += part.size;
                            }
                        }
                        else
                        {
                            normalSize += part.size;
                            if (part.SizingType == SizingType.FLOATING)
                            {
                                floatSizeSum += part.size;
                            }
                        }
                    }
                }

                var remaining = 0f;
                if (splitLayout.direction == Direction.VERTICAL)
                {
                    remaining = splitLayout.Size.y - normalSize;
                }
                else if (splitLayout.direction == Direction.HORIZONTAL)
                {
                    remaining = splitLayout.Size.x - normalSize;
                }

                var repeatCount = 0;

                repeatCount = Math.Max(0, (int) Math.Round(remaining / repeatingSize));
                var leftover = remaining - repeatCount * repeatingSize;

                var floatSizeLeftover = leftover / (floatSizeSum + repeatCount * repeatingFloatSizeSum);

                var childPosition = Vector3.zero; //splitLayout.Position;
                var splitCount = 0;
                for (var i = 0; i < parts.Count; i++)
                {
                    var childSize = splitLayout.Size;
                    var nextPosition = childPosition;


                    for (int j = 0; j < (parts[i].isRepeating ? repeatCount : 1); j++)
                    {
                        foreach (var part in parts[i].parts)
                        {
                            float size;
                            if (part.SizingType == SizingType.FLOATING)
                            {
                                size = part.size + floatSizeLeftover * part.size;
                            }
                            else
                            {
                                size = part.size;
                            }

                            if (direction == Direction.VERTICAL)
                            {
                                childSize.y = size;
                                nextPosition += Vector3.up * childSize.y;
                            }
                            else if (direction == Direction.HORIZONTAL)
                            {
                                childSize.x = size;
                                nextPosition += Vector3.right * childSize.x;
                            }

                            var newChild = splitLayout.AddChild(childPosition, childSize);
                            newChild.Index = index++;
                            splitCount++;
                            newChild.func = part.func;
                            childPosition = nextPosition;
                        }
                    }
                }

                foreach (var splitLayoutChild in splitLayout.children)
                {
                    splitLayoutChild.ParentSplitCount = splitCount;
                }
            }

            public SplitBuilder WithSize(float size, Rule func)
            {
                parts.Add(new Parts(new List<Part>() {new Part(SizingType.NORMAL, size, func)}, false));
                return this;
            }

            public SplitBuilder WithFloatingSize(float size, Rule func)
            {
                parts.Add(new Parts(new List<Part>() {new Part(SizingType.FLOATING, size, func)}, false));
                return this;
            }


            public RepeatBuilder WithRepeat()
            {
                return new RepeatBuilder(this);
            }

            public LayoutBuilderV1 And()
            {
                return parentLayout;
            }

            public CompoundMeshDraft Build()
            {
                return parentLayout.Build();
            }


            public class RepeatBuilder
            {
                private List<Part> parts = new List<Part>();

                private SplitBuilder splitBuilder;

                public RepeatBuilder(SplitBuilder splitBuilder)
                {
                    this.splitBuilder = splitBuilder;
                    splitBuilder.parts.Add(new Parts(parts, true));
                }

                public RepeatBuilder WithSize(float size, Rule func)
                {
                    parts.Add(new Part(SizingType.NORMAL, size, func));
                    return this;
                }

                public RepeatBuilder WithFloatingSize(float size, Rule func)
                {
                    parts.Add(new Part(SizingType.FLOATING, size, func));
                    return this;
                }

                public SplitBuilder And()
                {
                    return splitBuilder;
                }

                public CompoundMeshDraft Build()
                {
                    return splitBuilder.Build();
                }
            }
        }

        public class ExtrudeBuilder
        {
            private LayoutBuilderV1 parentLayout;
            private float amount;

            public ExtrudeBuilder(float amount, LayoutBuilderV1 layoutBuilder)
            {
                this.parentLayout = layoutBuilder;
                this.amount = amount;
            }

            public ExtrudeBuilder Front(Rule func)
            {
                LayoutBuilderV1 newChild;
                if (amount > 0)
                {
                    newChild = parentLayout.AddChild(Vector3.forward * amount,
                        parentLayout.Size);
                }
                else
                {
                    newChild = parentLayout.AddChild(Vector3.right * parentLayout.Size.x,
                        parentLayout.Size);
                }

                var sign = Math.Sign(amount);
                newChild.lookRotation = Quaternion.LookRotation(Vector3.forward * sign, Vector3.up);
                newChild.func = func;
                return this;
            }

            public ExtrudeBuilder Back(Rule func)
            {
                LayoutBuilderV1 newChild;
                if (amount > 0)
                {
                    newChild = parentLayout.AddChild(Vector3.right * parentLayout.Size.x,
                        parentLayout.Size);
                }
                else
                {
                    newChild = parentLayout.AddChild(Vector3.forward * amount,
                        parentLayout.Size);
                }

                var sign = Math.Sign(amount);
                newChild.lookRotation = Quaternion.LookRotation(Vector3.back * sign);
                newChild.func = func;
                return this;
            }

            public ExtrudeBuilder All(Rule func, Directions parts = Directions.All)
            {
                if (parts.HasFlag(Directions.Forward)) Front(func);
                if (parts.HasFlag(Directions.Back)) Back(func);
                if (parts.HasFlag(Directions.Down)) Bottom(func);
                if (parts.HasFlag(Directions.Up)) Top(func);
                if (parts.HasFlag(Directions.Left)) Left(func);
                if (parts.HasFlag(Directions.Right)) Right(func);
                return this;
            }

            public ExtrudeBuilder Top(Rule func)
            {
                var newChild =
                    parentLayout.AddChild(
                        Vector3.forward * amount + Vector3.up * parentLayout.Size.y,
                        new Vector3(parentLayout.Size.x, Math.Abs(amount), 1));
                // newChild.normal = Vector3.up;
                //newChild.up = Vector3.back;

                var sign = Math.Sign(amount);
                newChild.lookRotation = Quaternion.LookRotation(Vector3.up * sign, Vector3.back * sign);
                newChild.func = func;
                return this;
            }

            public ExtrudeBuilder Bottom(Rule func)
            {
                var newChild = parentLayout.AddChild(Vector3.zero,
                    new Vector3(parentLayout.Size.x, Math.Abs(amount), 1));

                var sign = Math.Sign(amount);
                newChild.lookRotation = Quaternion.LookRotation(Vector3.down * sign, Vector3.forward * sign);
                newChild.func = func;
                return this;
            }

            public ExtrudeBuilder Right(Rule func)
            {
                var newChild = parentLayout.AddChild(Vector3.zero,
                    new Vector3(Math.Abs(amount), parentLayout.Size.y, 1));

                var sign = Math.Sign(amount);
                newChild.lookRotation = Quaternion.LookRotation(Vector3.left * sign, Vector3.up);
                newChild.func = func;
                return this;
            }

            public ExtrudeBuilder Left(Rule func)
            {
                var newChild = parentLayout.AddChild(Vector3.forward * amount + Vector3.right * parentLayout.Size.x,
                    new Vector3(Math.Abs(amount), parentLayout.Size.y, 1));

                var sign = Math.Sign(amount);
                newChild.lookRotation = Quaternion.LookRotation(Vector3.right * sign, Vector3.up);
                newChild.func = func;
                return this;
            }


            public LayoutBuilderV1 And()
            {
                return parentLayout;
            }

            public CompoundMeshDraft Build()
            {
                return parentLayout.Build();
            }
        }


        public enum Direction
        {
            NONE = 0,
            HORIZONTAL = 1,
            VERTICAL = 2
        }
    }

    public enum SizingType
    {
        NORMAL,
        FLOATING
    }

    public class Parts
    {
        public List<Part> parts;

        public bool isRepeating;

        public Parts(List<Part> parts, bool isRepeating)
        {
            this.parts = parts;
            this.isRepeating = isRepeating;
        }
    }

    public class Part
    {
        public SizingType SizingType;

        public float size;

        public Rule func;

        public Part(SizingType sizingType, float size, Rule func)
        {
            SizingType = sizingType;
            this.size = size;
            this.func = func;
        }
    }


    public static class MyExtensions
    {
        public static Func<LayoutBuilderV1, LayoutBuilderV1> Bind<T>(this Func<LayoutBuilderV1, T, LayoutBuilderV1> func, T arg)
        {
            return (p1) => func(p1, arg);
        }
    }
}