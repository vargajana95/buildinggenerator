using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine;

namespace BuildingGenerator.Common.StraightSkeleton
{
    /// <summary>
    /// A straight skeleton representation
    /// </summary>
    public class StraightSkeleton
    {
        public List<List<Vector2d>> polygons = new List<List<Vector2d>>();

        public StraightSkeleton()
        {
        }

        public StraightSkeleton(Plan plan)
        {
            foreach (var currentVertex in plan)
            {
                var nextVertex = currentVertex.next;
                var polygon = new List<Vector2d> {currentVertex.position, nextVertex.position};
                currentVertex.nextPolygonIndex = polygons.Count;
                currentVertex.previousPolygonIndex = polygons.Count - 1;
                polygons.Add(polygon);
            }
        }

        public void AddVertex(Plan.Vertex vertex)
        {
            if (vertex.previousPolygonIndex == vertex.nextPolygonIndex)
            {
                AddVertex(vertex.previousPolygonIndex, vertex.position);
            }
            else
            {
                AddVertex(vertex.previousPolygonIndex, vertex.position);
                AddVertex(vertex.nextPolygonIndex, vertex.position);
            }
        }

        public void ValidatePolygons()
        {
            foreach (var polygon in polygons)
            {
                ValidatePolygon(polygon);
            }
        }

        private void AddVertex(int polygonIndex, Vector2d vertex)
        {
            var polygon = polygons.GetLooped(polygonIndex);
            if (polygon.Count > 2)
            {
                for (int i = 2; i < polygon.Count; i++)
                {
                    if (polygon[i] == vertex)
                    {
                        return;
                    }
                }

                polygon.Add(vertex);
            }
            else
            {
                polygon.Add(vertex);
            }
        }

        private void ValidatePolygon(List<Vector2d> polygon)
        {
            Vector2d controurDirection = polygon[1] - polygon[0];
            int count = 0;
            bool swapped;
            do
            {
                swapped = false;
                if (count > polygon.Count)
                {
                    Debug.LogError("Too many iterations");
                    break;
                }
                for (int i = 3; i < polygon.Count; i++)
                {
                    Vector2d current = polygon[i];
                    Vector2d previous = polygon[i - 1];
                    Vector2d edgeDirection = current - previous;
                    double dot = Vector2d.Dot(controurDirection, edgeDirection);
                    if (dot < -Geometry.Epsilon)
                    {
                        // Contradirected
                    }
                    else if (dot > Geometry.Epsilon)
                    {
                        // Codirected
                        polygon[i] = previous;
                        polygon[i - 1] = current;
                        swapped = true;
                    }
                    else
                    {
                        // Perpendicular
                        Vector2d next = polygon.GetLooped(i + 1);
                        Vector2d previousPrevious = polygon[i - 2];
                        if (Intersect.SegmentSegment(current, next, previous, previousPrevious))
                        {
                            polygon[i] = previous;
                            polygon[i - 1] = current;
                            swapped = true;
                        }
                    }
                }
                count++;
            } while (swapped);
        }
    }
}
