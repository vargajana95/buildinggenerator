using UnityEngine;

namespace BuildingGenerator.Common.StraightSkeleton
{
    public struct IntersectionSegmentSegment2
    {
        public IntersectionType type;
        public Vector2d pointA;
        public Vector2d pointB;

        public static IntersectionSegmentSegment2 None()
        {
            return new IntersectionSegmentSegment2 {type = IntersectionType.None};
        }

        public static IntersectionSegmentSegment2 Point(Vector2d point)
        {
            return new IntersectionSegmentSegment2
            {
                type = IntersectionType.Point,
                pointA = point,
            };
        }

        public static IntersectionSegmentSegment2 Segment(Vector2d pointA, Vector2d pointB)
        {
            return new IntersectionSegmentSegment2
            {
                type = IntersectionType.Segment,
                pointA = pointA,
                pointB = pointB,
            };
        }
    }
}
