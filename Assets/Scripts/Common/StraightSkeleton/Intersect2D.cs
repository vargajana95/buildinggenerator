using System;
using UnityEngine;

namespace BuildingGenerator.Common.StraightSkeleton
{
    /// <summary>
    /// Collection of intersection algorithms
    /// </summary>
    public static partial class Intersect
    {
        #region Point-Line

        // /// <summary>
        // /// Tests if the point lies on the line
        // /// </summary>
        // public static bool PointLine(Vector2d point, Line2 line)
        // {
        //     return PointLine(point, line.origin, line.direction);
        // }
        //
        // /// <summary>
        // /// Tests if the point lies on the line
        // /// </summary>
        // /// <param name="side">
        // /// -1 if the point is to the left of the line,
        // /// 0 if it is on the line,
        // /// 1 if it is to the right of the line
        // /// </param>
        // public static bool PointLine(Vector2d point, Line2 line, out int side)
        // {
        //     return PointLine(point, line.origin, line.direction, out side);
        // }

        /// <summary>
        /// Tests if the point lies on the line
        /// </summary>
        public static bool PointLine(Vector2d point, Vector2d lineOrigin, Vector2d lineDirection)
        {
            double perpDot = Vector2dE.PerpDot(point - lineOrigin, lineDirection);
            return -Geometry.Epsilon < perpDot && perpDot < Geometry.Epsilon;
        }

        /// <summary>
        /// Tests if the point lies on the line
        /// </summary>
        /// <param name="side">
        /// -1 if the point is to the left of the line,
        /// 0 if it is on the line,
        /// 1 if it is to the right of the line
        /// </param>
        public static bool PointLine(Vector2d point, Vector2d lineOrigin, Vector2d lineDirection, out int side)
        {
            double perpDot = Vector2dE.PerpDot(point - lineOrigin, lineDirection);
            if (perpDot < -Geometry.Epsilon)
            {
                side = -1;
                return false;
            }
            if (perpDot > Geometry.Epsilon)
            {
                side = 1;
                return false;
            }
            side = 0;
            return true;
        }

        #endregion Point-Line

        #region Point-Ray

        // /// <summary>
        // /// Tests if the point lies on the ray
        // /// </summary>
        // public static bool PointRay(Vector2d point, Ray2D ray)
        // {
        //     return PointRay(point, ray.origin, ray.direction);
        // }
        //
        // /// <summary>
        // /// Tests if the point lies on the ray
        // /// </summary>
        // /// <param name="side">
        // /// -1 if the point is to the left of the ray,
        // /// 0 if it is on the line,
        // /// 1 if it is to the right of the ray
        // /// </param>
        // public static bool PointRay(Vector2d point, Ray2D ray, out int side)
        // {
        //     return PointRay(point, ray.origin, ray.direction, out side);
        // }

        /// <summary>
        /// Tests if the point lies on the ray
        /// </summary>
        public static bool PointRay(Vector2d point, Vector2d rayOrigin, Vector2d rayDirection)
        {
            Vector2d toPoint = point - rayOrigin;
            double perpDot = Vector2dE.PerpDot(toPoint, rayDirection);
            return -Geometry.Epsilon < perpDot && perpDot < Geometry.Epsilon &&
                   Vector2d.Dot(rayDirection, toPoint) > -Geometry.Epsilon;
        }

        /// <summary>
        /// Tests if the point lies on the ray
        /// </summary>
        /// <param name="side">
        /// -1 if the point is to the left of the ray,
        /// 0 if it is on the line,
        /// 1 if it is to the right of the ray
        /// </param>
        public static bool PointRay(Vector2d point, Vector2d rayOrigin, Vector2d rayDirection, out int side)
        {
            Vector2d toPoint = point - rayOrigin;
            double perpDot = Vector2dE.PerpDot(toPoint, rayDirection);
            if (perpDot < -Geometry.Epsilon)
            {
                side = -1;
                return false;
            }
            if (perpDot > Geometry.Epsilon)
            {
                side = 1;
                return false;
            }
            side = 0;
            return Vector2d.Dot(rayDirection, toPoint) > -Geometry.Epsilon;
        }

        #endregion Point-Ray

        #region Point-Segment

        // /// <summary>
        // /// Tests if the point lies on the segment
        // /// </summary>
        // public static bool PointSegment(Vector2d point, Segment2 segment)
        // {
        //     return PointSegment(point, segment.a, segment.b);
        // }
        //
        // /// <summary>
        // /// Tests if the point lies on the segment
        // /// </summary>
        // /// <param name="side">
        // /// -1 if the point is to the left of the segment,
        // /// 0 if it is on the line,
        // /// 1 if it is to the right of the segment
        // /// </param>
        // public static bool PointSegment(Vector2d point, Segment2 segment, out int side)
        // {
        //     return PointSegment(point, segment.a, segment.b, out side);
        // }

        /// <summary>
        /// Tests if the point lies on the segment
        /// </summary>
        public static bool PointSegment(Vector2d point, Vector2d segmentA, Vector2d segmentB)
        {
            Vector2d fromAToB = segmentB - segmentA;
            double sqrSegmentLength = fromAToB.sqrMagnitude;
            if (sqrSegmentLength < Geometry.Epsilon)
            {
                // The segment is a point
                return point == segmentA;
            }
            // Normalized direction gives more stable results
            Vector2d segmentDirection = fromAToB.normalized;
            Vector2d toPoint = point - segmentA;
            double perpDot = Vector2dE.PerpDot(toPoint, segmentDirection);
            if (-Geometry.Epsilon < perpDot && perpDot < Geometry.Epsilon)
            {
                double pointProjection = Vector2d.Dot(segmentDirection, toPoint);
                return pointProjection > -Geometry.Epsilon &&
                       pointProjection < Math.Sqrt(sqrSegmentLength) + Geometry.Epsilon;
            }
            return false;
        }

        /// <summary>
        /// Tests if the point lies on the segment
        /// </summary>
        /// <param name="side">
        /// -1 if the point is to the left of the segment,
        /// 0 if it is on the line,
        /// 1 if it is to the right of the segment
        /// </param>
        public static bool PointSegment(Vector2d point, Vector2d segmentA, Vector2d segmentB, out int side)
        {
            Vector2d fromAToB = segmentB - segmentA;
            double sqrSegmentLength = fromAToB.sqrMagnitude;
            if (sqrSegmentLength < Geometry.Epsilon)
            {
                // The segment is a point
                side = 0;
                return point == segmentA;
            }
            // Normalized direction gives more stable results
            Vector2d segmentDirection = fromAToB.normalized;
            Vector2d toPoint = point - segmentA;
            double perpDot = Vector2dE.PerpDot(toPoint, segmentDirection);
            if (perpDot < -Geometry.Epsilon)
            {
                side = -1;
                return false;
            }
            if (perpDot > Geometry.Epsilon)
            {
                side = 1;
                return false;
            }
            side = 0;
            double pointProjection = Vector2d.Dot(segmentDirection, toPoint);
            return pointProjection > -Geometry.Epsilon &&
                   pointProjection < Math.Sqrt(sqrSegmentLength) + Geometry.Epsilon;
        }

        private static bool PointSegment(Vector2d point, Vector2d segmentA, Vector2d segmentDirection, double sqrSegmentLength)
        {
            double segmentLength = Math.Sqrt(sqrSegmentLength);
            segmentDirection /= segmentLength;
            Vector2d toPoint = point - segmentA;
            double perpDot = Vector2dE.PerpDot(toPoint, segmentDirection);
            if (-Geometry.Epsilon < perpDot && perpDot < Geometry.Epsilon)
            {
                double pointProjection = Vector2d.Dot(segmentDirection, toPoint);
                return pointProjection > -Geometry.Epsilon &&
                       pointProjection < segmentLength + Geometry.Epsilon;
            }
            return false;
        }

        // public static bool PointSegmentCollinear(Vector2d segmentA, Vector2d segmentB, Vector2d point)
        // {
        //     if (Math.Abs(segmentA.x - segmentB.x) < Geometry.Epsilon)
        //     {
        //         // Vertical
        //         if (segmentA.y <= point.y && point.y <= segmentB.y)
        //         {
        //             return true;
        //         }
        //         if (segmentA.y >= point.y && point.y >= segmentB.y)
        //         {
        //             return true;
        //         }
        //     }
        //     else
        //     {
        //         // Not vertical
        //         if (segmentA.x <= point.x && point.x <= segmentB.x)
        //         {
        //             return true;
        //         }
        //         if (segmentA.x >= point.x && point.x >= segmentB.x)
        //         {
        //             return true;
        //         }
        //     }
        //     return false;
        // }

        #endregion Point-Segment

        #region Line-Line

        // /// <summary>
        // /// Computes an intersection of the lines
        // /// </summary>
        // public static bool LineLine(Line2 lineA, Line2 lineB)
        // {
        //     return LineLine(lineA.origin, lineA.direction, lineB.origin, lineB.direction, out IntersectionLineLine2 intersection);
        // }
        //
        // /// <summary>
        // /// Computes an intersection of the lines
        // /// </summary>
        // public static bool LineLine(Line2 lineA, Line2 lineB, out IntersectionLineLine2 intersection)
        // {
        //     return LineLine(lineA.origin, lineA.direction, lineB.origin, lineB.direction, out intersection);
        // }

        // /// <summary>
        // /// Computes an intersection of the lines
        // /// </summary>
        // public static bool LineLine(Vector2d originA, Vector2d directionA, Vector2d originB, Vector2d directionB)
        // {
        //     return LineLine(originA, directionA, originB, directionB, out IntersectionLineLine2 intersection);
        // }

        // /// <summary>
        // /// Computes an intersection of the lines
        // /// </summary>
        // public static bool LineLine(Vector2d originA, Vector2d directionA, Vector2d originB, Vector2d directionB,
        //     out IntersectionLineLine2 intersection)
        // {
        //     Vector2d originBToA = originA - originB;
        //     double denominator = Vector2dE.PerpDot(directionA, directionB);
        //     double perpDotB = Vector2dE.PerpDot(directionB, originBToA);
        //
        //     if (Math.Abs(denominator) < Geometry.Epsilon)
        //     {
        //         // Parallel
        //         double perpDotA = Vector2dE.PerpDot(directionA, originBToA);
        //         if (Math.Abs(perpDotA) > Geometry.Epsilon || Math.Abs(perpDotB) > Geometry.Epsilon)
        //         {
        //             // Not collinear
        //             intersection = IntersectionLineLine2.None();
        //             return false;
        //         }
        //         // Collinear
        //         intersection = IntersectionLineLine2.Line(originA);
        //         return true;
        //     }
        //
        //     // Not parallel
        //     intersection = IntersectionLineLine2.Point(originA + directionA*(perpDotB/denominator));
        //     return true;
        // }

        #endregion Line-Line

        #region Line-Ray

        // /// <summary>
        // /// Computes an intersection of the line and the ray
        // /// </summary>
        // public static bool LineRay(Line2 line, Ray2D ray)
        // {
        //     return LineRay(line.origin, line.direction, ray.origin, ray.direction, out IntersectionLineRay2 intersection);
        // }
        //
        // /// <summary>
        // /// Computes an intersection of the line and the ray
        // /// </summary>
        // public static bool LineRay(Line2 line, Ray2D ray, out IntersectionLineRay2 intersection)
        // {
        //     return LineRay(line.origin, line.direction, ray.origin, ray.direction, out intersection);
        // }

        // /// <summary>
        // /// Computes an intersection of the line and the ray
        // /// </summary>
        // public static bool LineRay(Vector2d lineOrigin, Vector2d lineDirection, Vector2d rayOrigin, Vector2d rayDirection)
        // {
        //     return LineRay(lineOrigin, lineDirection, rayOrigin, rayDirection, out IntersectionLineRay2 intersection);
        // }
        //
        // /// <summary>
        // /// Computes an intersection of the line and the ray
        // /// </summary>
        // public static bool LineRay(Vector2d lineOrigin, Vector2d lineDirection, Vector2d rayOrigin, Vector2d rayDirection,
        //     out IntersectionLineRay2 intersection)
        // {
        //     Vector2d rayOriginToLineOrigin = lineOrigin - rayOrigin;
        //     double denominator = Vector2dE.PerpDot(lineDirection, rayDirection);
        //     double perpDotA = Vector2dE.PerpDot(lineDirection, rayOriginToLineOrigin);
        //
        //     if (Math.Abs(denominator) < Geometry.Epsilon)
        //     {
        //         // Parallel
        //         double perpDotB = Vector2dE.PerpDot(rayDirection, rayOriginToLineOrigin);
        //         if (Math.Abs(perpDotA) > Geometry.Epsilon || Math.Abs(perpDotB) > Geometry.Epsilon)
        //         {
        //             // Not collinear
        //             intersection = IntersectionLineRay2.None();
        //             return false;
        //         }
        //         // Collinear
        //         intersection = IntersectionLineRay2.Ray(rayOrigin);
        //         return true;
        //     }
        //
        //     // Not parallel
        //     double rayDistance = perpDotA/denominator;
        //     if (rayDistance > -Geometry.Epsilon)
        //     {
        //         intersection = IntersectionLineRay2.Point(rayOrigin + rayDirection*rayDistance);
        //         return true;
        //     }
        //     intersection = IntersectionLineRay2.None();
        //     return false;
        // }

        #endregion Line-Ray

        #region Line-Segment

        // /// <summary>
        // /// Computes an intersection of the line and the segment
        // /// </summary>
        // public static bool LineSegment(Line2 line, Segment2 segment)
        // {
        //     return LineSegment(line.origin, line.direction, segment.a, segment.b, out IntersectionLineSegment2 intersection);
        // }
        //
        // /// <summary>
        // /// Computes an intersection of the line and the segment
        // /// </summary>
        // public static bool LineSegment(Line2 line, Segment2 segment, out IntersectionLineSegment2 intersection)
        // {
        //     return LineSegment(line.origin, line.direction, segment.a, segment.b, out intersection);
        // }
        //
        // /// <summary>
        // /// Computes an intersection of the line and the segment
        // /// </summary>
        // public static bool LineSegment(Vector2d lineOrigin, Vector2d lineDirection, Vector2d segmentA, Vector2d segmentB)
        // {
        //     return LineSegment(lineOrigin, lineDirection, segmentA, segmentB, out IntersectionLineSegment2 intersection);
        // }

        // /// <summary>
        // /// Computes an intersection of the line and the segment
        // /// </summary>
        // public static bool LineSegment(Vector2d lineOrigin, Vector2d lineDirection, Vector2d segmentA, Vector2d segmentB,
        //     out IntersectionLineSegment2 intersection)
        // {
        //     Vector2d segmentAToOrigin = lineOrigin - segmentA;
        //     Vector2d segmentDirection = segmentB - segmentA;
        //     double denominator = Vector2dE.PerpDot(lineDirection, segmentDirection);
        //     double perpDotA = Vector2dE.PerpDot(lineDirection, segmentAToOrigin);
        //
        //     if (Math.Abs(denominator) < Geometry.Epsilon)
        //     {
        //         // Parallel
        //         // Normalized direction gives more stable results 
        //         double perpDotB = Vector2dE.PerpDot(segmentDirection.normalized, segmentAToOrigin);
        //         if (Math.Abs(perpDotA) > Geometry.Epsilon || Math.Abs(perpDotB) > Geometry.Epsilon)
        //         {
        //             // Not collinear
        //             intersection = IntersectionLineSegment2.None();
        //             return false;
        //         }
        //         // Collinear
        //         bool segmentIsAPoint = segmentDirection.sqrMagnitude < Geometry.Epsilon;
        //         if (segmentIsAPoint)
        //         {
        //             intersection = IntersectionLineSegment2.Point(segmentA);
        //             return true;
        //         }
        //
        //         bool codirected = Vector2d.Dot(lineDirection, segmentDirection) > 0;
        //         if (codirected)
        //         {
        //             intersection = IntersectionLineSegment2.Segment(segmentA, segmentB);
        //         }
        //         else
        //         {
        //             intersection = IntersectionLineSegment2.Segment(segmentB, segmentA);
        //         }
        //         return true;
        //     }
        //
        //     // Not parallel
        //     double segmentDistance = perpDotA/denominator;
        //     if (segmentDistance > -Geometry.Epsilon && segmentDistance < 1 + Geometry.Epsilon)
        //     {
        //         intersection = IntersectionLineSegment2.Point(segmentA + segmentDirection*segmentDistance);
        //         return true;
        //     }
        //     intersection = IntersectionLineSegment2.None();
        //     return false;
        // }

        #endregion Line-Segment


        #region Ray-Ray

        // /// <summary>
        // /// Computes an intersection of the rays
        // /// </summary>
        // public static bool RayRay(Ray2D rayA, Ray2D rayB)
        // {
        //     return RayRay(rayA.origin, rayA.direction, rayB.origin, rayB.direction, out IntersectionRayRay2 intersection);
        // }
        //
        // /// <summary>
        // /// Computes an intersection of the rays
        // /// </summary>
        // public static bool RayRay(Ray2D rayA, Ray2D rayB, out IntersectionRayRay2 intersection)
        // {
        //     return RayRay(rayA.origin, rayA.direction, rayB.origin, rayB.direction, out intersection);
        // }
        //
        // /// <summary>
        // /// Computes an intersection of the rays
        // /// </summary>
        // public static bool RayRay(Vector2d originA, Vector2d directionA, Vector2d originB, Vector2d directionB)
        // {
        //     return RayRay(originA, directionA, originB, directionB, out IntersectionRayRay2 intersection);
        // }

        /// <summary>
        /// Computes an intersection of the rays
        /// </summary>
        public static bool RayRay(Vector2d originA, Vector2d directionA, Vector2d originB, Vector2d directionB,
            out IntersectionRayRay2 intersection)
        {
            Vector2d originBToA = originA - originB;
            double denominator = Vector2dE.PerpDot(directionA, directionB);
            double perpDotA = Vector2dE.PerpDot(directionA, originBToA);
            double perpDotB = Vector2dE.PerpDot(directionB, originBToA);

            if (Math.Abs(denominator) < Geometry.Epsilon)
            {
                // Parallel
                if (Math.Abs(perpDotA) > Geometry.Epsilon || Math.Abs(perpDotB) > Geometry.Epsilon)
                {
                    // Not collinear
                    intersection = IntersectionRayRay2.None();
                    return false;
                }
                // Collinear

                bool codirected = Vector2d.Dot(directionA, directionB) > 0;
                double originBProjection = -Vector2d.Dot(directionA, originBToA);
                if (codirected)
                {
                    intersection = IntersectionRayRay2.Ray(originBProjection > 0 ? originB : originA, directionA);
                    return true;
                }
                else
                {
                    if (originBProjection < -Geometry.Epsilon)
                    {
                        intersection = IntersectionRayRay2.None();
                        return false;
                    }
                    if (originBProjection < Geometry.Epsilon)
                    {
                        intersection = IntersectionRayRay2.Point(originA);
                        return true;
                    }
                    intersection = IntersectionRayRay2.Segment(originA, originB);
                    return true;
                }
            }

            // Not parallel
            double distanceA = perpDotB/denominator;
            if (distanceA < -Geometry.Epsilon)
            {
                intersection = IntersectionRayRay2.None();
                return false;
            }

            double distanceB = perpDotA/denominator;
            if (distanceB < -Geometry.Epsilon)
            {
                intersection = IntersectionRayRay2.None();
                return false;
            }

            intersection = IntersectionRayRay2.Point(originA + directionA*distanceA);
            return true;
        }

        #endregion Ray-Ray

        #region Ray-Segment

        // /// <summary>
        // /// Computes an intersection of the ray and the segment
        // /// </summary>
        // public static bool RaySegment(Ray2D ray, Segment2 segment)
        // {
        //     return RaySegment(ray.origin, ray.direction, segment.a, segment.b, out IntersectionRaySegment2 intersection);
        // }
        //
        // /// <summary>
        // /// Computes an intersection of the ray and the segment
        // /// </summary>
        // public static bool RaySegment(Ray2D ray, Segment2 segment, out IntersectionRaySegment2 intersection)
        // {
        //     return RaySegment(ray.origin, ray.direction, segment.a, segment.b, out intersection);
        // }

        /// <summary>
        /// Computes an intersection of the ray and the segment
        /// </summary>
        public static bool RaySegment(Vector2d rayOrigin, Vector2d rayDirection, Vector2d segmentA, Vector2d segmentB)
        {
            return RaySegment(rayOrigin, rayDirection, segmentA, segmentB, out IntersectionRaySegment2 intersection);
        }

        /// <summary>
        /// Computes an intersection of the ray and the segment
        /// </summary>
        public static bool RaySegment(Vector2d rayOrigin, Vector2d rayDirection, Vector2d segmentA, Vector2d segmentB,
            out IntersectionRaySegment2 intersection)
        {
            Vector2d segmentAToOrigin = rayOrigin - segmentA;
            Vector2d segmentDirection = segmentB - segmentA;
            double denominator = Vector2dE.PerpDot(rayDirection, segmentDirection);
            double perpDotA = Vector2dE.PerpDot(rayDirection, segmentAToOrigin);
            // Normalized direction gives more stable results 
            double perpDotB = Vector2dE.PerpDot(segmentDirection.normalized, segmentAToOrigin);

            if (Math.Abs(denominator) < Geometry.Epsilon)
            {
                // Parallel
                if (Math.Abs(perpDotA) > Geometry.Epsilon || Math.Abs(perpDotB) > Geometry.Epsilon)
                {
                    // Not collinear
                    intersection = IntersectionRaySegment2.None();
                    return false;
                }
                // Collinear

                bool segmentIsAPoint = segmentDirection.sqrMagnitude < Geometry.Epsilon;
                double segmentAProjection = Vector2d.Dot(rayDirection, segmentA - rayOrigin);
                if (segmentIsAPoint)
                {
                    if (segmentAProjection > -Geometry.Epsilon)
                    {
                        intersection = IntersectionRaySegment2.Point(segmentA);
                        return true;
                    }
                    intersection = IntersectionRaySegment2.None();
                    return false;
                }

                double segmentBProjection = Vector2d.Dot(rayDirection, segmentB - rayOrigin);
                if (segmentAProjection > -Geometry.Epsilon)
                {
                    if (segmentBProjection > -Geometry.Epsilon)
                    {
                        if (segmentBProjection > segmentAProjection)
                        {
                            intersection = IntersectionRaySegment2.Segment(segmentA, segmentB);
                        }
                        else
                        {
                            intersection = IntersectionRaySegment2.Segment(segmentB, segmentA);
                        }
                    }
                    else
                    {
                        if (segmentAProjection > Geometry.Epsilon)
                        {
                            intersection = IntersectionRaySegment2.Segment(rayOrigin, segmentA);
                        }
                        else
                        {
                            intersection = IntersectionRaySegment2.Point(rayOrigin);
                        }
                    }
                    return true;
                }
                if (segmentBProjection > -Geometry.Epsilon)
                {
                    if (segmentBProjection > Geometry.Epsilon)
                    {
                        intersection = IntersectionRaySegment2.Segment(rayOrigin, segmentB);
                    }
                    else
                    {
                        intersection = IntersectionRaySegment2.Point(rayOrigin);
                    }
                    return true;
                }
                intersection = IntersectionRaySegment2.None();
                return false;
            }

            // Not parallel
            double rayDistance = perpDotB/denominator;
            double segmentDistance = perpDotA/denominator;
            if (rayDistance > -Geometry.Epsilon &&
                segmentDistance > -Geometry.Epsilon && segmentDistance < 1 + Geometry.Epsilon)
            {
                intersection = IntersectionRaySegment2.Point(segmentA + segmentDirection*segmentDistance);
                return true;
            }
            intersection = IntersectionRaySegment2.None();
            return false;
        }

        #endregion Ray-Segment


        #region Segment-Segment

        // /// <summary>
        // /// Computes an intersection of the segments
        // /// </summary>
        // public static bool SegmentSegment(Segment2 segment1, Segment2 segment2)
        // {
        //     return SegmentSegment(segment1.a, segment1.b, segment2.a, segment2.b, out IntersectionSegmentSegment2 intersection);
        // }
        //
        // /// <summary>
        // /// Computes an intersection of the segments
        // /// </summary>
        // public static bool SegmentSegment(Segment2 segment1, Segment2 segment2, out IntersectionSegmentSegment2 intersection)
        // {
        //     return SegmentSegment(segment1.a, segment1.b, segment2.a, segment2.b, out intersection);
        // }

        /// <summary>
        /// Computes an intersection of the segments
        /// </summary>
        public static bool SegmentSegment(Vector2d segment1A, Vector2d segment1B, Vector2d segment2A, Vector2d segment2B)
        {
            return SegmentSegment(segment1A, segment1B, segment2A, segment2B, out IntersectionSegmentSegment2 intersection);
        }

        /// <summary>
        /// Computes an intersection of the segments
        /// </summary>
        public static bool SegmentSegment(Vector2d segment1A, Vector2d segment1B, Vector2d segment2A, Vector2d segment2B,
            out IntersectionSegmentSegment2 intersection)
        {
            Vector2d from2ATo1A = segment1A - segment2A;
            Vector2d direction1 = segment1B - segment1A;
            Vector2d direction2 = segment2B - segment2A;

            double sqrSegment1Length = direction1.sqrMagnitude;
            double sqrSegment2Length = direction2.sqrMagnitude;
            bool segment1IsAPoint = sqrSegment1Length < Geometry.Epsilon;
            bool segment2IsAPoint = sqrSegment2Length < Geometry.Epsilon;
            if (segment1IsAPoint && segment2IsAPoint)
            {
                if (segment1A == segment2A)
                {
                    intersection = IntersectionSegmentSegment2.Point(segment1A);
                    return true;
                }
                intersection = IntersectionSegmentSegment2.None();
                return false;
            }
            if (segment1IsAPoint)
            {
                if (PointSegment(segment1A, segment2A, direction2, sqrSegment2Length))
                {
                    intersection = IntersectionSegmentSegment2.Point(segment1A);
                    return true;
                }
                intersection = IntersectionSegmentSegment2.None();
                return false;
            }
            if (segment2IsAPoint)
            {
                if (PointSegment(segment2A, segment1A, direction1, sqrSegment1Length))
                {
                    intersection = IntersectionSegmentSegment2.Point(segment2A);
                    return true;
                }
                intersection = IntersectionSegmentSegment2.None();
                return false;
            }

            double denominator = Vector2dE.PerpDot(direction1, direction2);
            double perpDot1 = Vector2dE.PerpDot(direction1, from2ATo1A);
            double perpDot2 = Vector2dE.PerpDot(direction2, from2ATo1A);

            if (Math.Abs(denominator) < Geometry.Epsilon)
            {
                // Parallel
                if (Math.Abs(perpDot1) > Geometry.Epsilon || Math.Abs(perpDot2) > Geometry.Epsilon)
                {
                    // Not collinear
                    intersection = IntersectionSegmentSegment2.None();
                    return false;
                }
                // Collinear

                bool codirected = Vector2d.Dot(direction1, direction2) > 0;
                if (codirected)
                {
                    // Codirected
                    double segment2AProjection = -Vector2d.Dot(direction1, from2ATo1A);
                    if (segment2AProjection > -Geometry.Epsilon)
                    {
                        // 1A------1B
                        //     2A------2B
                        return SegmentSegmentCollinear(segment1A, segment1B, sqrSegment1Length, segment2A, segment2B, out intersection);
                    }
                    else
                    {
                        //     1A------1B
                        // 2A------2B
                        return SegmentSegmentCollinear(segment2A, segment2B, sqrSegment2Length, segment1A, segment1B, out intersection);
                    }
                }
                else
                {
                    // Contradirected
                    double segment2BProjection = Vector2d.Dot(direction1, segment2B - segment1A);
                    if (segment2BProjection > -Geometry.Epsilon)
                    {
                        // 1A------1B
                        //     2B------2A
                        return SegmentSegmentCollinear(segment1A, segment1B, sqrSegment1Length, segment2B, segment2A, out intersection);
                    }
                    else
                    {
                        //     1A------1B
                        // 2B------2A
                        return SegmentSegmentCollinear(segment2B, segment2A, sqrSegment2Length, segment1A, segment1B, out intersection);
                    }
                }
            }

            // Not parallel
            double distance1 = perpDot2/denominator;
            if (distance1 < -Geometry.Epsilon || distance1 > 1 + Geometry.Epsilon)
            {
                intersection = IntersectionSegmentSegment2.None();
                return false;
            }

            double distance2 = perpDot1/denominator;
            if (distance2 < -Geometry.Epsilon || distance2 > 1 + Geometry.Epsilon)
            {
                intersection = IntersectionSegmentSegment2.None();
                return false;
            }

            intersection = IntersectionSegmentSegment2.Point(segment1A + direction1*distance1);
            return true;
        }

        private static bool SegmentSegmentCollinear(Vector2d leftA, Vector2d leftB, double sqrLeftLength, Vector2d rightA, Vector2d rightB,
            out IntersectionSegmentSegment2 intersection)
        {
            Vector2d leftDirection = leftB - leftA;
            double rightAProjection = Vector2d.Dot(leftDirection, rightA - leftB);
            if (Math.Abs(rightAProjection) < Geometry.Epsilon)
            {
                // LB == RA
                // LA------LB
                //         RA------RB
                intersection = IntersectionSegmentSegment2.Point(leftB);
                return true;
            }
            if (rightAProjection < 0)
            {
                // LB > RA
                // LA------LB
                //     RARB
                //     RA--RB
                //     RA------RB
                Vector2d pointB;
                double rightBProjection = Vector2d.Dot(leftDirection, rightB - leftA);
                if (rightBProjection > sqrLeftLength)
                {
                    pointB = leftB;
                }
                else
                {
                    pointB = rightB;
                }
                intersection = IntersectionSegmentSegment2.Segment(rightA, pointB);
                return true;
            }
            // LB < RA
            // LA------LB
            //             RA------RB
            intersection = IntersectionSegmentSegment2.None();
            return false;
        }

        #endregion Segment-Segment


    }
}
