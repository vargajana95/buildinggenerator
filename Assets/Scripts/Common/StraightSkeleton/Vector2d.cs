﻿using System;
using UnityEngine;

namespace BuildingGenerator.Common.StraightSkeleton
{
    /// <summary> Represents point or vector in 2D space. </summary>
    public struct Vector2d
    {
        public static Vector2d Empty = new Vector2d(double.MinValue, double.MinValue);

        public double X;
        public double Y;

        public Vector2d(Vector2d var1)
        {
            X = var1.X;
            Y = var1.Y;
        }

        public Vector2d(double var1, double var3)
        {
            X = var1;
            Y = var3;
        }

        public Vector2d normalized => Normalized();
        public double sqrMagnitude => SqrMagnitude();
        public double magnitude => Magnitude();

        public void Negate()
        {
            X = -X;
            Y = -Y;
        }

        public double DistanceTo(Vector2d var1)
        {
            var var2 = X - var1.X;
            var var4 = Y - var1.Y;
            return Math.Sqrt(var2 * var2 + var4 * var4);
        }
        
        public double Magnitude()
        {
            return Math.Sqrt(X * X + Y * Y);
        }

        public double SqrMagnitude()
        {
            return X * X + Y * Y;
        }

        public Vector2d Normalized()
        {
            var var1 = 1.0D/Math.Sqrt(X*X + Y*Y);
            return new Vector2d(X * var1, Y * var1);
        }

        public double Dot(Vector2d var1)
        {
            return X*var1.X + Y*var1.Y;
        }

        public double DistanceSquared(Vector2d var1)
        {
            var var2 = X - var1.X;
            var var4 = Y - var1.Y;
            return var2*var2 + var4*var4;
        }

        public bool Approximately(Vector2d v)
        {
            return Math.Abs((this - v).Magnitude()) < Geometry.Epsilon;
        }

        public static Vector2d operator -(Vector2d left, Vector2d right)
        {
            return new Vector2d(left.X - right.X, left.Y - right.Y);
        }

        public static Vector2d operator +(Vector2d left, Vector2d right)
        {
            return new Vector2d(left.X + right.X, left.Y + right.Y);
        }

        public static Vector2d operator *(Vector2d left, double scale)
        {
            return new Vector2d(left.X * scale, left.Y * scale);
        }
        
        public static Vector2d operator /(Vector2d left, double scale)
        {
            return new Vector2d(left.X / scale, left.Y / scale);
        }

        public static bool operator ==(Vector2d left, Vector2d right)
        {
            // return left.X == right.X && left.Y == right.Y;
            return left.Approximately(right);
        }

        public static bool operator !=(Vector2d left, Vector2d right)
        {
            return !(left == right);
        }

        public bool Equals(Vector2d obj)
        {
            return X.Equals(obj.X) && Y.Equals(obj.Y);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Vector2d && Equals((Vector2d)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode()*397) ^ Y.GetHashCode();
            }
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", X, Y);
        }
        
        public static double Angle(Vector2d from, Vector2d to)
        {
            double num = Math.Sqrt(from.SqrMagnitude() * to.SqrMagnitude());
            return num < 1.00000000362749E-15 ? 0.0 : Math.Acos((double) Clamp(from.Dot( to) / num, -1.0, 1.0)) * (180 / Math.PI);
        }
        
        private static double Clamp(double value, double min, double max)
        {
            if (value <  min)
                value = min;
            else if ( value > max)
                value = max;
            return value;
        }

        public static double Dot(Vector2d v1, Vector2d v2)
        {
            return v1.Dot(v2);
        }

        public static double Distance(Vector2d vertexAPosition, Vector2d vertexBPosition)
        {
            return vertexAPosition.DistanceTo(vertexBPosition);
        }
    }

    public static class Vector2dE
    {
        public static double Angle360(Vector2d from, Vector2d to)
        {
            double angle = SignedAngle(from, to);
            while (angle < 0)
            {
                angle += 360;
            }
            return angle;
        }

        /// <summary>
        /// Returns a new vector rotated clockwise by the specified angle
        /// </summary>
        public static Vector2d RotateCW(this Vector2d vector, double degrees)
        {
            double radians = degrees*(Math.PI / 180);
            double sin = Math.Sin(radians);
            double cos = Math.Cos(radians);
            return new Vector2d(
                vector.X*cos + vector.Y*sin,
                vector.Y*cos - vector.X*sin);
        }
        /// <summary>
        /// Returns a signed clockwise angle in degrees [-180, 180] between from and to
        /// </summary>
        /// <param name="from">The angle extends round from this vector</param>
        /// <param name="to">The angle extends round to this vector</param>
        public static double SignedAngle(Vector2d from, Vector2d to)
        {
            return Math.Atan2(PerpDot(to, from), to.Dot(from))*( 180 / Math.PI);
        }
        
        /// <summary>
        /// Returns a perp dot product of vectors
        /// </summary>
        /// <remarks>
        /// Hill, F. S. Jr. "The Pleasures of 'Perp Dot' Products."
        /// Ch. II.5 in Graphics Gems IV (Ed. P. S. Heckbert). San Diego: Academic Press, pp. 138-148, 1994
        /// </remarks>
        public static double PerpDot(Vector2d a, Vector2d b)
        {
            return a.X*b.Y - a.Y*b.X;
        }
    }
}