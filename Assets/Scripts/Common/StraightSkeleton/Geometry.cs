using System;
using UnityEngine;
using System.Collections.Generic;
using ProceduralToolkit;
using UnityEngine.Assertions;

namespace BuildingGenerator.Common.StraightSkeleton
{
    /// <summary>
    /// Utility class for computational geometry algorithms
    /// </summary>
    public static class Geometry
    {
        /// <summary>
        /// A tiny doubleing point value used in comparisons
        /// </summary>
        public const double Epsilon = 1E-7;

        #region Point samplers 2D

        // /// <summary>
        // /// Returns a point on a segment at the given normalized position
        // /// </summary>
        // /// <param name="segmentA">Start of the segment</param>
        // /// <param name="segmentB">End of the segment</param>
        // /// <param name="position">Normalized position</param>
        // public static Vector2d PointOnSegment2(Vector2d segmentA, Vector2d segmentB, double position)
        // {
        //     return Vector2d.Lerp(segmentA, segmentB, position);
        // }

        // /// <summary>
        // /// Returns a list of evenly distributed points on a segment
        // /// </summary>
        // /// <param name="segmentA">Start of the segment</param>
        // /// <param name="segmentB">End of the segment</param>
        // /// <param name="count">Number of points</param>
        // public static List<Vector2d> PointsOnSegment2(Vector2d segmentA, Vector2d segmentB, int count)
        // {
        //     var points = new List<Vector2d>(count);
        //     if (count <= 0)
        //     {
        //         return points;
        //     }
        //     if (count == 1)
        //     {
        //         points.Add(segmentA);
        //         return points;
        //     }
        //     for (int i = 0; i < count; i++)
        //     {
        //         points.Add(PointOnSegment2(segmentA, segmentB, i/(double) (count - 1)));
        //     }
        //     return points;
        // }

        #region PointOnCircle2

        /// <summary>
        /// Returns a point on a circle in the XY plane
        /// </summary>
        /// <param name="radius">Circle radius</param>
        /// <param name="angle">Angle in degrees</param>
        public static Vector2d PointOnCircle2(double radius, double angle)
        {
            double angleInRadians = angle*(Math.PI / 180);
            return new Vector2d(radius*Math.Sin(angleInRadians), radius*Math.Cos(angleInRadians));
        }

        /// <summary>
        /// Returns a point on a circle in the XY plane
        /// </summary>
        /// <param name="center">Center of the circle</param>
        /// <param name="radius">Circle radius</param>
        /// <param name="angle">Angle in degrees</param>
        public static Vector2d PointOnCircle2(Vector2d center, double radius, double angle)
        {
            return center + PointOnCircle2(radius, angle);
        }

        #endregion PointOnCircle2

        #region PointsOnCircle2

        /// <summary>
        /// Returns a list of evenly distributed points on a circle in the XY plane
        /// </summary>
        /// <param name="radius">Circle radius</param>
        /// <param name="count">Number of points</param>
        public static List<Vector2d> PointsOnCircle2(double radius, int count)
        {
            double segmentAngle = 360f/count;
            double currentAngle = 0;
            var points = new List<Vector2d>(count);
            for (var i = 0; i < count; i++)
            {
                points.Add(PointOnCircle2(radius, currentAngle));
                currentAngle += segmentAngle;
            }
            return points;
        }

        /// <summary>
        /// Returns a list of evenly distributed points on a circle in the XY plane
        /// </summary>
        /// <param name="center">Center of the circle</param>
        /// <param name="radius">Circle radius</param>
        /// <param name="count">Number of points</param>
        public static List<Vector2d> PointsOnCircle2(Vector2d center, double radius, int count)
        {
            double segmentAngle = 360f/count;
            double currentAngle = 0;
            var points = new List<Vector2d>(count);
            for (var i = 0; i < count; i++)
            {
                points.Add(PointOnCircle2(center, radius, currentAngle));
                currentAngle += segmentAngle;
            }
            return points;
        }

        #endregion PointsOnCircle2

        #region PointsInCircle2

        /// <summary>
        /// Returns a list of evenly distributed points inside a circle in the XY plane
        /// </summary>
        /// <param name="radius">Circle radius</param>
        /// <param name="count">Number of points</param>
        public static List<Vector2d> PointsInCircle2(double radius, int count)
        {
            double currentAngle = 0;
            var points = new List<Vector2d>(count);
            for (int i = 0; i < count; i++)
            {
                // The 0.5 offset improves the position of the first point
                double r = Math.Sqrt((i + 0.5f)/count);
                points.Add(new Vector2d(radius*Math.Sin(currentAngle)*r, radius*Math.Cos(currentAngle)*r));
                currentAngle += PTUtils.GoldenAngle;
            }
            return points;
        }

        /// <summary>
        /// Returns a list of evenly distributed points inside a circle in the XY plane
        /// </summary>
        /// <param name="center">Center of the circle</param>
        /// <param name="radius">Circle radius</param>
        /// <param name="count">Number of points</param>
        public static List<Vector2d> PointsInCircle2(Vector2d center, double radius, int count)
        {
            double currentAngle = 0;
            var points = new List<Vector2d>(count);
            for (int i = 0; i < count; i++)
            {
                // The 0.5 offset improves the position of the first point
                double r = Math.Sqrt((i + 0.5f)/count);
                points.Add(center + new Vector2d(radius*Math.Sin(currentAngle)*r, radius*Math.Cos(currentAngle)*r));
                currentAngle += PTUtils.GoldenAngle;
            }
            return points;
        }

        #endregion PointsInCircle2

        #endregion Point samplers 2D

        /// <summary>
        /// Returns a list of points representing a polygon in the XY plane
        /// </summary>
        /// <param name="radius">Radius of the circle passing through the vertices</param>
        /// <param name="vertices">Number of polygon vertices</param>
        public static List<Vector2d> Polygon2(int vertices, double radius)
        {
            return PointsOnCircle2(radius, vertices);
        }

        /// <summary>
        /// Returns a list of points representing a star polygon in the XY plane
        /// </summary>
        /// <param name="innerRadius">Radius of the circle passing through the outer vertices</param>
        /// <param name="outerRadius">Radius of the circle passing through the inner vertices</param>
        /// <param name="vertices">Number of polygon vertices</param>
        public static List<Vector2d> StarPolygon2(int vertices, double innerRadius, double outerRadius)
        {
            double segmentAngle = 360f/vertices;
            double halfSegmentAngle = segmentAngle/2;
            double currentAngle = 0;
            var polygon = new List<Vector2d>(vertices);
            for (var i = 0; i < vertices; i++)
            {
                polygon.Add(PointOnCircle2(outerRadius, currentAngle));
                polygon.Add(PointOnCircle2(innerRadius, currentAngle + halfSegmentAngle));
                currentAngle += segmentAngle;
            }
            return polygon;
        }

        /// <summary>
        /// Returns the value of an angle. Assumes clockwise order of the polygon.
        /// </summary>
        /// <param name="previous">Previous vertex</param>
        /// <param name="current">Current vertex</param>
        /// <param name="next">Next vertex</param>
        public static double GetAngle(Vector2d previous, Vector2d current, Vector2d next)
        {
            Vector2d toPrevious = (previous - current).normalized;
            Vector2d toNext = (next - current).normalized;
            return Vector2dE.Angle360(toNext, toPrevious);
        }

        /// <summary>
        /// Returns the bisector of an angle. Assumes clockwise order of the polygon.
        /// </summary>
        /// <param name="previous">Previous vertex</param>
        /// <param name="current">Current vertex</param>
        /// <param name="next">Next vertex</param>
        /// <param name="degrees">Value of the angle in degrees. Always positive.</param>
        public static Vector2d GetAngleBisector(Vector2d previous, Vector2d current, Vector2d next, out double degrees)
        {
            Vector2d toPrevious = (previous - current).normalized;
            Vector2d toNext = (next - current).normalized;

            degrees = Vector2dE.Angle360(toNext, toPrevious);
            Assert.IsFalse(double.IsNaN(degrees));
            return toNext.RotateCW(degrees/2);
        }

        /// <summary>
        /// Creates a new offset polygon from the input polygon. Assumes clockwise order of the polygon.
        /// Does not handle intersections.
        /// </summary>
        /// <param name="polygon">Vertices of the polygon in clockwise order.</param>
        /// <param name="distance">Offset distance. Positive values offset outside, negative inside.</param>
        public static List<Vector2d> OffsetPolygon(IList<Vector2d> polygon, double distance)
        {
            var newPolygon = new List<Vector2d>(polygon.Count);
            for (int i = 0; i < polygon.Count; i++)
            {
                Vector2d previous = polygon.GetLooped(i - 1);
                Vector2d current = polygon[i];
                Vector2d next = polygon.GetLooped(i + 1);

                Vector2d bisector = GetAngleBisector(previous, current, next, out double angle);
                double angleOffset = GetAngleOffset(distance, angle);
                newPolygon.Add(current - bisector*angleOffset);
            }
            return newPolygon;
        }

        // /// <summary>
        // /// Offsets the input polygon. Assumes clockwise order of the polygon.
        // /// Does not handle intersections.
        // /// </summary>
        // /// <param name="polygon">Vertices of the polygon in clockwise order.</param>
        // /// <param name="distance">Offset distance. Positive values offset outside, negative inside.</param>
        // public static void OffsetPolygon(ref List<Vector2d> polygon, double distance)
        // {
        //     var offsets = new Vector2d[polygon.Count];
        //     for (int i = 0; i < polygon.Count; i++)
        //     {
        //         Vector2d previous = polygon.GetLooped(i - 1);
        //         Vector2d current = polygon[i];
        //         Vector2d next = polygon.GetLooped(i + 1);
        //
        //         Vector2d bisector = GetAngleBisector(previous, current, next, out double angle);
        //         double angleOffset = GetAngleOffset(distance, angle);
        //         offsets[i] = -bisector*angleOffset;
        //     }
        //
        //     for (int i = 0; i < polygon.Count; i++)
        //     {
        //         polygon[i] += offsets[i];
        //     }
        // }

        // /// <summary>
        // /// Offsets the input polygon. Assumes clockwise order of the polygon.
        // /// Does not handle intersections.
        // /// </summary>
        // /// <param name="polygon">Vertices of the polygon in clockwise order.</param>
        // /// <param name="distance">Offset distance. Positive values offset outside, negative inside.</param>
        // public static void OffsetPolygon(ref Vector2d[] polygon, double distance)
        // {
        //     var offsets = new Vector2d[polygon.Length];
        //     for (int i = 0; i < polygon.Length; i++)
        //     {
        //         Vector2d previous = polygon.GetLooped(i - 1);
        //         Vector2d current = polygon[i];
        //         Vector2d next = polygon.GetLooped(i + 1);
        //
        //         Vector2d bisector = GetAngleBisector(previous, current, next, out double angle);
        //         double angleOffset = GetAngleOffset(distance, angle);
        //         offsets[i] = -bisector*angleOffset;
        //     }
        //
        //     for (int i = 0; i < polygon.Length; i++)
        //     {
        //         polygon[i] += offsets[i];
        //     }
        // }

        public static double GetAngleOffset(double edgeOffset, double angle)
        {
            return edgeOffset/GetAngleBisectorSin(angle);
        }

        public static double GetAngleBisectorSin(double angle)
        {
            return Math.Sin(angle*(Math.PI / 180)/2);
        }

        // /// <summary>
        // /// Calculates a bounding rect for a set of vertices.
        // /// </summary>
        // public static Rect GetRect(IList<Vector2d> vertices)
        // {
        //     Vector2d min = vertices[0];
        //     Vector2d max = vertices[0];
        //     for (var i = 1; i < vertices.Count; i++)
        //     {
        //         var vertex = vertices[i];
        //         min = Vector2d.Min(min, vertex);
        //         max = Vector2d.Max(max, vertex);
        //     }
        //     return Rect.MinMaxRect(min.x, min.y, max.x, max.y);
        // }

        /// <summary>
        /// Calculates a circumradius for a rectangle.
        /// </summary>
        public static double GetCircumradius(Rect rect)
        {
            return GetCircumradius(rect.width, rect.height);
        }

        /// <summary>
        /// Calculates a circumradius for a rectangle.
        /// </summary>
        public static double GetCircumradius(double width, double height)
        {
            return Math.Sqrt(width/2*width/2 + height/2*height/2);
        }
    }
}
