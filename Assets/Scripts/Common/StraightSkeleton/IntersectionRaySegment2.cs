using UnityEngine;

namespace BuildingGenerator.Common.StraightSkeleton
{
    public struct IntersectionRaySegment2
    {
        public IntersectionType type;
        public Vector2d pointA;
        public Vector2d pointB;

        public static IntersectionRaySegment2 None()
        {
            return new IntersectionRaySegment2 {type = IntersectionType.None};
        }

        public static IntersectionRaySegment2 Point(Vector2d pointA)
        {
            return new IntersectionRaySegment2
            {
                type = IntersectionType.Point,
                pointA = pointA,
            };
        }

        public static IntersectionRaySegment2 Segment(Vector2d pointA, Vector2d pointB)
        {
            return new IntersectionRaySegment2
            {
                type = IntersectionType.Segment,
                pointA = pointA,
                pointB = pointB,
            };
        }
    }
}
