using UnityEngine;

namespace BuildingGenerator.Common.StraightSkeleton
{
    public struct IntersectionRayRay2
    {
        public IntersectionType type;
        public Vector2d pointA;
        public Vector2d pointB;

        public static IntersectionRayRay2 None()
        {
            return new IntersectionRayRay2 {type = IntersectionType.None};
        }

        public static IntersectionRayRay2 Point(Vector2d point)
        {
            return new IntersectionRayRay2
            {
                type = IntersectionType.Point,
                pointA = point,
            };
        }

        public static IntersectionRayRay2 Ray(Vector2d origin, Vector2d direction)
        {
            return new IntersectionRayRay2
            {
                type = IntersectionType.Ray,
                pointA = origin,
                pointB = direction,
            };
        }

        public static IntersectionRayRay2 Segment(Vector2d pointA, Vector2d pointB)
        {
            return new IntersectionRayRay2
            {
                type = IntersectionType.Segment,
                pointA = pointA,
                pointB = pointB,
            };
        }
    }
}
