﻿using System;

namespace BuildingGenerator.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    public class ForMeshAttribute: Attribute
    {
        public readonly string name;
        public readonly int submesh;

        public ForMeshAttribute(string name, int submesh = 0)
        {
            this.name = name;
            this.submesh = submesh;
        }
    }
}