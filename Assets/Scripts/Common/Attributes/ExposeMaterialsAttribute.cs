﻿using System;
using UnityEngine;

namespace BuildingGenerator.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ExposeMaterialsAttribute: PropertyAttribute
    {
        public string fieldName;

        public ExposeMaterialsAttribute(string fieldName)
        {
            this.fieldName = fieldName;
        }
    }
}