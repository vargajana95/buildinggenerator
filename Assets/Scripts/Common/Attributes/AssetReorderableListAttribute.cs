﻿using UnityEditor;
using UnityEditorInternal;
using UnityExtensions;

namespace BuildingGenerator.Common.Attributes
{
    public class AssetReorderableListAttribute : ReorderableListAttribute
    {
        public AssetReorderableListAttribute(bool overrideAddCallback = true, bool overrideRemoveCallback = true,
            bool alwaysInsertNew = false)
        {
            if (overrideAddCallback)
            {
                addCallback = AddItem;
            }

            if (overrideRemoveCallback)
            {
                removeCallback = RemoveItem;
            }
        }

        private void AddItem(ReorderableList list)
        {
            SerializedProperty property = list.serializedProperty;

            property.InsertArrayElementAtIndex(list.count);
            //InsertArrayElementAtIndex copies the last element, but we need an empty space
            if (property.GetArrayElementAtIndex(list.count - 1).objectReferenceValue != null)
            {
                property.DeleteArrayElementAtIndex(list.count - 1);
            }

            //EditorUtility.SetDirty(target);
        }

        private void RemoveItem(ReorderableList list)
        {
            SerializedProperty property = list.serializedProperty;

            //DeleteArrayElementAtIndex leaves a null value in the array, so we have to delete that too
            if (property.GetArrayElementAtIndex(list.index).objectReferenceValue != null)
            {
                property.DeleteArrayElementAtIndex(list.index);
            }

            property.DeleteArrayElementAtIndex(list.index);

            //EditorUtility.SetDirty(target);
        }
    }
}