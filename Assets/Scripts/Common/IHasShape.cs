﻿using System.Collections.Generic;
using UnityEngine;

namespace BuildingGenerator.Common
{
    public interface IHasShape
    {
         List<Vector2> GetPoints();

         void ShapeChanged();
    }
}