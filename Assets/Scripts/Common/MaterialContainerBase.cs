﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BuildingGenerator.Common.Attributes;
using UnityEngine;

namespace BuildingGenerator.Common
{
    public abstract class MaterialContainerBase
    {
        public Dictionary<string, List<Material>> AvailableMaterials { get; } = new Dictionary<string, List<Material>>();
        
         public Dictionary<string, List<Material>> GetAvailableMaterials()
        {
            var availableMaterials = new Dictionary<string, List<Material>>();

            var type = GetType();
            var fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public)
                .Where(prop => prop.IsDefined(typeof(ForMeshAttribute), false));
            foreach (var field in fields)
            {
                var attributes = field.GetCustomAttributes<ForMeshAttribute>(false);
                foreach (var attribute in attributes)
                {
                    var fieldValue = field.GetValue(this);
                    var isMaterialField = IsMaterial(field.FieldType);
                    var isMaterialListField = IsMaterialList(field.FieldType);
                    if (!(isMaterialField || isMaterialListField))
                    {
                        Debug.LogError($"{field.Name} is not a Material");
                        break;
                    }

                    if (isMaterialField)
                    {
                        if (!availableMaterials.ContainsKey(attribute.name))
                        {
                            availableMaterials[attribute.name] = new List<Material>();
                        }

                        var materials = availableMaterials[attribute.name];
                        if (materials.Count <= attribute.submesh)
                        {
                            materials.AddRange(Enumerable.Repeat<Material>(null, attribute.submesh - materials.Count + 1));
                        }

                        availableMaterials[attribute.name][attribute.submesh] = (Material) fieldValue;
                    }
                    else if (isMaterialListField)
                    {
                        availableMaterials[attribute.name] = (List<Material>) fieldValue;
                    }
                }
            }

            MergeWithAvailableMaterials(availableMaterials);

            return availableMaterials;
        }

         private void MergeWithAvailableMaterials(Dictionary<string, List<Material>> availableMaterials)
         {
             foreach (var meshToMaterial in AvailableMaterials)
             {
                 availableMaterials[meshToMaterial.Key] = meshToMaterial.Value;
             }
         }

         private static bool IsMaterial(Type fieldType)
         {
             return fieldType.IsAssignableFrom(typeof(Material));
         }

         private static bool IsMaterialList(Type fieldType)
         {
             return fieldType.IsAssignableFrom(typeof(List<Material>));
         }
         
    }

        public class EmptyMaterialContainer: MaterialContainerBase
        {
        }
}