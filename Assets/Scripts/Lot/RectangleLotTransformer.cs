﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Base;
using ProceduralToolkit;
using UnityEngine;
using static BuildingGenerator.Common.Helpers.Utils;

namespace BuildingGenerator.Lot
{
    [CreateAssetMenu(fileName = "RectangleLotTransformer",
        menuName = "BuildingGeneration/LotTransformer/RectangleLotTransformer")]
    public class RectangleLotTransformer : LotTransformer
    {
        public float RectWidth { get; set; }
        public float RectHeight { get; set; }
        
        protected override bool TransformPolygon()
        {
            var success = GetInsideRectangle(BuildingBase.Polygon.ToList(), out var rectangle);
            if (success)
            {
                CalculateWidthAndHeight(rectangle);
                TransformedBuildingBase = new BuildingBase {Polygon = rectangle};
            }

            return success;
        }

        private void CalculateWidthAndHeight(List<Vector2> rectangle)
        {
            RectWidth = (rectangle[1] - rectangle[0]).magnitude;
            RectHeight = (rectangle[2] - rectangle[1]).magnitude;
        }

        private bool GetInsideRectangle(List<Vector2> inputPolygon, out List<Vector2> rectangle)
        {
            rectangle = new List<Vector2>();
            if (inputPolygon.Count < 4)
            {
                return false;
            }

            var mainSide = inputPolygon[1] - inputPolygon[0];

            var leftDotProd =
                Vector2.Dot(inputPolygon[2] - inputPolygon[1], -mainSide.normalized);
            var rightDotProd =
                Vector2.Dot(inputPolygon[inputPolygon.Count - 1] - inputPolygon[0], mainSide.normalized);

            if (leftDotProd > mainSide.magnitude || rightDotProd > mainSide.magnitude)
            {
                return false;
            }

            Vector2 leftBottomPoint = new Vector2();
            Vector2 rightBottomPoint = new Vector2();
            var leftTopPoint = new Vector2(float.NaN, float.NaN);
            var rightTopPoint = new Vector2(float.NaN, float.NaN);
            var leftUpperStartIndex = -1;
            var rightUpperEndIndex = -1;
            var mainSidePerp = -Vector2.Perpendicular(mainSide).normalized;

            if (Mathf.Abs(leftDotProd) <= 0.00005f)
            {
                leftBottomPoint = inputPolygon[1];
                leftUpperStartIndex = 2;
                leftTopPoint = inputPolygon[2];
            }
            else
            {
                if (leftDotProd >= 0)
                {
                    var intersection = SegmentsIntersect(inputPolygon[2], inputPolygon[2] + mainSidePerp,
                        inputPolygon[1],
                        inputPolygon[0]);

                    if (intersection.secondSegmentIntersect)
                    {
                        leftBottomPoint = intersection.intersectionPoint;
                        leftUpperStartIndex = 2;
                        leftTopPoint = inputPolygon[2];
                    }
                }
                else
                {
                    for (var i = 1; i < inputPolygon.Count; i++)
                    {
                        var leftIntersection = SegmentsIntersect(inputPolygon.GetLooped(i + 1),
                            inputPolygon.GetLooped(i + 2),
                            inputPolygon[1],
                            inputPolygon[1] + mainSidePerp);
                        if (leftIntersection.firstSegmentIntersect)
                        {
                            leftUpperStartIndex = (i + 2) % inputPolygon.Count;
                            leftTopPoint = leftIntersection.intersectionPoint;
                            break;
                        }
                    }

                    leftBottomPoint = inputPolygon[1];
                }
            }

            if (Mathf.Abs(rightDotProd) <= 0.00005f)

            {
                rightBottomPoint = inputPolygon[0];
                rightUpperEndIndex = inputPolygon.Count - 1;
                rightTopPoint = inputPolygon.GetLooped(-1);
            }
            else
            {
                if (rightDotProd >= 0)
                {
                    var intersection = SegmentsIntersect(inputPolygon.GetLooped(-1),
                        inputPolygon.GetLooped(-1) + mainSidePerp,
                        inputPolygon[0], inputPolygon[1]);
                    if (intersection.secondSegmentIntersect)
                    {
                        rightBottomPoint = intersection.intersectionPoint;
                        rightUpperEndIndex = inputPolygon.Count - 1;
                        rightTopPoint = inputPolygon.GetLooped(-1);
                    }
                }
                else
                {
                    for (var i = 1; i < inputPolygon.Count; i++)
                    {
                        var rightIntersection = SegmentsIntersect(inputPolygon.GetLooped(-i),
                            inputPolygon.GetLooped(-i - 1),
                            inputPolygon[0],
                            inputPolygon[0] + mainSidePerp);
                        if (rightIntersection.firstSegmentIntersect)
                        {
                            rightUpperEndIndex = inputPolygon.Count - i - 1;
                            rightTopPoint = rightIntersection.intersectionPoint;
                            break;
                        }
                    }

                    rightBottomPoint = inputPolygon[0];
                }
            }

            if (leftUpperStartIndex == -1 || rightUpperEndIndex == -1)
            {
                throw new InvalidOperationException("Malformed polygon. Cannot get inner rectangle.");
            }

            var distanceToClosestPoint = Math.Min(
                DistanceFromPointToLine(rightTopPoint, inputPolygon[0], inputPolygon[1]),
                DistanceFromPointToLine(leftTopPoint, inputPolygon[0], inputPolygon[1]));

            for (var i = leftUpperStartIndex; i <= rightUpperEndIndex; i++)
            {
                var distanceToPoint = DistanceFromPointToLine(inputPolygon[i], inputPolygon[0], inputPolygon[1]);
                if (distanceToPoint < distanceToClosestPoint)
                {
                    distanceToClosestPoint = distanceToPoint;
                }
            }

            leftTopPoint = leftBottomPoint + (leftTopPoint - leftBottomPoint).normalized * distanceToClosestPoint;
            rightTopPoint = rightBottomPoint + (rightTopPoint - rightBottomPoint).normalized * distanceToClosestPoint;

            rectangle = new List<Vector2>
            {
                rightBottomPoint, leftBottomPoint, leftTopPoint, rightTopPoint
            };

            return true;
        }

 
    }
    
}