﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Helpers;
using BuildingGenerator.Common.StraightSkeleton;
using BuildingGenerator.Creator;
using BuildingGenerator.Generators;
using ProceduralToolkit;
using Unity.EditorCoroutines.Editor;
using UnityEngine;
using CompoundMeshDraft = BuildingGenerator.Common.CompoundMeshDraft;
using MeshDraft = BuildingGenerator.Common.MeshDraft;
using Random = UnityEngine.Random;
using static BuildingGenerator.Common.Helpers.Utils;

namespace BuildingGenerator.Lot
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(SkyscraperCreator))]
    public class SkyscraperGroupGenerator : GeneratorBase, IHasShape
    {
        public List<Vector2> inputPoints = new List<Vector2>
        {
            new Vector2(175, 50),
            new Vector2(50, -85),
            new Vector2(-110, -50),
            new Vector2(-100, 50),
            new Vector2(0, 200),
            new Vector2(70, 200)
        };
        
        [SerializeField] [Expandable] [AssetReorderableList(hideFooterButtons = true)]
        private List<LotDivider> lotDividers;

        [Expandable] [SerializeField] [AssetReorderableList(hideFooterButtons = true)]
        private List<SectionCreatorMapperBase> sectionCreatorMappers;

        [Expandable] [SerializeField] [AssetReorderableList(hideFooterButtons = true)]
        private List<LotTransformer> lotTransformers;

        public Material defaultMaterial;

        private SkyscraperCreator skyscraperCreator;

        private EditorCoroutine generatorCoroutine;

        public override bool EnableAutoGenerate => true;

        public override bool IsGeneratorReady =>
            inputPoints.Count > 0 && sectionCreatorMappers.Count > 0 && lotTransformers.Count > 0;

        private void OnEnable()
        {
            skyscraperCreator = GetComponent<SkyscraperCreator>();
            skyscraperCreator.EnableGenerate = false;
        }

        private void OnDisable()
        {
            skyscraperCreator.EnableGenerate = true;
        }

        public override void Generate()
        {
            var lotPolygon = OrderClockwisePoints(inputPoints);

            var skyscrapersObject = CreateNewObjectWithName("Skyscrapers");
            var lotDivider = RandomE.GetRandom(lotDividers);
            var lots = lotDivider.Divide(lotPolygon);

            DrawLots(lots.Select(l => Utils.MovePolygon(l.BuildingBase.Polygon.ToList(), l.Position)).ToList());

            if (generatorCoroutine != null)
            {
                EditorCoroutineUtility.StopCoroutine(generatorCoroutine);
            }

            generatorCoroutine =
                EditorCoroutineUtility.StartCoroutine(GenerateSkyscrapers(lots, skyscrapersObject), gameObject);
        }

        private IEnumerator GenerateSkyscrapers(List<LotData> lots, GameObject skyscrapersObject)
        {
            var index = 0;
            foreach (var lot in lots)
            {
                var availableLotTransformers = new List<LotTransformer>();
                foreach (var lotTransformer in lotTransformers)
                {
                    if (lotTransformer.Transform(lot.BuildingBase))
                    {
                        availableLotTransformers.Add(lotTransformer);
                    }
                }

                var availableSectionCreator = GetAvailableSectionCreators(availableLotTransformers).ToList().First();
                skyscraperCreator.SectionCreators = new List<SkyscraperSectionCreator> {availableSectionCreator};

                var skyscraper = skyscraperCreator.GenerateSkyscraper();
                var skyscraperObject =
                    SkyscraperGenerator.CreateSkyscraperObject(skyscraper, new GameObject($"Skyscraper{index++}"));
                skyscraperObject.transform.SetParent(skyscrapersObject.transform);
                //Move it so the GameObject center is in the lot center
                skyscraperObject.transform.Translate(lot.Position.ToVector3XZ());


                yield return null;
            }
        }

        private IEnumerable<SkyscraperSectionCreator> GetAvailableSectionCreators(
            List<LotTransformer> availableLotTransformers)
        {
            foreach (var lotTransformer in availableLotTransformers)
            {
                foreach (var sectionCreatorMapper in sectionCreatorMappers)
                {
                    if (sectionCreatorMapper.GetLotTransformerType().IsInstanceOfType(lotTransformer))
                    {
                        if (sectionCreatorMapper.CanMap(lotTransformer))
                        {
                            yield return sectionCreatorMapper.CreateSectionCreator(lotTransformer);
                        }
                    }
                }
            }
        }
        public void DrawLots(List<List<Vector2>> lots, string name = "polygonGameObject", float height = 0f)
        {
            var polygonGameObject = GameObject.Find(name);
            if (polygonGameObject == null)
            {
                polygonGameObject = new GameObject(name);
                polygonGameObject.AddComponent<MeshFilter>();
                polygonGameObject.AddComponent<MeshRenderer>();
                polygonGameObject.transform.position = transform.position;
                polygonGameObject.transform.Translate(0, height, 0);
            }

            var meshFilter = polygonGameObject.GetComponent<MeshFilter>();
            var meshRenderer = polygonGameObject.GetComponent<MeshRenderer>();
            var polygonMeshes = new CompoundMeshDraft();

            var materials = new List<Material>();
            foreach (var lot in lots)
            {
                var tessellator = new Tessellator();
                tessellator.AddContour(lot.ConvertAll(v => v.ToVector3XZ()));
                tessellator.Tessellate(normal: Vector3.up);
                var contourDraft = new MeshDraft().Add(tessellator.ToMeshDraft());
                polygonMeshes.Add(contourDraft);
                var material = new Material(defaultMaterial) {color = Random.ColorHSV(0, 1, 1, 1, 0.7f, 0.7f)};
                materials.Add(material);
            }

            meshFilter.mesh = polygonMeshes.ToMeshWithSubMeshes();
            meshRenderer.materials = materials.ToArray();
        }

        public List<Vector2> GetPoints()
        {
            return inputPoints;
        }

        public void ShapeChanged()
        {
            if (autoGenerate)
            {
                if (useSeed)
                {
                    Random.InitState(seed);
                }

                Generate();
            }
        }

        private List<Vector2> OrderClockwisePoints(List<Vector2> polygon)
        {
            if (PolygonIsClockwise(polygon))
            {
                return new List<Vector2>(polygon);
            }

            return ((IEnumerable<Vector2>) polygon).Reverse().ToList();
        }
    }
}