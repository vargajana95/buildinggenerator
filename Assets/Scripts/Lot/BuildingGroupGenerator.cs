﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Base;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Helpers;
using BuildingGenerator.Creator;
using BuildingGenerator.Generators;
using ProceduralToolkit;
using Unity.EditorCoroutines.Editor;
using UnityEngine;
using Random = UnityEngine.Random;
using CompoundMeshDraft = BuildingGenerator.Common.CompoundMeshDraft;
using MeshDraft = BuildingGenerator.Common.MeshDraft;


namespace BuildingGenerator.Lot
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(HouseGeneratorRandomizer))]
    public class BuildingGroupGenerator : GeneratorBase, IHasShape
    {
        public List<Vector2> inputPoints = new List<Vector2>
        {
            new Vector2(175, 50),
            new Vector2(50, -85),
            new Vector2(-110, -50),
            new Vector2(-100, 50),
            new Vector2(0, 200),
            new Vector2(70, 200)
        };

        [SerializeField] [Expandable] [AssetReorderableList(hideFooterButtons = true)]
        private List<LotDivider> lotDividers;

        private HouseGeneratorRandomizer houseGeneratorRandomizer;
        private HouseGenerator houseGenerator;
        public Material defaultMaterial;

        private EditorCoroutine generatorCoroutine;

        private void OnEnable()
        {
            houseGeneratorRandomizer = GetComponent<HouseGeneratorRandomizer>();
            houseGenerator = GetComponent<HouseGenerator>();

            houseGeneratorRandomizer.EnableGenerate = false;
        }

        private void OnDisable()
        {
            houseGeneratorRandomizer.EnableGenerate = true;
        }

        public override bool IsGeneratorReady => inputPoints.Count > 0;

        public override void Generate()
        {
            var lotPolygon = OrderClockwisePoints(inputPoints);
            var housesObject = Utils.CreateNewObjectWithName("Houses");
            var lotDivider = RandomE.GetRandom(lotDividers);
            var lots = lotDivider.Divide(lotPolygon);

            DrawLots(lots.Select(l => Utils.MovePolygon(l.BuildingBase.Polygon.ToList(), l.Position)).ToList());

            if (generatorCoroutine != null)
            {
                EditorCoroutineUtility.StopCoroutine(generatorCoroutine);
            }

            generatorCoroutine = EditorCoroutineUtility.StartCoroutine(GenerateHouses(lots, housesObject), gameObject);
        }

        private IEnumerator GenerateHouses(List<LotData> lots, GameObject housesObject)
        {
            var index = 0;
            foreach (var lot in lots)
            {
                var baseGenerator = ScriptableObject.CreateInstance<BaseGeneratorWrapper>();
                baseGenerator.BuildingBase = lot.BuildingBase;

                houseGeneratorRandomizer.BaseGeneratorRandomizers.Clear();
                var wrapperBaseGeneratorRandomizer = ScriptableObject.CreateInstance<WrapperBaseGeneratorRandomizer>();
                // baseGenerator.AdjacentSides = lot.NonVisibleSides;
                wrapperBaseGeneratorRandomizer.BaseGenerator = baseGenerator;

                houseGeneratorRandomizer.BaseGeneratorRandomizers.Add(wrapperBaseGeneratorRandomizer);

                var house = houseGeneratorRandomizer.GenerateHouse();
                var houseObject = HouseGenerator.CreateHouseObject(house, new GameObject($"House{index++}"));
                houseObject.transform.SetParent(housesObject.transform);
                //Move it so the GameObject center is in the lot center
                houseObject.transform.Translate(lot.Position.ToVector3XZ());

                yield return null;
            }
        }
        
        private void DrawLots(List<List<Vector2>> lots, string name = "polygonGameObject")
        {
            var polygonGameObject = GameObject.Find(name);
            if (polygonGameObject == null)
            {
                polygonGameObject = new GameObject(name);
                polygonGameObject.AddComponent<MeshFilter>();
                polygonGameObject.AddComponent<MeshRenderer>();
                polygonGameObject.transform.position = transform.position;
            }

            var meshFilter = polygonGameObject.GetComponent<MeshFilter>();
            var meshRenderer = polygonGameObject.GetComponent<MeshRenderer>();

            var polygonMeshes = new CompoundMeshDraft();
            var materials = new List<Material>();

            foreach (var lot in lots)
            {
                var tessellator = new Tessellator();
                tessellator.AddContour(lot.ConvertAll(v => v.ToVector3XZ()));
                tessellator.Tessellate(normal: Vector3.up);
                var contourDraft = new MeshDraft().Add(tessellator.ToMeshDraft());
                polygonMeshes.Add(contourDraft);
                var material = new Material(defaultMaterial) {color = Random.ColorHSV(0, 1, 1, 1, 0.7f, 0.7f)};
                materials.Add(material);
            }

            meshFilter.mesh = polygonMeshes.ToMeshWithSubMeshes();
            meshRenderer.materials = materials.ToArray();
        }

        

        private void DrawDebugSphere(Vector2 position)
        {
            var spheresParent = GameObject.Find("Spheres");
            var primitive = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            primitive.transform.SetParent(spheresParent.transform);
            primitive.transform.localScale *= 7;
            primitive.transform.position = position.ToVector3XZ();
        }

        public List<Vector2> GetPoints()
        {
            return inputPoints;
        }

        public void ShapeChanged()
        {
            if (autoGenerate)
            {
                Generate();
            }
        }

        private List<Vector2> OrderClockwisePoints(List<Vector2> polygon)
        {
            if (Utils.PolygonIsClockwise(polygon))
            {
                return new List<Vector2>(polygon);
            }

            return ((IEnumerable<Vector2>) polygon).Reverse().ToList();
        }

        
    }
}