﻿using System;
using System.Collections.Generic;
using BuildingGenerator.Creator;
using UnityEngine;

namespace BuildingGenerator.Lot
{
    public abstract class SectionCreatorMaker : ScriptableObject
    {
        public List<Vector2> Lot { get; set; }
        public SkyscraperSectionCreator SectionCreator { get; set; }
        
        public abstract bool CanMake();
        public abstract void SetupSectionCreator();

        public abstract Vector2 GetBaseCenter();
    }
}