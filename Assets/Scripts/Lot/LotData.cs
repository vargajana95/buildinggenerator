﻿using BuildingGenerator.Base;
using UnityEngine;

namespace BuildingGenerator.Lot
{
    public class LotData
    {
        public BuildingBase BuildingBase { get; set; }
        public Vector2 Position { get; set; }
    }
}