﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Creator;
using ProceduralToolkit;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;
using static BuildingGenerator.Common.Helpers.Utils;

namespace BuildingGenerator.Lot
{
    [CreateAssetMenu(fileName = "PolygonSectionCreatorMaker",
        menuName = "BuildingGeneration/Skyscraper/SectionCreatorMaker/PolygonSectionCreatorMaker",
        order = 1)]
    public class PolygonSectionCreatorMaker : SectionCreatorMaker
    {
        private static readonly Type SUPPORTED_TYPE = typeof(PolygonSectionCreator);

        [SerializeField] private float lotOffset = 5;
        [SerializeField] private float cornerSquarePercent = 0.2f;

        public override bool CanMake()
        {
            if (SUPPORTED_TYPE != SectionCreator.GetType())
            {
                return false;
            }

            return true;
        }

        public override void SetupSectionCreator()
        {
            var offsetPolygons = OffsetPolygon(Lot, -lotOffset);
            var offsetLot = offsetPolygons[0];

            FixSpikes(offsetLot);
            var sectionCreator = SectionCreator as PolygonSectionCreator;
            sectionCreator.Polygon = offsetLot;
        }
        

        private void FixSpikes(List<Vector2> polygon)
        {
            for (var i = 0; i < polygon.Count; i++)
            {
                var prevSide =polygon[i] - polygon.GetLooped(i - 1);
                var nextSde = polygon.GetLooped(i + 1) - polygon[i];
                
                if (VectorE.Angle360(nextSde, -prevSide) < 60)
                {
                    SquareCorner(polygon, i);
                }
            }
        }

        private void SquareCorner(List<Vector2> polygon, int cornerIndex)
        {
            var prevSide =polygon.GetLooped(cornerIndex - 1) - polygon[cornerIndex];
            var nextSde = polygon.GetLooped(cornerIndex + 1) - polygon[cornerIndex];

            var oldCorner = polygon[cornerIndex];
            polygon.RemoveAt(cornerIndex);
            var sideNewEnd = oldCorner + prevSide * cornerSquarePercent;
            polygon.Insert(cornerIndex, sideNewEnd);
            var nextSideNewEnd = oldCorner + nextSde * cornerSquarePercent;
            polygon.Insert((cornerIndex+1)%polygon.Count, nextSideNewEnd);
        }

        public override Vector2 GetBaseCenter()
        {
            return GetPolygonCenter(Lot);
        }
    }
}