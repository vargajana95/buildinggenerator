﻿using System;
using BuildingGenerator.Creator;
using UnityEngine;

namespace BuildingGenerator.Lot
{
    public abstract class SectionCreatorMapper<T> : SectionCreatorMapperBase
        where T : LotTransformer
    {
        public override Type GetLotTransformerType()
        {
            return typeof(T);
        }
        
        public override bool CanMap(LotTransformer lotTransformer)
        {
            return CanMapSectionCreator((T) lotTransformer);
        }
        protected abstract bool CanMapSectionCreator(T lotTransformer);

        public override SkyscraperSectionCreator CreateSectionCreator(LotTransformer lotTransformer)
        {
            return CreateSectionCreatorInstance((T) lotTransformer);
        }
        protected abstract SkyscraperSectionCreator CreateSectionCreatorInstance(T lotTransformer);
    }
}