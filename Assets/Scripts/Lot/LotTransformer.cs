﻿using System.Collections.Generic;
using BuildingGenerator.Base;
using UnityEngine;

namespace BuildingGenerator.Lot
{
    public abstract class LotTransformer : ScriptableObject
    {
        public BuildingBase BuildingBase { get; private set; }
        public BuildingBase TransformedBuildingBase { get; protected set; }

        public bool Transform(BuildingBase lot)
        {
            BuildingBase = lot;
            return TransformPolygon();
        }

        protected abstract bool TransformPolygon();
    }
}