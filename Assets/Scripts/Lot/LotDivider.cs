﻿using System.Collections.Generic;
using BuildingGenerator.Base;
using UnityEngine;

namespace BuildingGenerator.Lot
{
    public abstract class LotDivider: ScriptableObject
    {
        public abstract List<LotData> Divide(List<Vector2> lotPolygon);
    }
}