﻿using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Base;
using ProceduralToolkit;
using UnityEngine;
using static BuildingGenerator.Common.Helpers.Utils;

namespace BuildingGenerator.Lot
{
    [CreateAssetMenu(fileName = "OffsetLotTransformer",
        menuName = "BuildingGeneration/LotTransformer/OffsetLotTransformer")]
    public class OffsetLotTransformer : LotTransformer
    {
        private float lotOffset = 5f;
        private float cornerSquarePercent = 0.3f;
        
        protected override bool TransformPolygon()
        {
            var offsetPolygons = OffsetPolygon(BuildingBase.Polygon.ToList(), -lotOffset);

            if (offsetPolygons.Count == 0)
            {
                TransformedBuildingBase = new BuildingBase();
                return false;
            }

            var transformedPolygon = offsetPolygons[0];
            FixSpikes(transformedPolygon);
            TransformedBuildingBase = new BuildingBase
            {
                Polygon = transformedPolygon,
            };
            
            return true;
        }
        
        private void FixSpikes(List<Vector2> polygon)
        {
            for (var i = 0; i < polygon.Count; i++)
            {
                var prevSide =polygon[i] - polygon.GetLooped(i - 1);
                var nextSde = polygon.GetLooped(i + 1) - polygon[i];
                
                if (VectorE.Angle360(nextSde, -prevSide) < 60)
                {
                    SquareCorner(polygon, i);
                }
            }
        }

        private void SquareCorner(List<Vector2> polygon, int cornerIndex)
        {
            var prevSide =polygon.GetLooped(cornerIndex - 1) - polygon[cornerIndex];
            var nextSde = polygon.GetLooped(cornerIndex + 1) - polygon[cornerIndex];

            var oldCorner = polygon[cornerIndex];
            polygon.RemoveAt(cornerIndex);
            var sideNewEnd = oldCorner + prevSide * cornerSquarePercent;
            polygon.Insert(cornerIndex, sideNewEnd);
            var nextSideNewEnd = oldCorner + nextSde * cornerSquarePercent;
            polygon.Insert((cornerIndex+1)%polygon.Count, nextSideNewEnd);
        }

 
    }
    
}