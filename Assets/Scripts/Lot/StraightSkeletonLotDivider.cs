﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Base;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Helpers;
using ProceduralToolkit;
using ProceduralToolkit.Skeleton;
using UnityEngine;
using static BuildingGenerator.Common.Helpers.Utils;
using Random = UnityEngine.Random;

namespace BuildingGenerator.Lot
{
    [CreateAssetMenu(fileName = "StraightSkeletonLotDivider",
        menuName = "BuildingGeneration/LotDividers/StraightSkeletonLotDivider")]
    public class StraightSkeletonLotDivider : LotDivider
    {
        [SerializeField] private bool doOffset = true;
        [SerializeField] private float offset = 40f;
        [MinMaxSlider(10, 60)] public Vector2 lotWidth = new Vector2(10, 60);

        [SerializeField] private float lotLengthThreshold = 10f;
        [SerializeField] private float lotAreaThreshold = 200f;

        private List<Vector2> lotPolygon;

        public override List<LotData> Divide(List<Vector2> lotPolygon)
        {
            this.lotPolygon = lotPolygon;

            var buildingBases = new List<LotData>();
            var offsetPolygons = DividePolygon(lotPolygon, out var innerPoly);

            foreach (var offsetPolygon in offsetPolygons)
            {
                RemoveDuplicatedPointsFromPolygon(offsetPolygon);
            }

            FixDiagonalEdges(offsetPolygons);

            var strips = new List<Strip>();
            foreach (var offsetPolygon in offsetPolygons)
            {
                strips.Add(new Strip());
                 // var lots = new List<List<Vector2>> {offsetPolygon}; 
                 var lots = SplitLotSections(offsetPolygon);

                strips.Last().lots = lots;
            }

            PostProcessLots(strips);

            foreach (var lot in strips.SelectMany(s => s.lots))
            {
                var lotCenter = GetPolygonCenter(lot);
                var visibleSides = GetVisibleSideIndices(lot, innerPoly);
                var nonVisibleSides = lot.Select((_, index) => index)
                    .Where((index) => !visibleSides.Contains(index)).ToList();

                var movedLot = MovePolygon(lot, -lotCenter);

                var buildingBase = new BuildingBase {Polygon = movedLot};

                buildingBase.SetAdjacentSides(nonVisibleSides);
                buildingBases.Add(new LotData {Position = lotCenter, BuildingBase = buildingBase});
            }

            return buildingBases;
        }


        private void PostProcessLots(List<Strip> strips)
        {
            foreach (var strip in strips)
            {
                //Merge lots with small length
                MergeLotsWhere(
                    lot => (lot[1] - lot[0]).sqrMagnitude < lotLengthThreshold * lotLengthThreshold, strip.lots);
                //Merge lots with small area
                MergeLotsWhere(lot => PolygonArea(lot) < lotAreaThreshold, strip.lots);
            }
        }

        private void MergeLotsWhere(Predicate<List<Vector2>> pred, List<List<Vector2>> lots)
        {
            if (lots.Count < 2) return;

            for (var i = 0; i < lots.Count;)
            {
                var lot = lots[i];
                if (pred(lot))
                {
                    if (i == 0)
                    {
                        MergeToPreviousLot(lots[i + 1], lot);
                        lots.RemoveAt(i + 1);
                    }
                    else
                    {
                        MergeToPreviousLot(lot, lots[i - 1]);
                        lots.RemoveAt(i);
                    }
                }
                else
                {
                    i++;
                }
            }
        }


        private void MergeToPreviousLot(List<Vector2> lot, List<Vector2> prevLot)
        {
            PolygonFindMutualEdge(prevLot, lot, out var polygon1Indices, out var polygon2Indices);

            if (polygon1Indices.Count < 2)
            {
                throw new ArgumentException("Lots are not neighbours. Cannot merge them.");
            }

            for (var i = polygon2Indices.Count - 1; i >= 0; i--)
            {
                lot.RemoveAt(polygon2Indices[i]);
            }

            for (var i = polygon1Indices.Count - 1; i >= 0; i--)
            {
                prevLot.RemoveAt(polygon1Indices[i]);
            }

            for (var i = 0; i < lot.Count; i++)
            {
                prevLot.Insert(polygon1Indices[0] + i, lot[i]);
            }
        }


        private void FixDiagonalEdges(List<List<Vector2>> polygons)
        {
            for (var i = 0; i < polygons.Count; i++)
            {
                var polygon = polygons[i];
                var nextPolygon = polygons.GetLooped(i + 1);
                var polygonMainSide = (polygon[1] - polygon[0]).normalized;
                var nextPolygonMainSide = (nextPolygon[1] - nextPolygon[0]).normalized;

                var cornerPoint = polygon[1];
                var furthestPoint = FindFurthestMutualPointFromCorner(polygon, nextPolygon);

                var isReflexAngle = Vector2.SignedAngle(-polygonMainSide, nextPolygonMainSide) <= 0;
                if (isReflexAngle)
                {
                    continue;
                }

                // var cutPoint = isReflexAngle ? cornerPoint : furthestPoint;
                var cutPoint = furthestPoint;

                //var cutVector1 = isReflexAngle ? nextPolygonMainSide.normalized : -nextPolygonMainSide.normalized;
                var cutVector1 = -nextPolygonMainSide.normalized;
                //var cutVector1 = Vector2.Perpendicular(polygonMainSide);
                var cutVector2 = /*isReflexAngle ?*/
                    (cornerPoint - furthestPoint).normalized /*: furthestPoint - cornerPoint*/;

                if (Math.Abs(Vector2.Dot(cutVector2, -polygonMainSide)) <
                    Math.Abs(Vector2.Dot(cutVector1, -polygonMainSide)))
                {
                    continue;
                }

                var segmentsIntersection = SegmentsIntersect(cutPoint, cutPoint + cutVector1, polygon[0], polygon[1]);
                if (segmentsIntersection.t2 < 1f && segmentsIntersection.t2 > 0f)
                {
                    var split = SplitPolygonByLine(polygon, cutPoint, cutPoint + cutVector1);

                    polygons[i] = split.leftPolygon;

                    // if (isReflexAngle)
                    // {
                    //     split.rightPolygon.RemoveAt(0);
                    //     split.rightPolygon.RemoveAt(0);
                    // }
                    // else
                    // {
                    split.rightPolygon.RemoveAt(split.rightPolygon.Count - 1);
                    split.rightPolygon.RemoveAt(split.rightPolygon.Count - 1);
                    //}

                    nextPolygon.AddRange(split.rightPolygon);
                }
                else
                {
                    // if (isReflexAngle)
                    // {
                    //     // split.rightPolygon.RemoveAt(0);
                    //     // split.rightPolygon.RemoveAt(0);
                    // }
                    // else
                    // {
                    polygons[i].RemoveAt(2);
                    polygons[i].RemoveAt(1);
                    polygons[i].Add(polygons[i][0]);
                    polygons[i].RemoveAt(0);

                    // foreach (var vector2 in polygons[i])
                    // {
                    //     DrawDebugSphere(vector2);
                    // }
                    nextPolygon.AddRange(polygons[i]);
                    polygons.RemoveAt(i);
                    //i--;
                    // }
                }


                // if (polygons[i].Count <= 1)
                // {
                //     polygons.RemoveAt(i);
                //     //i--;
                // }
            }
        }

        private Vector2 FindFurthestMutualPointFromCorner(List<Vector2> polygon1, List<Vector2> polygon2)
        {
            if (polygon1[1] != polygon2[0])
            {
                throw new ArgumentException("Bad polygons");
            }

            //TODO 2020-09-16 get mutual points even if its just on the others edge. Not necessarily exactly the same points  
            var mutualPoints = PolygonFindMutualEdge(polygon1, polygon2);
            if (mutualPoints.Count <= 1)
            {
                throw new ArgumentException("Bad polygons. Not enough mutual points");
            }

            var fromPoint = polygon1[1];
            var furthestPoint = fromPoint;
            foreach (var mutualPoint in mutualPoints)
            {
                if ((mutualPoint - fromPoint).sqrMagnitude > (furthestPoint - fromPoint).sqrMagnitude)
                {
                    furthestPoint = mutualPoint;
                }
            }

            return furthestPoint;
        }

        private List<List<Vector2>> DividePolygon(List<Vector2> polygon, out List<Vector2> innerEdges)
        {
            var skeletonGenerator = new StraightSkeletonGenerator();
            var skeleton = skeletonGenerator.Generate(polygon);
            
            var offsetPolygons = new List<List<Vector2>>();
            innerEdges = new List<Vector2>();

            foreach (var skeletonPolygon in skeleton.polygons)
            {
                if (doOffset)
                {
                    var lineNormalVector =
                        -Vector2.Perpendicular(skeletonPolygon[1] - skeletonPolygon[0]).normalized * offset;
                    var lineA = lineNormalVector + skeletonPolygon[0];
                    var lineB = lineNormalVector + skeletonPolygon[1];

                    var split = SplitPolygonByLine(skeletonPolygon, lineA, lineB);

                    if (split.splitSuccessful)
                    {
                        innerEdges.AddRange(split.newEdge);
                    }

                    offsetPolygons.Add(split.leftPolygon);
                }
                else
                {
                    offsetPolygons.Add(skeletonPolygon);
                }
            }

            return offsetPolygons;
        }

        public List<List<Vector2>> SplitLotSections(List<Vector2> polygon)
        {
            var mainSide = polygon[1] - polygon[0];
            var mainSideNormalized = mainSide.normalized;
            var remainder = polygon;
            var splitNormal = -Vector2.Perpendicular(mainSideNormalized);
            var lots = new List<List<Vector2>>();

            var splitDistance = Random.Range(lotWidth.x, lotWidth.y);
            var splitDistanceSum = splitDistance;
            while (splitDistanceSum < mainSide.magnitude)
            {
                var splitVector = mainSideNormalized * splitDistance;
                var splitPoint = remainder[0] + splitVector;
                var split = SplitPolygonByLine(remainder, splitPoint, splitPoint + splitNormal);
                lots.Add(split.rightPolygon);

                remainder = split.leftPolygon;

                splitDistance = Random.Range(lotWidth.x, lotWidth.y);
                splitDistanceSum += splitDistance;
            }

            if (remainder.Count > 0)
            {
                lots.Add(remainder);
            }

            return lots;
        }

        private List<int> GetVisibleSideIndices(List<Vector2> lot, List<Vector2> innerEdges)
        {
            var visibleSides = new List<int>();

            for (var i = 0; i < lot.Count; i++)
            {
                if (InnerEdgesContainSegment(innerEdges, lot[i], lot.GetLooped(i + 1)))
                {
                    visibleSides.Add(i);
                }

                if (PolygonEdgesContainSegment(lotPolygon, lot[i], lot.GetLooped(i + 1)))
                {
                    visibleSides.Add(i);
                }
            }

            return visibleSides;
        }

        private bool InnerEdgesContainSegment(List<Vector2> innerEdges, Vector2 segmentA, Vector2 segmentB)
        {
            for (var i = 0; i < innerEdges.Count; i += 2)
            {
                if (SegmentContainsSegment(innerEdges[i], innerEdges[i + 1], segmentA, segmentB))
                {
                    return true;
                }
            }

            return false;
        }

        private bool PolygonEdgesContainSegment(List<Vector2> polygon, Vector2 segmentA, Vector2 segmentB)
        {
            for (var i = 0; i < polygon.Count; i++)
            {
                if (SegmentContainsSegment(polygon[i], polygon.GetLooped(i + 1), segmentA, segmentB))
                {
                    return true;
                }
            }

            return false;
        }

        private class Strip
        {
            public List<List<Vector2>> lots { get; set; }
        }
    }
}