﻿using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Creator;
using ProceduralToolkit;
using UnityEngine;
using static BuildingGenerator.Common.Helpers.Utils;

namespace BuildingGenerator.Lot
{
    [CreateAssetMenu(fileName = "RectangleLotSegmentSectionCreatorMapper",
        menuName = "BuildingGeneration/SectionCreatorMapper/RectangleLotSegmentSectionCreatorMapper")]
    public class
        RectangleLotSegmentSectionCreatorMapper : SectionCreatorMapper<RectangleLotTransformer>
    {
        [SerializeField] private float areaThreshold = 0.7f;
        [SerializeField] [Expandable] private SegmentBasedSectionCreator sectionCreatorTemplate;

        protected override bool CanMapSectionCreator(RectangleLotTransformer lotTransformer)
        {
            return IsRectangleBigEnough(lotTransformer.BuildingBase.Polygon.ToList(),
                lotTransformer.RectWidth * lotTransformer.RectHeight);
        }

        protected override SkyscraperSectionCreator CreateSectionCreatorInstance(RectangleLotTransformer lotTransformer)
        {
            var newSectionCreator = Instantiate(sectionCreatorTemplate);
            newSectionCreator.MaxWidth = (int) lotTransformer.RectWidth;
            newSectionCreator.MaxDepth = (int) lotTransformer.RectHeight;
            var transformedPolygon = lotTransformer.TransformedBuildingBase.Polygon.ToList();
            newSectionCreator.Origin = GetPolygonCenter(transformedPolygon);
            newSectionCreator.BaseRotation = Quaternion.FromToRotation(Vector3.forward,
                (transformedPolygon[1] - transformedPolygon[0]).normalized
                .ToVector3XZ());

            return newSectionCreator;
        }


        private bool IsRectangleBigEnough(List<Vector2> lot, float rectangleArea)
        {
            return rectangleArea / PolygonArea(lot) > areaThreshold;
        }
    }
}