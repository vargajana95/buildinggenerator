﻿using System;
using BuildingGenerator.Creator;
using UnityEngine;

namespace BuildingGenerator.Lot
{
    public abstract class SectionCreatorMapperBase : ScriptableObject
    {
        public abstract Type GetLotTransformerType();

        public abstract bool CanMap(LotTransformer lotTransformer);

        public abstract SkyscraperSectionCreator CreateSectionCreator(LotTransformer lotTransformer);
    }
}