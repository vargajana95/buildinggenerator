﻿using System.Linq;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Creator;
using UnityEngine;

namespace BuildingGenerator.Lot
{
    [CreateAssetMenu(fileName = "PolygonSectionCreatorMapper",
        menuName = "BuildingGeneration/SectionCreatorMapper/PolygonSectionCreatorMapper")]
    public class PolygonSectionCreatorMapper : SectionCreatorMapper<LotTransformer>
    {
        [SerializeField] [Expandable] private PolygonSectionCreator sectionCreator;
        
        protected override bool CanMapSectionCreator(LotTransformer lotTransformer)
        {
            return true;
        }

        protected override SkyscraperSectionCreator CreateSectionCreatorInstance(LotTransformer lotTransformer)
        {
            sectionCreator.Polygon = lotTransformer.TransformedBuildingBase.Polygon.ToList();
            return sectionCreator;
        }
    }
}