﻿using System;
using BuildingGenerator.Common;
using ProceduralToolkit;
using UnityEditor;
using UnityEngine;

namespace BuildingGenerator.Editor
{
    [CustomEditor(typeof(ShapeCreator))]
    public class ShapeEditor : UnityEditor.Editor
    {
        ShapeCreator shapeCreator;
        SelectionInfo selectionInfo;
        bool needsRepaint;
        private bool editMode;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            Event guiEvent = Event.current;
            if (guiEvent.type == EventType.KeyDown && guiEvent.keyCode == KeyCode.E &&
                guiEvent.modifiers == EventModifiers.Control)
            {
                editMode = !editMode;
            }

            string helpMessage =
                "Left click to add points.\nShift-left click on point to delete.";
            EditorGUILayout.HelpBox(helpMessage, MessageType.Info);

            GUILayout.BeginHorizontal();


            EditorGUI.BeginChangeCheck();
            editMode = GUILayout.Toggle(editMode, "Edit polygon", "Button");
            if (EditorGUI.EndChangeCheck())
            {
                SceneView.RepaintAll();
            }

            GUILayout.EndHorizontal();
        }

        void OnSceneGUI()
        {
            Event guiEvent = Event.current;
            if (guiEvent.type == EventType.KeyDown && guiEvent.keyCode == KeyCode.E &&
                guiEvent.modifiers == EventModifiers.Control)
            {
                editMode = !editMode;
                needsRepaint = true;
            }

            if (!editMode)
            {
                return;
            }


            if (guiEvent.type == EventType.Repaint)
            {
                Draw();
            }
            else if (guiEvent.type == EventType.Layout)
            {
                HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            }
            else
            {
                HandleInput(guiEvent);
                if (needsRepaint)
                {
                    HandleUtility.Repaint();
                }
            }
        }

        void HandleInput(Event guiEvent)
        {
            Ray mouseRay = HandleUtility.GUIPointToWorldRay(guiEvent.mousePosition);
            float drawPlaneHeight = 0;
            float dstToDrawPlane = (drawPlaneHeight - mouseRay.origin.y) / mouseRay.direction.y;
            Vector2 mousePosition = mouseRay.GetPoint(dstToDrawPlane).ToVector2XZ();
            mousePosition = new Vector2((float) Math.Round(mousePosition.x, 2), (float) Math.Round(mousePosition.y, 2));

            if (guiEvent.type == EventType.MouseDown && guiEvent.button == 0 &&
                guiEvent.modifiers == EventModifiers.Shift)
            {
                HandleShiftLeftMouseDown(mousePosition);
            }

            if (guiEvent.type == EventType.MouseDown && guiEvent.button == 0 &&
                guiEvent.modifiers == EventModifiers.None)
            {
                HandleLeftMouseDown(mousePosition);
            }

            if (guiEvent.type == EventType.MouseUp && guiEvent.button == 0 && guiEvent.modifiers == EventModifiers.None)
            {
                HandleLeftMouseUp(mousePosition);
            }

            if (guiEvent.type == EventType.MouseDrag && guiEvent.button == 0 &&
                guiEvent.modifiers == EventModifiers.None)
            {
                HandleLeftMouseDrag(mousePosition);
            }

            if (!selectionInfo.pointIsSelected)
            {
                UpdateMouseOverInfo(mousePosition);
            }
        }

        private void HandleShiftLeftMouseDown(Vector2 mousePosition)
        {
            if (selectionInfo.mouseIsOverPoint)
            {
                DeletePointUnderMouse();
            }
        }


        void HandleLeftMouseDown(Vector2 mousePosition)
        {
            if (!selectionInfo.mouseIsOverPoint)
            {
                int newPointIndex = (selectionInfo.mouseIsOverLine)
                    ? selectionInfo.lineIndex + 1
                    : shapeCreator.points.Count;
                Undo.RecordObject(shapeCreator, "Add point");
                shapeCreator.points.Insert(newPointIndex, mousePosition);
                selectionInfo.pointIndex = newPointIndex;
                shapeCreator.ShapeChanged();
            }

            selectionInfo.pointIsSelected = true;
            selectionInfo.positionAtStartOfDrag = mousePosition;
            needsRepaint = true;
        }

        void HandleLeftMouseUp(Vector2 mousePosition)
        {
            if (selectionInfo.pointIsSelected)
            {
                shapeCreator.points[selectionInfo.pointIndex] = selectionInfo.positionAtStartOfDrag;
                Undo.RecordObject(shapeCreator, "Move point");
                shapeCreator.points[selectionInfo.pointIndex] = mousePosition;

                selectionInfo.pointIsSelected = false;
                selectionInfo.pointIndex = -1;
                needsRepaint = true;
            }
        }

        void HandleLeftMouseDrag(Vector2 mousePosition)
        {
            if (selectionInfo.pointIsSelected)
            {
                shapeCreator.points[selectionInfo.pointIndex] = mousePosition;
                needsRepaint = true;
                shapeCreator.ShapeChanged();
            }
        }

        void UpdateMouseOverInfo(Vector2 mousePosition)
        {
            int mouseOverPointIndex = -1;
            for (int i = 0; i < shapeCreator.points.Count; i++)
            {
                if (Vector2.Distance(mousePosition, shapeCreator.points[i]) < shapeCreator.handleRadius)
                {
                    mouseOverPointIndex = i;
                    break;
                }
            }

            if (mouseOverPointIndex != selectionInfo.pointIndex)
            {
                selectionInfo.pointIndex = mouseOverPointIndex;
                selectionInfo.mouseIsOverPoint = mouseOverPointIndex != -1;

                needsRepaint = true;
            }

            if (selectionInfo.mouseIsOverPoint)
            {
                selectionInfo.mouseIsOverLine = false;
                selectionInfo.lineIndex = -1;
            }
            else
            {
                int mouseOverLineIndex = -1;
                float closestLineDst = shapeCreator.handleRadius;
                for (int i = 0; i < shapeCreator.points.Count; i++)
                {
                    Vector2 nextPointInShape = shapeCreator.points[(i + 1) % shapeCreator.points.Count];
                    float dstFromMouseToLine = HandleUtility.DistancePointToLineSegment(mousePosition,
                        shapeCreator.points[i], nextPointInShape);
                    if (dstFromMouseToLine < closestLineDst)
                    {
                        closestLineDst = dstFromMouseToLine;
                        mouseOverLineIndex = i;
                    }
                }

                if (selectionInfo.lineIndex != mouseOverLineIndex)
                {
                    selectionInfo.lineIndex = mouseOverLineIndex;
                    selectionInfo.mouseIsOverLine = mouseOverLineIndex != -1;
                    needsRepaint = true;
                }
            }
        }

        void Draw()
        {
            for (int i = 0; i < shapeCreator.points.Count; i++)
            {
                Vector2 nextPoint = shapeCreator.points[(i + 1) % shapeCreator.points.Count];
                if (i == selectionInfo.lineIndex)
                {
                    Handles.color = Color.red;
                    //Handles.DrawLine(shapeCreator.points[i], nextPoint);
                    Handles.DrawAAPolyLine(Texture2D.whiteTexture, 3, shapeCreator.points[i].ToVector3XZ(),
                        nextPoint.ToVector3XZ());
                }
                else
                {
                    Handles.color = Color.black;
                    //Handles.DrawDottedLine(shapeCreator.points[i], nextPoint, 4);
                    Handles.DrawAAPolyLine(Texture2D.grayTexture, 3, shapeCreator.points[i].ToVector3XZ(),
                        nextPoint.ToVector3XZ());
                }

                if (i == selectionInfo.pointIndex)
                {
                    Handles.color = (selectionInfo.pointIsSelected) ? Color.black : Color.red;
                }
                else
                {
                    Handles.color = Color.white;
                }

                Handles.DrawSolidDisc(shapeCreator.points[i].ToVector3XZ(), Vector3.up, shapeCreator.handleRadius);
            }


            needsRepaint = false;
        }

        private void DeletePointUnderMouse()
        {
            Undo.RecordObject(shapeCreator, "Delete point");
            shapeCreator.points.RemoveAt(selectionInfo.pointIndex);
            selectionInfo.pointIsSelected = false;
            selectionInfo.pointIndex = -1;
            selectionInfo.mouseIsOverPoint = false;
            needsRepaint = true;
            shapeCreator.ShapeChanged();
        }

        void OnEnable()
        {
            shapeCreator = target as ShapeCreator;
            selectionInfo = new SelectionInfo();
        }

        public class SelectionInfo
        {
            public int pointIndex = -1;
            public bool mouseIsOverPoint;
            public bool pointIsSelected;
            public Vector2 positionAtStartOfDrag;

            public int lineIndex = -1;
            public bool mouseIsOverLine;
        }
    }
}