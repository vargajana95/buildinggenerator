﻿using BuildingGenerator.Common;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

#if UNITY_EDITOR
namespace BuildingGenerator.Editor
{
    /// <summary>
    /// Draws the property field for any field marked with ExpandableAttribute.
    /// </summary>
    [CustomPropertyDrawer(typeof(WeightedItem), true)]
    public class WeightedItemDrawer : PropertyDrawer
    {
        // Use the following area to change the style of the expandable ScriptableObject drawers;

        #region Style Setup

        private enum BackgroundStyles
        {
            None,
            HelpBox,
            Darken,
            Lighten
        }

        /// <summary>
        /// Whether the default editor Script field should be shown.
        /// </summary>
        // private static bool SHOW_SCRIPT_FIELD = false;

        /// <summary>
        /// The spacing on the inside of the background rect.
        /// </summary>
        // private static float INNER_SPACING = 6.0f;

        /// <summary>
        /// The spacing on the outside of the background rect.
        /// </summary>
        // private static float OUTER_SPACING = 4.0f;

        /// <summary>
        /// The style the background uses.
        /// </summary>
        // private static BackgroundStyles BACKGROUND_STYLE = BackgroundStyles.Lighten;

        /// <summary>
        /// The colour that is used to darken the background.
        /// </summary>
        private static Color DARKEN_COLOUR = new Color(0.0f, 0.0f, 0.0f, 0.2f);

        /// <summary>
        /// The colour that is used to lighten the background.
        /// </summary>
        private static Color LIGHTEN_COLOUR = new Color(1.0f, 1.0f, 1.0f, 0.2f);

        #endregion

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float totalHeight = 0.0f;

            // totalHeight += EditorGUIUtility.singleLineHeight;

            totalHeight += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("item"), true) +
                           EditorGUIUtility.standardVerticalSpacing;
            // totalHeight += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("weight"), true) + EditorGUIUtility.standardVerticalSpacing;

            // totalHeight += INNER_SPACING * 2;
            // totalHeight += OUTER_SPACING * 2;

            return totalHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Rect fieldRect = new Rect(position);
            fieldRect.height = EditorGUIUtility.singleLineHeight;


            var itemProperty = property.FindPropertyRelative("item");
            var weightProperty = property.FindPropertyRelative("weight");

            EditorGUI.PropertyField(fieldRect, itemProperty, GUIContent.none);
            fieldRect.x += fieldRect.width - 100;
            fieldRect.width = 100;
            var labelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 55;
            EditorGUI.PropertyField(fieldRect, weightProperty);
            // weightProperty.floatValue = EditorGUI.FloatField(fieldRect, "Weight:", weightProperty.floatValue);
            EditorGUIUtility.labelWidth = labelWidth;
        }
    }
}
#endif