﻿using System.Linq;
using System.Reflection;
using BuildingGenerator.Common.Attributes;
using Cinemachine.Editor;
using UnityEngine;
using Object = System.Object;
#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;

#endif

#if UNITY_EDITOR
namespace BuildingGenerator.Editor
{
    /// <summary>
    /// Draws the property field for any field marked with ExpandableAttribute.
    /// </summary>
    [CustomPropertyDrawer(typeof(ExposeMaterialsAttribute), true)]
    public class ExposeMaterialFieldsAttributeDrawer : PropertyDrawer
    {
        // Use the following area to change the style of the expandable ScriptableObject drawers;

        #region Style Setup

        private enum BackgroundStyles
        {
            None,
            HelpBox,
            Darken,
            Lighten
        }

        /// <summary>
        /// The spacing on the inside of the background rect.
        /// </summary>
        private static float INNER_SPACING = 6.0f;

        /// <summary>
        /// The spacing on the outside of the background rect.
        /// </summary>
        private static float OUTER_SPACING = 4.0f;

        /// <summary>
        /// The style the background uses.
        /// </summary>
        private static BackgroundStyles BACKGROUND_STYLE = BackgroundStyles.Lighten;

        /// <summary>
        /// The colour that is used to darken the background.
        /// </summary>
        private static Color DARKEN_COLOUR = new Color(0.0f, 0.0f, 0.0f, 0.2f);

        /// <summary>
        /// The colour that is used to lighten the background.
        /// </summary>
        private static Color LIGHTEN_COLOUR = new Color(1.0f, 1.0f, 1.0f, 0.2f);

        #endregion

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float totalHeight = 0.0f;
            var exposeFacadeMaterialsAttribute = (ExposeMaterialsAttribute) attribute;
            var facadePlanner = property.serializedObject
                .FindProperty(exposeFacadeMaterialsAttribute.fieldName)
                .objectReferenceValue;
            if (facadePlanner == null)
            {
                return totalHeight;
            }

            totalHeight += EditorGUIUtility.singleLineHeight;

            if (!property.isExpanded)
                return totalHeight;


            if (facadePlanner != null)
            {
                var materialContainer = facadePlanner.GetType().GetProperty("Materials",
                    BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

                var materialsFields = materialContainer.GetValue(facadePlanner).GetType()
                    .GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

                var fieldValuesDictionary = new Dictionary<string, SerializedProperty>();

                var fieldValues = property.FindPropertyRelative("fieldValues");

                for (int i = 0; i < fieldValues.arraySize; i++)
                {
                    var element = fieldValues.GetArrayElementAtIndex(i);
                    var fieldValue = element.FindPropertyRelative("value");
                    var name = element.FindPropertyRelative("propertyName").stringValue;
                    fieldValuesDictionary[name] = fieldValue;
                }

                foreach (var fi in materialsFields)
                {
                    if (!fieldValuesDictionary.ContainsKey(fi.Name))
                    {
                        totalHeight += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
                    }
                    else
                    {
                        totalHeight += EditorGUI.GetPropertyHeight(fieldValuesDictionary[fi.Name], true) +
                                       EditorGUIUtility.standardVerticalSpacing;
                    }
                }
            }

            totalHeight += INNER_SPACING * 2;
            totalHeight += OUTER_SPACING * 2;

            return totalHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var exposeFacadeMaterialsAttribute = (ExposeMaterialsAttribute) attribute;
            var materialHolderObject = property.serializedObject
                .FindProperty(exposeFacadeMaterialsAttribute.fieldName)
                .objectReferenceValue;

            if (materialHolderObject == null)
            {
                return;
            }

            Rect fieldRect = new Rect(position);
            fieldRect.height = EditorGUIUtility.singleLineHeight;

            var indentLevel = EditorGUI.indentLevel;

            EditorGUI.indentLevel = 0;
            EditorGUI.indentLevel++; // = 1;
            // var propertyType = GetPropertyType();
            //
            // var childrenCount = property.GetVisibleChildren().Count();
            //
            // if (childrenCount > 0)
            // {
            // }
            //
            // if (childrenCount <= 0)
            // {
            //     property.isExpanded = false;
            //     EditorGUI.indentLevel = indentLevel;
            //     return;
            // }

            property.isExpanded = EditorGUI.Foldout(fieldRect, property.isExpanded, label, true);

            if (!property.isExpanded)
            {
                EditorGUI.indentLevel = indentLevel;
                return;
            }

            #region Format Field Rects

            List<Rect> propertyRects = new List<Rect>();
            Rect marchingRect = new Rect(fieldRect);

            Rect bodyRect = new Rect(fieldRect);
            bodyRect.xMin += EditorGUI.indentLevel * 14;
            bodyRect.yMin += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing
                                                               + OUTER_SPACING;

            marchingRect.y += INNER_SPACING + OUTER_SPACING;


            var fieldValuesDictionary = GetFieldValuesDictionary(property);

            foreach (var keyValuePair in fieldValuesDictionary)
            {
                marchingRect.y += marchingRect.height + EditorGUIUtility.standardVerticalSpacing;
                marchingRect.height = EditorGUI.GetPropertyHeight(keyValuePair.Value, true);
                propertyRects.Add(marchingRect);
            }


            marchingRect.y += INNER_SPACING;

            bodyRect.yMax = marchingRect.yMax;

            #endregion

            DrawBackground(bodyRect);

            #region Draw Fields

            EditorGUI.indentLevel++;

            int index = 0;

            var propertyValues = new List<(string name, UnityEngine.Object property)>();
            foreach (var keyValuePair in fieldValuesDictionary)
            {
                var o = keyValuePair.Value;
                EditorGUI.PropertyField(propertyRects[index], o, new GUIContent(keyValuePair.Key + "s"), true);
                propertyValues.Add((keyValuePair.Key, o.objectReferenceValue));
                index++;
            }

            SerializeMaterialFieldValues(property, propertyValues);

            // targetObject.ApplyModifiedProperties();

            EditorGUI.indentLevel--;
            EditorGUI.indentLevel = indentLevel;

            #endregion
        }

        private Dictionary<string, SerializedProperty> GetFieldValuesDictionary(SerializedProperty property)
        {
            var exposeFacadeMaterialsAttribute = (ExposeMaterialsAttribute) attribute;

            var facadePlanner = property.serializedObject
                .FindProperty(exposeFacadeMaterialsAttribute.fieldName)
                .objectReferenceValue;


            var materialContainer = facadePlanner.GetType().GetProperty("Materials",
                BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

            var materialsFields = materialContainer.GetValue(facadePlanner).GetType()
                .GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

            var fieldValuesDictionary = new Dictionary<string, SerializedProperty>();

            var fieldValues = property.FindPropertyRelative("fieldValues");

            foreach (var fi in materialsFields)
            {
                fieldValuesDictionary[fi.Name] = null;
            }

            for (int i = 0; i < fieldValues.arraySize; i++)
            {
                var element = fieldValues.GetArrayElementAtIndex(i);
                var fieldValue = element.FindPropertyRelative("value");
                var name = element.FindPropertyRelative("propertyName").stringValue;
                if (fieldValuesDictionary.ContainsKey(name))
                {
                    fieldValuesDictionary[name] = fieldValue;
                }
            }


            var propertyDictionary = new Dictionary<string, SerializedProperty>(fieldValuesDictionary);
            foreach (var keyValuePair in fieldValuesDictionary)
            {
                SerializedProperty prop;
                if (keyValuePair.Value == null)
                {
                    fieldValues.arraySize++;
                    var fieldValue = fieldValues.GetArrayElementAtIndex(fieldValues.arraySize - 1)
                        .FindPropertyRelative("value");
                    var name = fieldValues.GetArrayElementAtIndex(fieldValues.arraySize - 1)
                        .FindPropertyRelative("propertyName");
                    name.stringValue = keyValuePair.Key;
                    prop = fieldValue;
                }
                else
                {
                    prop = keyValuePair.Value;
                }

                propertyDictionary[keyValuePair.Key] = prop;
            }

            return propertyDictionary;
        }

        private static void SerializeMaterialFieldValues(SerializedProperty property,
            List<(string name, UnityEngine.Object property)> propertyValues)
        {
            var fieldValues = property.FindPropertyRelative("fieldValues");
            fieldValues.ClearArray();
            for (var i = 0; i < propertyValues.Count; i++)
            {
                fieldValues.arraySize++;
                // var prop = (MaterialSelector) propertyValues[i].property;
                // if (prop != null)
                // {
                //     prop.FieldName = propertyValues[i].name;
                // }

                fieldValues.GetArrayElementAtIndex(fieldValues.arraySize - 1).FindPropertyRelative("value")
                    .objectReferenceValue = propertyValues[i].property;
                fieldValues.GetArrayElementAtIndex(fieldValues.arraySize - 1).FindPropertyRelative("propertyName")
                    .stringValue = propertyValues[i].name;
            }
        }


        /// <summary>
        /// Draws the Background
        /// </summary>
        /// <param name="rect">The Rect where the background is drawn.</param>
        private void DrawBackground(Rect rect)
        {
            switch (BACKGROUND_STYLE)
            {
                case BackgroundStyles.HelpBox:
                    EditorGUI.HelpBox(rect, "", MessageType.None);
                    break;

                case BackgroundStyles.Darken:
                    EditorGUI.DrawRect(rect, DARKEN_COLOUR);
                    break;

                case BackgroundStyles.Lighten:
                    //EditorGUI.DrawRect (rect, LIGHTEN_COLOUR);
                    GUI.Box(rect, GUIContent.none, GUI.skin.box);
                    break;
            }
        }

        public Type GetPropertyType()
        {
            // there are only two container field type that can be serialized:
            // Array and List<T>
            if (fieldInfo.FieldType.IsArray)
            {
                return fieldInfo.FieldType.GetElementType();
            }

            if (fieldInfo.FieldType.IsGenericType)
            {
                return fieldInfo.FieldType.GetGenericArguments()[0];
            }

            return fieldInfo.FieldType;
        }
    }
}
#endif