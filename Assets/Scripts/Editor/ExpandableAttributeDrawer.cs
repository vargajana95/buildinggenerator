﻿using System.Linq;
using System.Text.RegularExpressions;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Helpers;
using UnityEngine;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;

#endif

#if UNITY_EDITOR
namespace BuildingGenerator.Editor
{
    /// <summary>
    /// Draws the property field for any field marked with ExpandableAttribute.
    /// </summary>
    [CustomPropertyDrawer(typeof(ExpandableAttribute), true)]
    public class ExpandableAttributeDrawer : PropertyDrawer
    {
        // Use the following area to change the style of the expandable ScriptableObject drawers;

        #region Style Setup

        private enum BackgroundStyles
        {
            None,
            HelpBox,
            Darken,
            Lighten
        }

        /// <summary>
        /// Whether the default editor Script field should be shown.
        /// </summary>
        private static bool SHOW_SCRIPT_FIELD = false;

        /// <summary>
        /// The spacing on the inside of the background rect.
        /// </summary>
        private static float INNER_SPACING = 6.0f;

        /// <summary>
        /// The spacing on the outside of the background rect.
        /// </summary>
        private static float OUTER_SPACING = 4.0f;

        /// <summary>
        /// The style the background uses.
        /// </summary>
        private static BackgroundStyles BACKGROUND_STYLE = BackgroundStyles.Lighten;

        /// <summary>
        /// The colour that is used to darken the background.
        /// </summary>
        private static Color DARKEN_COLOUR = new Color(0.0f, 0.0f, 0.0f, 0.2f);

        /// <summary>
        /// The colour that is used to lighten the background.
        /// </summary>
        private static Color LIGHTEN_COLOUR = new Color(1.0f, 1.0f, 1.0f, 0.2f);

        #endregion

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float totalHeight = 0.0f;

            totalHeight += EditorGUIUtility.singleLineHeight;

            if (property.objectReferenceValue == null)
                return totalHeight;

            if (!property.isExpanded)
                return totalHeight;

            SerializedObject targetObject = new SerializedObject(property.objectReferenceValue);

            SerializedProperty field = targetObject.GetIterator();

            field.NextVisible(true);

            if (SHOW_SCRIPT_FIELD)
            {
                totalHeight += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            }

            while (field.NextVisible(false))
            {
                totalHeight += EditorGUI.GetPropertyHeight(field, true) + EditorGUIUtility.standardVerticalSpacing;
            }

            totalHeight += INNER_SPACING * 2;
            totalHeight += OUTER_SPACING * 2;

            return totalHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            CreateAssetCopies(property);

            Rect fieldRect = new Rect(position);
            fieldRect.height = EditorGUIUtility.singleLineHeight;

            var indentLevel = EditorGUI.indentLevel;

            EditorGUI.indentLevel = 0;
            var propertyType = GetPropertyType();

//        Debug.Log(property.propertyPath);
            var childrenCount = property.GetVisibleChildren().Count();

            EditorGUI.indentLevel++; // = 1;
            if (childrenCount > 0)
            {
                // EditorGUI.indentLevel++; // = 1;
            }

            EditorGUI.BeginChangeCheck();

            var expandableAttribute = (ExpandableAttribute) attribute;
            Object expandableFieldValue = property.objectReferenceValue;
            var objectRect = fieldRect;
            objectRect.width -= expandableAttribute.headerEnd + 6;
            expandableFieldValue = EditorGUI.ObjectField(objectRect, label,
                expandableFieldValue, propertyType, false);

            if (EditorGUI.EndChangeCheck())
            {
                if (expandableFieldValue == null)
                {
                    var assetPath = AssetDatabase.GetAssetPath(property.objectReferenceValue);
                    if (property.objectReferenceValue != null && assetPath.Length > 0)
                    {
                        Object.DestroyImmediate(property.objectReferenceValue, true);
                        AssetDatabase.SaveAssets();
                    }

                    property.objectReferenceValue = null;
                }
                else
                {
                    var scriptableObjectInstance = Object.Instantiate(expandableFieldValue);

                    //Eliminate the (Clone) from the name
                    scriptableObjectInstance.name = expandableFieldValue.name;
                    property.objectReferenceValue = scriptableObjectInstance;
                    if (property.objectReferenceValue == null)
                    {
                        string prefix;
                        if (property.propertyPath.Contains("["))
                        {
                            var i1 = property.propertyPath.LastIndexOf("[", StringComparison.Ordinal) + 1;
                            var i2 = property.propertyPath.LastIndexOf("]", StringComparison.Ordinal) - i1;

                            var elementIndex = Convert.ToInt32(property.propertyPath.Substring(i1, i2));
                            prefix = $"z_{property.name}[{elementIndex}].";
                        }
                        else
                        {
                            prefix = $"z_{property.name}.";
                        }

                        var path = AssetDatabase.GetAssetPath(property.serializedObject.targetObject);
                        Object[] assets = AssetDatabase.LoadAllAssetRepresentationsAtPath(path);

                        // var assetToDelete = assets.FirstOrDefault(a => a.name.StartsWith(prefix));
                        // if (assetToDelete != null)
                        // {
                        //     UnityEngine.Object.DestroyImmediate(assetToDelete, true);
                        // }
                        for (int i = 0; i < assets.Length; i++)
                        {
                            // UnityEngine.Object.DestroyImmediate(assets[i], true);
                        }

                        AssetDatabase.SaveAssets();

                        var name = $"{prefix}{scriptableObjectInstance.name}";
                        scriptableObjectInstance.name = name;
                        // scriptableObjectInstance.hideFlags = HideFlags.HideInHierarchy;
                        AssetDatabase.AddObjectToAsset(scriptableObjectInstance, path);
                        AssetDatabase.SaveAssets();
                        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(scriptableObjectInstance));
                        property.objectReferenceValue = scriptableObjectInstance;
                    }

                    property.isExpanded = true;
                }
            }

            if (property.objectReferenceValue == null)
            {
                EditorGUI.indentLevel = indentLevel;
                return;
            }


            childrenCount = property.GetVisibleChildren().Count();
            if (childrenCount <= 0)
            {
                property.isExpanded = false;
                EditorGUI.indentLevel = indentLevel;
                return;
            }

            property.isExpanded = EditorGUI.Foldout(objectRect, property.isExpanded, GUIContent.none, true);

            if (!property.isExpanded)
            {
                EditorGUI.indentLevel = indentLevel;
                return;
            }


            SerializedObject targetObject = new SerializedObject(property.objectReferenceValue);

            #region Format Field Rects

            List<Rect> propertyRects = new List<Rect>();
            Rect marchingRect = new Rect(fieldRect);

            Rect bodyRect = new Rect(fieldRect);
            bodyRect.xMin += EditorGUI.indentLevel * 14;
            bodyRect.yMin += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing
                                                               + OUTER_SPACING;

            SerializedProperty field = targetObject.GetIterator();
            field.NextVisible(true);

            marchingRect.y += INNER_SPACING + OUTER_SPACING;

            if (SHOW_SCRIPT_FIELD)
            {
                propertyRects.Add(marchingRect);
                marchingRect.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            }

            while (field.NextVisible(false))
            {
                marchingRect.y += marchingRect.height + EditorGUIUtility.standardVerticalSpacing;
                marchingRect.height = EditorGUI.GetPropertyHeight(field, true);
                propertyRects.Add(marchingRect);
            }

            marchingRect.y += INNER_SPACING;

            bodyRect.yMax = marchingRect.yMax;

            #endregion

            DrawBackground(bodyRect);

            #region Draw Fields

            EditorGUI.indentLevel++;

            int index = 0;
            field = targetObject.GetIterator();
            field.NextVisible(true);

            if (SHOW_SCRIPT_FIELD)
            {
                //Show the disabled script field
                EditorGUI.BeginDisabledGroup(true);
                EditorGUI.PropertyField(propertyRects[index], field, true);
                EditorGUI.EndDisabledGroup();
                index++;
            }

            //Replacement for "editor.OnInspectorGUI ();" so we have more control on how we draw the editor
            while (field.NextVisible(false))
            {
                try
                {
                    EditorGUI.PropertyField(propertyRects[index], field, true);
                }
                catch (StackOverflowException)
                {
                    field.objectReferenceValue = null;
                    Debug.LogError("Detected self-nesting cauisng a StackOverflowException, avoid using the same " +
                                   "object iside a nested structure.");
                }

                index++;
            }

            targetObject.ApplyModifiedProperties();

            EditorGUI.indentLevel--;
            EditorGUI.indentLevel = indentLevel;

            #endregion
        }

        private static void CreateAssetCopies(SerializedProperty property)
        {
            var parent = property.serializedObject.targetObject;
            var parentAssetPath = AssetDatabase.GetAssetPath(parent);
            var propertyAssetPath = AssetDatabase.GetAssetPath(property.objectReferenceValue);

            if (parentAssetPath.Length == 0 && propertyAssetPath.Length > 0)
            {
                var newValue = Object.Instantiate(property.objectReferenceValue);
                newValue.name = newValue.name.Replace("(Clone)", "");
                newValue.name = Regex.Replace(newValue.name, "^z_[^.]*\\.", "");
                property.objectReferenceValue = newValue;
            }
        }


        /// <summary>
        /// Draws the Background
        /// </summary>
        /// <param name="rect">The Rect where the background is drawn.</param>
        private void DrawBackground(Rect rect)
        {
            switch (BACKGROUND_STYLE)
            {
                case BackgroundStyles.HelpBox:
                    EditorGUI.HelpBox(rect, "", MessageType.None);
                    break;

                case BackgroundStyles.Darken:
                    EditorGUI.DrawRect(rect, DARKEN_COLOUR);
                    break;

                case BackgroundStyles.Lighten:
                    //EditorGUI.DrawRect (rect, LIGHTEN_COLOUR);
                    GUI.Box(rect, GUIContent.none, GUI.skin.box);
                    break;
            }
        }

        public Type GetPropertyType()
        {
            // there are only two container field type that can be serialized:
            // Array and List<T>
            if (fieldInfo.FieldType.IsArray)
            {
                return fieldInfo.FieldType.GetElementType();
            }

            if (fieldInfo.FieldType.IsGenericType)
            {
                return fieldInfo.FieldType.GetGenericArguments()[0];
            }

            return fieldInfo.FieldType;
        }
    }
}
#endif