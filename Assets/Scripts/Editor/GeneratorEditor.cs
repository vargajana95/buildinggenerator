﻿using BuildingGenerator.Generators;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;


namespace BuildingGenerator.Editor
{
    [CustomEditor(typeof(GeneratorBase), true)]
    public class GeneratorEditor : UnityEditor.Editor
    {
        private GeneratorBase generator;

        private bool shouldGenerate;

        private SerializedProperty useSeed;
        private SerializedProperty seed;

        private SerializedProperty autoGenerate;


        public void OnEnable()
        {
            generator = (GeneratorBase) target;
            useSeed = serializedObject.FindProperty("useSeed");
            seed = serializedObject.FindProperty("seed");
            autoGenerate = serializedObject.FindProperty("autoGenerate");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            DrawDefaultInspector();

            EditorGUILayout.Separator();

            DrawGeneratorSettings();

            serializedObject.ApplyModifiedProperties();

            if (ShouldGenerate && generator.EnableGenerate)
            {
                generator.Generate();
                shouldGenerate = false;
            }

            if (GUILayout.Button("Export to .fbx"))
            {
                var gameObjectToExport = GameObject.Find("House");
                ExportGameObject(gameObjectToExport);
            }
        }

        private void DrawGeneratorSettings()
        {
            EditorGUILayout.BeginHorizontal();

            var isGeneratorReady = generator.IsGeneratorReady;
            EditorGUI.BeginDisabledGroup(!(generator.EnableGenerate && generator.IsGeneratorReady));
            useSeed.boolValue = EditorGUILayout.Toggle("Use Specific Seed", useSeed.boolValue);
            if (useSeed.boolValue)
            {
                seed.intValue = EditorGUILayout.IntField("Seed", seed.intValue);
                Random.InitState(seed.intValue);
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Separator();

            EditorGUILayout.BeginHorizontal();

            var tooltip = isGeneratorReady ? "" : "The generator is not set up correctly!";
            if (!generator.EnableGenerate)
            {
                tooltip = "Generation is controlled by another component!";
            }

            if (GUILayout.Button(new GUIContent("Generate", tooltip)))
            {
                shouldGenerate = true;
            }

            if (generator.EnableAutoGenerate)
            {
                var labelWidth = GUI.skin.label.CalcSize(new GUIContent("Auto Generate")).x;
                EditorGUIUtility.labelWidth = labelWidth;
                autoGenerate.boolValue = EditorGUILayout.Toggle("Auto Generate", autoGenerate.boolValue,
                    GUILayout.MaxWidth(labelWidth + 15));
            }

            EditorGUILayout.EndHorizontal();
            EditorGUI.EndDisabledGroup();
        }

        private bool ShouldGenerate =>
            generator.EnableAutoGenerate && autoGenerate.boolValue && GUI.changed || shouldGenerate;


        private static void ExportGameObject(GameObject o)
        {
            var activeGameObject = Selection.activeGameObject;
            Selection.activeGameObject = o;
            EditorApplication.ExecuteMenuItem("GameObject/Export To FBX...");
            Selection.activeGameObject = activeGameObject;
        }
    }
}