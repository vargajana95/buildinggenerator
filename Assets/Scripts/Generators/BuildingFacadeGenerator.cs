﻿using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Helpers;
using BuildingGenerator.Facade;
using UnityEngine;
using static BuildingGenerator.Common.Helpers.Utils;

namespace BuildingGenerator.Generators
{
    public class BuildingFacadeGenerator : GeneratorBase
    {
        [SerializeField] private Material material;

        [Expandable] [SerializeField] private FacadeGenerator facadePlanner;

        [Range(0, 60f)] [SerializeField] private float facadeHeight = 15f;
        [Range(0, 60f)] [SerializeField] private float facadeWidth = 10f;

        public override bool EnableAutoGenerate => true;
        public override bool IsGeneratorReady => facadePlanner != null;

        public override void Generate()
        {
            var facade = facadePlanner.Generate(facadeWidth, facadeHeight);

            facade.MergeDraftsWithTheSameName();

            var housesGameObject = CreateNewObjectWithName("Facade");

            CreateWallObject(facade, housesGameObject);
        }

        private GameObject CreateWallObject(CompoundMeshDraft facadeMeshDraft, GameObject housesGameObject)
        {
            var wallObject = new GameObject("Wall");
            wallObject.transform.SetParent(housesGameObject.transform);
            wallObject.transform.position = new Vector3(-facadeWidth / 2, 0, 0);


            var meshFilter = wallObject.gameObject.AddComponent<MeshFilter>();
            var finalMesh = facadeMeshDraft.ToMeshWithSubMeshes();
            meshFilter.mesh = finalMesh;

            var meshRenderer = wallObject.gameObject.AddComponent<MeshRenderer>();

            meshRenderer.materials = facadeMeshDraft
                .CalculateMaterials(facadePlanner.Materials.GetAvailableMaterials(), material).ToArray();
            ;

            return wallObject;
        }
    }
}