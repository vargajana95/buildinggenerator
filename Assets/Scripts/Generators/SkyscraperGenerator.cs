﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Base;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Helpers;
using BuildingGenerator.Roof;
using ProceduralToolkit;
using ProceduralToolkit.Buildings;
using UnityEngine;
using UnityExtensions;
using CompoundMeshDraft = BuildingGenerator.Common.CompoundMeshDraft;
using MeshDraft = BuildingGenerator.Common.MeshDraft;
using Random = UnityEngine.Random;
using static BuildingGenerator.Common.Helpers.Utils;

namespace BuildingGenerator.Generators
{
    public class SkyscraperGenerator : GeneratorBase
    {
        public Material defaultMaterial;
        public Material sectionRoofMaterial;

        [Expandable] [SerializeField] private RoofGenerator roofGenerator;

        [ReorderableList(hideFooterButtons = true)]
        public List<Section> sections;

        public override bool EnableAutoGenerate => true;

        public override bool IsGeneratorReady => roofGenerator != null && sections.Count > 0;


        public RoofGenerator RoofGenerator
        {
            get => roofGenerator;
            set => roofGenerator = value;
        }


        public List<Section> Sections
        {
            get => sections;
            set => sections = value;
        }

        public Material SectionRoofMaterial
        {
            get => sectionRoofMaterial;
            set => sectionRoofMaterial = value;
        }

        public override void Generate()
        {
            var skyscraperGameObject = CreateNewObjectWithName("Skyscraper");

            var skyscraper = GenerateSkyscraper();

            CreateSkyscraperObject(skyscraper, skyscraperGameObject);
        }

        public static GameObject CreateSkyscraperObject(Skyscraper skyscraper, GameObject skyscraperGameObject = null)
        {
            if (skyscraperGameObject == null)
            {
                skyscraperGameObject = new GameObject("Skyscraper");
            }

            foreach (var skyscraperSegment in skyscraper.segments)
            {
                CreateWallObject(skyscraperSegment.facadesMeshDraft, skyscraperGameObject, skyscraperSegment.materials);
            }

            var height = skyscraper.segments.Sum(s => s.height);
            CreateRoofObject(skyscraper.roofMeshDraft, height, skyscraperGameObject, skyscraper.roofMaterials);

            return skyscraperGameObject;
        }

        public Skyscraper GenerateSkyscraper()
        {
            var skyscraper = new Skyscraper();
            int actualHeight = 0;
            for (var sectionIndex = 0; sectionIndex < sections.Count; sectionIndex++)
            {
                var section = sections[sectionIndex];
                var buildingBase = section.baseGenerator.Generate();

                var facades = new List<CompoundMeshDraft>();

                foreach (var side in buildingBase)
                {
                    var point = side.StartPoint;
                    var nextPoint = side.EndPoint;
                    var facadeWidth = (point - nextPoint).magnitude;
                    Vector3 normal = (nextPoint - point).Perp().ToVector3XZ();


                    var facade = section.facadeGenerator.Generate(facadeWidth, section.height);
                    facade.Rotate(Quaternion.LookRotation(normal));
                    facade.Move(point.ToVector3XZ());
                    facades.Add(facade);
                }

                var facadesMeshDraft = new CompoundMeshDraft();
                foreach (var facadeMesh in facades)
                {
                    facadesMeshDraft.Add(facadeMesh);
                }

                if (sectionIndex == sections.Count - 1)
                {
                    var roofMeshDraft = roofGenerator.Generate(buildingBase.Polygon.ToList());
                    roofMeshDraft.Rotate(section.rotation);
                    roofMeshDraft.Move(section.origin.ToVector3XZ());
                    roofMeshDraft.MergeDraftsWithTheSameName();
                    skyscraper.roofMeshDraft = roofMeshDraft;

                    skyscraper.roofMaterials = skyscraper.roofMeshDraft
                        .CalculateMaterials(roofGenerator.Materials.GetAvailableMaterials(), defaultMaterial).ToArray();
                }
                else
                {
                    var roofTop = CreateSectionRoofTop(buildingBase);

                    facadesMeshDraft.Add(roofTop.Move(Vector3.up * section.height));
                }


                facadesMeshDraft.Rotate(section.rotation);
                facadesMeshDraft.Move(section.origin.ToVector3XZ()).Move(Vector3.up * actualHeight);
                facadesMeshDraft.MergeDraftsWithTheSameName();

                var availableMaterials = section.facadeGenerator.Materials.GetAvailableMaterials();
                availableMaterials["SectionRoof"] = new List<Material> {sectionRoofMaterial};
                var materials = facadesMeshDraft.CalculateMaterials(availableMaterials, defaultMaterial).ToArray();
                skyscraper.segments.Add(
                    new SkyscraperSegment
                    {
                        facadesMeshDraft = facadesMeshDraft,
                        height = section.height,
                        materials = materials,
                    });

                actualHeight += section.height;
            }

            return skyscraper;
        }

        private static MeshDraft CreateSectionRoofTop(BuildingBase buildingBase)
        {
            var tessellator = new Tessellator();
            tessellator.AddContour(buildingBase.Polygon.Select(p => p.ToVector3XZ()).ToList());
            tessellator.Tessellate(normal: Vector3.up);
            var roofTop = tessellator.ToMeshDraft();
            for (var i = 0; i < roofTop.vertexCount; i++)
            {
                roofTop.uv.Add(Vector2.zero);
                roofTop.normals.Add(Vector3.up);
            }

            var sectionRoofTop = new MeshDraft(roofTop.ToMesh()) {name = "SectionRoof"};
            ApplyUVCoordinates(sectionRoofTop, 10, 10);
            return sectionRoofTop;
        }

        private static GameObject CreateWallObject(CompoundMeshDraft facadesMeshDraft, GameObject skyscraperGameObject,
            Material[] materials)
        {
            var wallObject = new GameObject("Wall");
            wallObject.transform.SetParent(skyscraperGameObject.transform);


            var meshFilter = wallObject.gameObject.AddComponent<MeshFilter>();
            var finalMesh = facadesMeshDraft.ToMeshWithSubMeshes();
            meshFilter.mesh = finalMesh;

            var meshRenderer = wallObject.gameObject.AddComponent<MeshRenderer>();
            // var materials = new Material[finalMesh.subMeshCount];
            // for (int i = 0; i < materials.Length; i++)
            // {
            //     materials[i] = material;
            // }

            meshRenderer.materials = materials;

            return wallObject;
        }

        private static GameObject CreateRoofObject(CompoundMeshDraft skyscraperRoofMeshDraft, float height,
            GameObject skyscraperGameObject, Material[] materials)
        {
            var roofObject = new GameObject("Roof");
            roofObject.transform.SetParent(skyscraperGameObject.transform);
            roofObject.transform.Translate(Vector3.up * height);


            var meshFilter = roofObject.gameObject.AddComponent<MeshFilter>();
            var finalMesh = skyscraperRoofMeshDraft.ToMeshWithSubMeshes();
            meshFilter.mesh = finalMesh;

            var meshRenderer = roofObject.gameObject.AddComponent<MeshRenderer>();

            meshRenderer.materials = materials;

            return roofObject;
        }

        public class Skyscraper
        {
            public List<SkyscraperSegment> segments = new List<SkyscraperSegment>();
            public Material[] roofMaterials;
            public CompoundMeshDraft roofMeshDraft;
        }

        public class SkyscraperSegment
        {
            public CompoundMeshDraft facadesMeshDraft;
            public Material[] materials;
            public float height;
        }
    }
}