﻿using UnityEngine;

namespace BuildingGenerator.Generators
{
    public abstract class GeneratorBase: MonoBehaviour
    {
        #region EditorSettings

        [HideInInspector] [SerializeField] protected bool autoGenerate;
        [HideInInspector] [SerializeField] protected bool useSeed;
        [HideInInspector] [SerializeField] protected int seed;

        #endregion
        
        public abstract bool IsGeneratorReady { get; }

        public bool EnableGenerate { get; set; } = true;

        public virtual bool EnableAutoGenerate => false;

        public abstract void Generate();
    }
}