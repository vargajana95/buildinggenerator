﻿using System.Collections.Generic;
using System.Linq;
using BuildingGenerator.Base;
using BuildingGenerator.Common;
using BuildingGenerator.Common.Attributes;
using BuildingGenerator.Common.Helpers;
using BuildingGenerator.Facade;
using BuildingGenerator.Roof;
using ProceduralToolkit;
using UnityEngine;
using CompoundMeshDraft = BuildingGenerator.Common.CompoundMeshDraft;
using static BuildingGenerator.Common.Helpers.Utils;

namespace BuildingGenerator.Generators
{
    public class HouseGenerator : GeneratorBase
    {
        [Range(0, 60f)] public float facadeHeight = 24f;
        public Material defaultMaterial;
        [SerializeField] private bool separateSubmeshForDistinctFacades = true;

        [Expandable] [SerializeField] private FacadeSelector facadeSelector;
        [Expandable] [SerializeField] private BaseGenerator baseGenerator;
        [Expandable] [SerializeField] private RoofGenerator roofGenerator;

        public FacadeSelector FacadeSelector
        {
            get => facadeSelector;
            set => facadeSelector = value;
        }

        public BaseGenerator BaseGenerator
        {
            get => baseGenerator;
            set => baseGenerator = value;
        }

        public RoofGenerator RoofGenerator
        {
            get => roofGenerator;
            set => roofGenerator = value;
        }

        public float FacadeHeight
        {
            get => facadeHeight;
            set => facadeHeight = value;
        }


        public override bool IsGeneratorReady =>
            facadeSelector != null && baseGenerator != null && roofGenerator != null;

        public override bool EnableAutoGenerate => true;

        public override void Generate()
        {
            var house = GenerateHouse();

            var housesGameObject = CreateNewObjectWithName("House");

            CreateHouseObject(house, housesGameObject);
        }

        public House GenerateHouse()
        {
            var buildingBase = baseGenerator.Generate();

            var facadeGenerators = facadeSelector.GetFacadeGenerators(buildingBase);
            var house = new House
            {
                facadesMeshDraft = GenerateFacades(buildingBase, facadeGenerators),
                roofMeshDraft = GenerateRoof(buildingBase.Polygon.ToList()),
                facadeHeight = facadeHeight,
            };
            var availableMaterials = CalculateAvailableMaterials(facadeGenerators);
            house.facadeMaterials = house.facadesMeshDraft
                .CalculateMaterials(availableMaterials, defaultMaterial).ToArray();

            house.roofMaterials = house.roofMeshDraft
                .CalculateMaterials(roofGenerator.Materials.GetAvailableMaterials(), defaultMaterial).ToArray();

            return house;
        }

        private Dictionary<string, List<Material>> CalculateAvailableMaterials(List<FacadeGenerator> facadeGenerators)
        {
            var availableMaterials = new Dictionary<string, List<Material>>();

            foreach (var facadeGenerator in facadeGenerators)
            {
                var facadeMaterials = facadeGenerator.Materials.GetAvailableMaterials();
                foreach (var keyValuePair in facadeMaterials)
                {
                    if (separateSubmeshForDistinctFacades)
                    {
                        var key = $"{facadeGenerator.name}.{keyValuePair.Key}";
                        availableMaterials[key] = keyValuePair.Value;
                    }
                    else
                    {
                        if (availableMaterials.ContainsKey(keyValuePair.Key))
                        {
                            if (keyValuePair.Value != null &&
                                keyValuePair.Value.Count > 0 &&
                                keyValuePair.Value.All(m => m != null))
                            {
                                availableMaterials[keyValuePair.Key] = keyValuePair.Value;
                            }
                        }
                        else
                        {
                            availableMaterials[keyValuePair.Key] = keyValuePair.Value;
                        }
                    }
                }
            }

            return availableMaterials;
        }

        private CompoundMeshDraft GenerateRoof(List<Vector2> basePolygon)
        {
            var roofMeshDraft = roofGenerator.Generate(basePolygon);
            roofMeshDraft.name = "Roof";
            roofMeshDraft.MergeDraftsWithTheSameName();
            return roofMeshDraft;
        }

        private CompoundMeshDraft GenerateFacades(BuildingBase buildingBase, List<FacadeGenerator> facadeGenerators)
        {
            var facades = new List<CompoundMeshDraft>();

            foreach (var side in buildingBase)
            {
                var point = side.StartPoint;
                var nextPoint = side.EndPoint;
                var facadeWidth = (point - nextPoint).magnitude;
                Vector3 normal = (nextPoint - point).Perp().ToVector3XZ();

                FacadeGenerator currentFacadePlanner = facadeGenerators[side.Index];

                var facade = currentFacadePlanner.Generate(facadeWidth, facadeHeight);
                facade.Rotate(Quaternion.LookRotation(normal));
                facade.Move(point.ToVector3XZ());

                if (separateSubmeshForDistinctFacades)
                {
                    PrefixMeshNames(facade, currentFacadePlanner.name);
                }

                facades.Add(facade);
            }

            var facadesMeshDraft = new CompoundMeshDraft();
            foreach (var facadeMesh in facades)
            {
                facadesMeshDraft.Add(facadeMesh);
            }

            facadesMeshDraft.MergeDraftsWithTheSameName();
            facadesMeshDraft.name = "Facades";

            return facadesMeshDraft;
        }

        private void PrefixMeshNames(CompoundMeshDraft facade, string facadeGeneratorName)
        {
            foreach (var meshDraft in facade)
            {
                meshDraft.name = $"{facadeGeneratorName}.{meshDraft.name}";
            }
        }

        public static GameObject CreateHouseObject(House house, GameObject houseGameObject = null)
        {
            if (houseGameObject == null)
            {
                houseGameObject = new GameObject("House");
            }

            var wallObject = CreateFacadesObject(house.facadesMeshDraft, house.facadeMaterials);
            wallObject.transform.SetParent(houseGameObject.transform);

            var roofObject = CreateRoofObject(house.roofMeshDraft, house.roofMaterials);
            roofObject.transform.SetParent(houseGameObject.transform, false);
            roofObject.transform.position = Vector3.up * house.facadeHeight;

            return houseGameObject;
        }

        public static GameObject CreateFacadesObject(CompoundMeshDraft facadesMeshDraft, Material[] materials)
        {
            var wallObject = new GameObject("Facades");

            var meshFilter = wallObject.gameObject.AddComponent<MeshFilter>();
            var finalMesh = facadesMeshDraft.ToMeshWithSubMeshes();
            meshFilter.mesh = finalMesh;

            var meshRenderer = wallObject.gameObject.AddComponent<MeshRenderer>();

            meshRenderer.materials = materials;

            return wallObject;
        }

        public static GameObject CreateRoofObject(CompoundMeshDraft roofMeshDraft, Material[] roofMaterials)
        {
            var roofGameObject = new GameObject("Roof");
            var meshFilter = roofGameObject.gameObject.AddComponent<MeshFilter>();

            var finalMesh = roofMeshDraft.ToMeshWithSubMeshes();
            meshFilter.mesh = finalMesh;

            var meshRenderer = roofGameObject.gameObject.AddComponent<MeshRenderer>();
            var materials = roofMaterials;

            meshRenderer.materials = materials;

            return roofGameObject;
        }
    }
}