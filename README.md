# Building Generator
I designed and implemented a building generator with Unity, that can create highly detailed buildings using procedural techniques. With
the help of this, it is not only possible to create a single building, but one can
create diverse buildings, or a whole group of them. 

The building types can be parameterized by the users, or even new building types
can be added.

![Building facade](Images/screen4_3840x2160_0.png)

![Building](Images/screen32_3840x2160_0.png)

![Building roof](Images/screen28_3840x2160_0.png)

![Skyscraper](Images/screen24_3840x2160_0.png)

![Building group](Images/group1.jpg)

![Skyscraper group](Images/screen30_3840x2160_0.png)
